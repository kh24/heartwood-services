program Heartwood4Synkka;

uses
  {$IFDEF EurekaLog}
  EMemLeaks,
  EResLeaks,
  EDebugExports,
  EDebugJCL,
  EFixSafeCallException,
  EMapWin32,
  EAppVCL,
  EDialogWinAPIMSClassic,
  EDialogWinAPIEurekaLogDetailed,
  EDialogWinAPIStepsToReproduce,
  ExceptionLog7,
  {$ENDIF EurekaLog}
  SmartInspect.VCL,
  SmartInspect.VCL.SiAuto,
  Vcl.Forms,
  windowSynkka in 'windowSynkka.pas' {window_Synkka},
  RelaxXml in 'lib\RelaxXml.pas',
  USynkkaXMLparserUnit in '..\..\KH24\gs1 master\USynkkaXMLparserUnit.pas' {SynkkaXMLparser: TDataModule},
  SynkkaDescriptionsUnit in '..\..\KH24\gs1 master\SynkkaDescriptionsUnit.pas' {SynkkaDescriptions: TDataModule},
  SynkkaXMLfileProcessingU in '..\..\KH24\gs1 master\SynkkaXMLfileProcessingU.pas' {SynkkaXMLfileProcessing: TDataModule},
  SynkkaConfigurationUnit in '..\..\KH24\gs1 master\SynkkaConfigurationUnit.pas',
  Debugs in '..\..\KH24\gs1 master\Debugs.pas',
  PsClasses in '..\..\KH24\gs1 master\PsClasses.pas',
  psRTTI in '..\..\KH24\gs1 master\psRTTI.pas',
  PsStrings in '..\..\KH24\gs1 master\PsStrings.pas',
  PsUtils in '..\..\KH24\gs1 master\PsUtils.pas',
  rLogglyGatewayThread in '..\..\KH24\gs1 master\Loggly gateway\rLogglyGatewayThread.pas',
  SynkkaLocalDatabaseFunctionalitiesU in '..\..\KH24\gs1 master\SynkkaLocalDatabaseFunctionalitiesU.pas' {SynkkaLocalDatabaseFunctionality: TDataModule},
  rMagento2Api in 'rMagento2Api.pas',
  gFieldNames in 'Magento2\gFieldNames.pas';

{$R *.res}

begin
  Si.Connections := 'pipe(), file(filename="Heartwood4Synkka.sil", maxparts="30", rotate="daily")';
  Si.Enabled := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Twindow_Synkka, window_Synkka);
  Application.CreateForm(TSynkkaXMLparser, SynkkaXMLparser);
  Application.CreateForm(TSynkkaLocalDatabaseFunctionality, SynkkaLocalDatabaseFunctionality);
  Application.CreateForm(TSynkkaDescriptions, SynkkaDescriptions);
  Application.CreateForm(TSynkkaXMLfileProcessing, SynkkaXMLfileProcessing);
  Application.CreateForm(TSynkkaLocalDatabaseFunctionality, SynkkaLocalDatabaseFunctionality);
  Application.Run;
end.

