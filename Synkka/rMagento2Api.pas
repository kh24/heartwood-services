unit rMagento2Api;

interface

uses
  SmartInspect.VCL,SmartInspect.VCL.SiAuto,
  classes, sysutils, windows,
  ScHttp,
  system.json;

{
procedure TForm1.Btn_MagentoConnectClick(Sender: TObject);
var
  iMagento2Api : TMagento2Api;
begin
  iMagento2Api := TMagento2Api.Create( nil );
  try
    iMagento2Api.MagentoURL := 'https://www.kauppahalli24.fi';
    if iMagento2Api.Connect( 'janne2', 'BsBCjPupNJtL3O5euC3iNBPGSwltLETL' ) then
    begin
      MemoContent.lines.Add( 'Connected' );

      if iMagento2Api.Product( '6408430030019' ) then
      begin
        MemoContent.lines.Add( 'Updateing product' );

        if iMagento2Api.UpdateProductDescriptions( '6408430030019',
          '<p>Valio voi 500 g voimakassuolainen / Valio sm�r 500 g extrasaltat</p><p>Kun Valio vuonna 1905 perustettiin voinvientiliikkeeksi, maailma oli hyvin erilainen kuin'+
          ' nyt. Valio voi kirnutaan kuitenkin edelleen puhtaasta suomalaisesta kermasta artesaanin ylpeydell� ja ammattitaidolla, ilman lis�aineita. My�s maailmanluokan '+
          'maku on ennallaan. Voimakassuolainen Valio voi on reilummin suolattu. Parhaat makunautinnot syntyv�t ensiluokkaisista raaka-aineista ja siksi Valio voita '+
          'k�ytet��n aina, kun halutaan saada aikaan paras mahdollinen lopputulos ruuanlaitossa ja leivonnassa. Sill� voi on aina voita.</p><p>N�r Valio grundades 1905'+
          ' som en sm�rexportfirma, s�g v�rlden mycket annorlunda ut �n idag. Valio sm�r k�rnas dock fortfarande av ren finsk gr�dde med artesanens stolthet och '+
          'yrkesskicklighet, utan tillsatser. Smaken i v�rldsklass �r ocks� densamma. Valio sm�r extrasaltat har en h�gre salthalt. De b�sta smakerna har sitt '+
          'ursprung i f�rstklassiga r�varor och d�rf�r anv�nds Valio sm�r alltid n�r man vill uppn� b�sta m�jliga resultat i matlagning och bakning. F�r sm�r �r '+
          'alltid sm�r.</p><p><strong>S�ILYTYSOHJEET/ F�RVARING INSTRUKTIONER</strong></p><p>S�ilytet��n +4-+8 asteessa. / F�rvaras vid h�gst +8 �C</p><p><strong>ALKUPER�MAA/'+
          ' VALMISTUSMAA/ URSPRUNGSLAND/TILLVERKNINGSLAND</strong></p><p>Suomi 246</p><p><strong>MAAHANTUOJA / IMPORT�R</strong></p><p>J�lleenmyyj� /�terf�rs�ljare</p><p>Valio '+
          'Oy</p><strong><p>VALMISTAJA/ TILLVERKARE</strong></p><p>Valio Oy</p>',
          '<p>Past�roitu KERMA ja suola</p><p>Past�riserad GR�DDE och salt</p>',
          '<p>Ravintosis�lt� 100 g:sta/ N�ringsv�rde per 100 g</p><p><ul><li>Suola/ Salt: 2.1 g</li><li>'+
          'Natrium/ Natrium: 800 mg</li><li>A-vitamiini/ Vitamin A: 600 ug</li><li>Hiilihydraattia, josta laktoosia/ Kolhydrat, varav laktos: 0.8 g</li><li>D-vitamiini/ Vitamin D:'+
          ' 0.7 ug</li><li>Proteiinia/ Protein: 0.6 g</li><li>Energia/ Energi: 2984 kJ / 713 kcal</li><li>Rasvaa/ Fett : 80 g</li><li>Rasvaa, josta tyydyttyneit� rasvoja/ Fett, '+
          'inklusive m�ttat fett: 47 g</li><li>Hiilihydraattia/ Kolhydrater: 0.8 g</li><li>Sokeria/ varav sockerarter: 0.8 g</li></ul></p>' ) then
          MemoContent.lines.Add( 'update succ' )
        else
          MemoContent.lines.Add( 'Update failed' );
      end else
        MemoContent.lines.Add( 'Product not found' );

    end else
      MemoContent.lines.Add( 'Connect failed' );

  finally
    iMagento2Api.Free;
  end;
end;
}

type
  TMagento2Api = class( TComponent )
    private
      FMagentoURL : string;
      FAccessToken : string;
      FAccessTokenExpiresAt : TDateTime;

      function IncludePageParameters : string;
    protected
      FResponseJSON : TJSONObject;
      FResponseString : string;

      FSearchCriteriaParameters : string;
      FSearchCriteriaPageSize, FSearchCriteriaCurrentPage,
      FSearchCriteriaTotalItemCount, FSearchCriteriaTotalItemCounter : integer;

      procedure SetMagentoURL( const aURL : string );

      function SafeGET( const aCommand : string; const aRestForAllShops : boolean = false  ) : boolean;
    public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;

      function Connect( const aUserName, aPassword : string ) : boolean;
      function IsAccessTokenValid : boolean;

      function Product( const aSKU : string ) : boolean;
      function Products( const aFromDateTime : TDateTime; const aFilterParams : string = '' ) : boolean;
      function ProductSynkkaChoices( const aFromDateTime : TDateTime ) : boolean;
      function GetNextPage( var aItemsArray : TJSONArray ) : boolean;

      function UpdateProductDescriptions( const aSKU : string; aDescription, aIngredients, aNutritional_values, aSynkka_Updated : string ) : boolean;

      property MagentoURL : string read FMagentoURL write SetMagentoURL;
      property SearchCriteriaPageSize : integer read FSearchCriteriaPageSize write FSearchCriteriaPageSize;
      property SearchCriteriaCurrentPage : integer read FSearchCriteriaCurrentPage write FSearchCriteriaCurrentPage;
  end;

const
  MAGENTO2_PRODUCTS_ID = 'id';
  MAGENTO2_PRODUCTS_STATUS = 'status';
    MAGENTO2_PRODUCTS_STATUS_ENABLED = 1;
    MAGENTO2_PRODUCTS_STATUS_DISABLED = 2;
  MAGENTO2_PRODUCTS_SKU = 'sku';
  MAGENTO2_PRODUCTS_NAME = 'name';
  MAGENTO2_PRODUCTS_CREATEDAT = 'created_at';
  MAGENTO2_PRODUCTS_UPDATEDAT = 'updated_at';
  MAGENTO2_PRODUCTS_CUSTOMATTRIBUTES = 'custom_attributes';
  MAGENTO2_PRODUCTS_SHORTNAME_custom = 'short_name';
  MAGENTO2_PRODUCTS_UNIT_custom = 'unit';
  MAGENTO2_PRODUCTS_PROVIDERID_custom = 'provider_id';
  MAGENTO2_PRODUCTS_MERCHANT_custom = 'merchant_id';
  MAGENTO2_PRODUCTS_SUPPLIERNAME_custom = 'supplier_name';
  MAGENTO2_PRODUCTS_SUPPLIERPRODUCTCODE_custom = 'supplier_product_code';
  MAGENTO2_PRODUCTS_MANUFACTURER_NAME_custom = 'manufacturer_name';
  MAGENTO2_PRODUCTS_MANUFACTURER_custom = 'manufacturer';
  MAGENTO2_PRODUCTS_PRICE = 'price';
  MAGENTO2_PRODUCTS_PURCHASEPRICE_custom = 'purchase_price';
  MAGENTO2_PRODUCTS_SPECIALPRICE_custom = 'special_price';
  MAGENTO2_PRODUCTS_SPECIALFROMDATE_custom = 'special_from_date';
  MAGENTO2_PRODUCTS_TAXCLASSID_custom = 'tax_class_id';
  MAGENTO2_PRODUCTS_MINIMUMUSAGEDAYS_custom = 'minimum_usage_days';
  MAGENTO2_PRODUCTS_LIFECYCLE_custom = 'lifecycle';
  MAGENTO2_PRODUCTS_STORAGETYPE_custom = 'storage_type';
  MAGENTO2_PRODUCTS_INSTOCK_custom = 'instock';
  MAGENTO2_PRODUCTS_DATETRACKING_custom = 'date_tracking';
  MAGENTO2_PRODUCTS_SALEBATCH_EAN_custom = 'sale_batch_ean';
  MAGENTO2_PRODUCTS_SALEBATCH_SIZE_custom = 'sale_batch_size';
  MAGENTO2_PRODUCTS_HANDLE_AS_SALEBATCH_custom = 'handle_as_salebatch';
  MAGENTO2_PRODUCTS_PRODUCT_COLLECTING_AND_CHARGE = 'product_collecting_and_charge';
  MAGENTO2_PRODUCTS_PCS_WEIGHT_FACTOR_custom = 'pcs_weight_factor';
  MAGENTO2_PRODUCTS_MINIMUM_ORDER_AMOUNT = 'minimum_order_amount';
  MAGENTO2_PRODUCTS_COMPARISON_FACTOR_custom = 'comparison_factor';
  MAGENTO2_PRODUCTS_COMPARISON_UNIT_custom = 'comparison_unit';
  MAGENTO2_PRODUCTS_AWORDER_ALARMAMOUNT_custom = 'aworder_alarmamount';
  MAGENTO2_PRODUCTS_AWORDER_ORDERAMOUNT_custom = 'aworder_orderamount';
  MAGENTO2_PRODUCTS_CALCULATEPRICE_FROM_PROFIT_custom = 'calcprice_fromprofit';
  MAGENTO2_PRODUCTS_TCODE_custom = 'tcode';
  MAGENTO2_PRODUCTS_TUKO_PRODUCT_TREE_custom = 'tuko_product_tree';
  MAGENTO2_PRODUCTS_WEIGHT_custom = 'weight';
  MAGENTO2_PRODUCTS_PACKAGE_HEIGHT_custom = 'package_height';
  MAGENTO2_PRODUCTS_PACKAGE_WIDTH_custom = 'package_width';
  MAGENTO2_PRODUCTS_PACKAGE_DEPTH_custom = 'package_depth';
  MAGENTO2_PRODUCTS_SALEBATCH_HEIGHT_custom = 'sale_batch_height';
  MAGENTO2_PRODUCTS_SALEBATCH_WIDTH_custom = 'sale_batch_width';
  MAGENTO2_PRODUCTS_SALEBATCH_DEPTH_custom = 'sale_batch_depth';
  MAGENTO2_PRODUCTS_CLASSIFICATION_CATEGORY_CODE_custom = 'classification_category_code'; //brick
  MAGENTO2_PRODUCTS_SCALED_IMAGE_extension = 'scaled_image';
  MAGENTO2_PRODUCTS_ATTRIBUTEGLUTENFREE = 'attribute_glutenfree';
  MAGENTO2_PRODUCTS_ATTRIBUTELACTOSEFREE = 'attribute_lactosefree';
  MAGENTO2_PRODUCTS_ATTRIBUTEORGANIC = 'attribute_organic';
  MAGENTO2_PRODUCTS_ATTRIBUTEPRICEIMAGE = 'attribute_priceimage';
  MAGENTO2_PRODUCTS_ATTRIBUTEVEGAN = 'attribute_vegan';
  MAGENTO2_PRODUCTS_UPDATE_LUOMUKAUPPA_custom = 'update_luomukauppa';

  MAGENTO2_CUSTOMERS_ID = 'id';
  MAGENTO2_CUSTOMERS_MAGENTO1_CUSTID_custom = 'm1_cust_id';
  MAGENTO2_CUSTOMERS_GROUPID = 'group_id';
  MAGENTO2_CUSTOMERS_EMAIL = 'email';
  MAGENTO2_CUSTOMERS_FIRSTNAME = 'firstname';
  MAGENTO2_CUSTOMERS_LASTNAME = 'lastname';
  MAGENTO2_CUSTOMERS_DEFAULTBILLINGID = 'default_billing'; // <default_billing>1</default_billing>
  MAGENTO2_CUSTOMERS_DEFAULTSHIPPINGID = 'default_shipping'; //<default_shipping>1</default_shipping>
  MAGENTO2_CUSTOMERS_COMPANYNAME_custom = 'company_name';
  MAGENTO2_CUSTOMERS_CREATEDAT = 'created_at';
  MAGENTO2_CUSTOMERS_UPDATEDAT = 'updated_at';
  MAGENTO2_CUSTOMERS_ADDRESS = 'addresses';
  MAGENTO2_CUSTOMERS_ADDRESS_ID = 'id';
  MAGENTO2_CUSTOMERS_ADDRESS_STREET = 'street';  //<street> <item>Kala-Maaaaaaatti</item> </street>
  MAGENTO2_CUSTOMERS_ADDRESS_TELEPHONE = 'telephone';
  MAGENTO2_CUSTOMERS_ADDRESS_POSTCODE = 'postcode';
  MAGENTO2_CUSTOMERS_ADDRESS_CITY = 'city';
  MAGENTO2_CUSTOMERS_ADDRESS_FIRSTNAME = 'firstname';
  MAGENTO2_CUSTOMERS_ADDRESS_LASTNAME = 'lastname';
  MAGENTO2_CUSTOMERS_ADDRESS_COMPANY = 'company';
  MAGENTO2_CUSTOMERS_ADDRESS_DEFAULTSHIPPING = 'default_shipping';
  MAGENTO2_CUSTOMERS_ADDRESS_DEFAULTBILLING = 'default_billing';
  MAGENTO2_CUSTOMERS_ADDRESS_RESERVETELEPHONE_custom = 'reserve_telephone';
  MAGENTO2_CUSTOMERS_ADDRESS_DOORCODE_custom = 'doorcode';
  MAGENTO2_CUSTOMERS_ADDRESS_INSTRUCTIONS_custom = 'instructions';

  MAGENTO2_ORDERS_GRANDTOTAL = 'grand_total';
  MAGENTO2_ORDERS_TOTALINVOICED = 'total_invoiced';
  MAGENTO2_ORDERS_ITEMS = 'items';
  MAGENTO2_ORDERS_ITEMS_NAME = 'name';
  MAGENTO2_ORDERS_ITEMS_ITEMID = 'item_id'; // related to ORDERROWS_EXTERNALID
  MAGENTO2_ORDERS_ITEMS_PRICE = 'price';
  MAGENTO2_ORDERS_ITEMS_QTYINVOICED = 'qty_invoiced';
  MAGENTO2_ORDERS_ITEMS_ROWINVOICED = 'row_invoiced';
  MAGENTO2_ORDERS_ITEMS_TAXINVOICED = 'tax_invoiced';
  MAGENTO2_ORDERS_ITEMS_DISCOUNTINVOICED = 'discount_invoiced';
  MAGENTO2_ORDERS_ITEMS_QTYSHIPPED = 'qty_shipped';
  MAGENTO2_ORDERS_ITEMS_QTYREFUNDED = 'qty_refunded';

function GetURLFilename( const aFilePath : String; Const aDelimiter : String = '/' ) : String;

procedure PrepareMagentoFormatSettings( var aFormatSettings : TFormatSettings );
function BoolToMagentoStr( const aBoolean : boolean ) : string;
function DoubleToMagentoStr( const aDouble : double ) : string;
function FormatMagentoDateTime( const aDateTime : TDateTime ) : string;
function SafeFindCustomAttributeNode( const aJSONProductObject : TJSONObject; const aFieldName : string; const aIgnoreWarning : boolean = false ) : TJSONValue;

implementation

uses
  REST.Types,
  gFieldNames,
  DateUtils;

const
  MAGENTORESTPATH = '/rest/V1/';
  MAGENTOALLRESTPATH = '/rest/all/V1/'; //Use this when updating products


(*******************************************************************************************************************)
function HexToInt(HexStr: String): Int64;
var RetVar : Int64;
    i : byte;
begin
  HexStr := UpperCase(HexStr);
  if HexStr[length(HexStr)] = 'H' then
     Delete(HexStr,length(HexStr),1);
  RetVar := 0;

  for i := 1 to length(HexStr) do begin
      RetVar := RetVar shl 4;
      if HexStr[i] in ['0'..'9'] then
         RetVar := RetVar + (byte(HexStr[i]) - 48)
      else
         if HexStr[i] in ['A'..'F'] then
            RetVar := RetVar + (byte(HexStr[i]) - 55)
         else begin
            Retvar := 0;
            break;
         end;
  end;

  Result := RetVar;
end;

function UrlDecode(const EncodedStr: String): String;
var
  I: Integer;
begin
  Result := '';
  if Length(EncodedStr) > 0 then
  begin
    I := 1;
    while I <= Length(EncodedStr) do
    begin
      if EncodedStr[I] = '%' then
        begin
          Result := Result + Chr(HexToInt(EncodedStr[I+1]
                                       + EncodedStr[I+2]));
          I := Succ(Succ(I));
        end
      else if EncodedStr[I] = '+' then
        Result := Result + ' '
      else
        Result := Result + EncodedStr[I];

      I := Succ(I);
    end;
  end;
end;

function GetURLFilename( const aFilePath : String; Const aDelimiter : String = '/' ) : String;
var ii : Integer;
begin
  ii := LastDelimiter(aDelimiter, aFilePath);
  Result := Copy(aFilePath, ii + 1, MaxInt);
  Result := UrlDecode(Result);
end;(* GetURLFilename *)

(*******************************************************************************************************************)

procedure PrepareMagentoFormatSettings( var aFormatSettings : TFormatSettings );
begin
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, aFormatSettings);

  aFormatSettings.DecimalSeparator := '.';
  aFormatSettings.DateSeparator    := '-';
  aFormatSettings.TimeSeparator    := ':';

  aFormatSettings.ShortDateFormat  := 'yyyy/mm/dd';
  aFormatSettings.LongDateFormat   := aFormatSettings.ShortDateFormat;
  aFormatSettings.ShortTimeFormat  := 'hh:nn:ss';
  aFormatSettings.LongTimeFormat   := aFormatSettings.ShortTimeFormat;
end;(* PrepareMagentoFormatSettings *)

function BoolToMagentoStr( const aBoolean : boolean ) : string;
begin
  if aBoolean then
    result := 'true'
  else
    result := 'false';
end;(* BoolToMagentoStr *)

function DoubleToMagentoStr( const aDouble : double ) : string;
var
  iFormatSettings : TFormatSettings;
begin
  PrepareMagentoFormatSettings( iFormatSettings );
  result := FloatToStr( aDouble, iFormatSettings );
end;(* DoubleToMagentoStr *)

function FormatMagentoDateTime( const aDateTime : TDateTime ) : string;
var
  iFormatSettings : TFormatSettings;
begin
  PrepareMagentoFormatSettings( iFormatSettings );

  if TimeOf( aDateTime ) = 0 then
    result := FormatDateTime( 'yyyy/mm/dd', aDateTime, iFormatSettings )
  else
    result := FormatDateTime( 'yyyy/mm/dd', aDateTime, iFormatSettings ) + '%20' +
              FormatDateTime( 'hh/nn/ss', aDateTime, iFormatSettings );

  //SiMain.LogDebug( 'FormatMagentoDateTime = '+ result );
end;(* FormatMagentoDateTime *)

function SafeFindCustomAttributeNode( const aJSONProductObject : TJSONObject; const aFieldName : string; const aIgnoreWarning : boolean = false ) : TJSONValue;
(* 	<custom_attributes>
				<item>
					<attribute_code>special_price</attribute_code>
					<value>1.4900</value>
				</item>
				<item>
					<attribute_code>special_from_date</attribute_code>
					<value>2016-03-11 00:00:00</value>
				</item>
*)
var
  iCustomAttributesArray : TJSONArray;
  iAttribute : TJSONObject;
  iFieldValue : TJSONValue;
  ii : word;
begin
  result := nil;

  if assigned( aJSONProductObject ) then
  begin
    iCustomAttributesArray := aJSONProductObject.FindValue( 'custom_attributes' ) as TJSONArray;

    if assigned( iCustomAttributesArray ) then
    begin
      for ii := 0 to pred( iCustomAttributesArray.Count ) do
      begin
        iAttribute := iCustomAttributesArray.Items[ ii ] as TJSONObject;
        if assigned( iAttribute ) then
        begin
          iFieldValue := iAttribute.FindValue( 'attribute_code' );
          if assigned( iFieldValue ) then
          begin
            if AnsiCompareText( iFieldValue.Value, aFieldName ) = 0 then
            begin
              iFieldValue := iAttribute.FindValue( 'value' );
              result := iFieldValue;
              break;
            end;
          end else
            SiMain.LogError( 'attribute_code attribute was not found!' );
        end else
          SiMain.LogError( 'iAttribute TJSONObject was nil!' );
      end;(* for ii *)

    end else
      SiMain.LogError( 'custom_attributes array not found!' );

  end else
    SiMain.LogError( 'aJSONProductObject parameter was nil!' );
end;(* SafeFindCustomAttributeNode *)

function TMagento2Api.IncludePageParameters : string;
begin
  result := '&searchCriteria[pageSize]='+ inttostr( FSearchCriteriaPageSize ) +
            '&searchCriteria[currentPage]='+ inttostr( FSearchCriteriaCurrentPage );
end;(* IncludePageParameters *)

procedure TMagento2Api.SetMagentoURL( const aURL : string );
begin
  FMagentoURL := aURL;

  FAccessToken := '';
  FAccessTokenExpiresAt := now;
end;(* SetMagentoURL *)

function TMagento2Api.SafeGET( const aCommand : string; const aRestForAllShops : boolean = false  ) : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iMagentoRestPath : string;
begin
  result := false;
  iResponse := nil;
  FreeAndNil( FResponseJSON );
  FResponseString := '';

  (* Use "all" in /rest/all/V1/products when updating product data *)
  if aRestForAllShops then
    iMagentoRestPath := MAGENTOALLRESTPATH
  else
    iMagentoRestPath := MAGENTORESTPATH;

  iRequest := TScHttpWebRequest.Create( FMagentoURL + iMagentoRestPath + aCommand );
  try
    iRequest.ContentType := 'application/json';
    iRequest.Method := ScHttp.rmGET;
    iRequest.Headers.Add( 'Authorization', 'Bearer ' + FAccessToken );
    SiMain.LogVerbose( 'EXECUTE: '+ FMagentoURL + iMagentoRestPath + aCommand );

    try
      try
        iResponse := iRequest.GetResponse;

      except
        on E: HttpException do
        begin
          //if HttpException(E).StatusCode = scForbidden then
          SiMain.LogError( HttpException(E).ServerMessage +
                           ' Msg:' + HttpException(E).Message +
                           ' Trace:' + HttpException(E).StackTrace );
        end;
        on E: Exception do
          SiMain.LogException( E, 'SafeGET' );
      end;(* Except *)

      if iResponse <> nil then
      begin
        //SiMain.LogInteger( 'iResponse.StatusCode', iResponse.StatusCode. );
        SiMain.LogString( 'iResponse.StatusDescription', iResponse.StatusDescription );
        //SiMain.LogString( 'iResponse.Headers.Text', iResponse.Headers.Text );

        if iResponse.StatusCode = scOK then
        begin
          SiMain.LogInt64( 'ContentLength', iResponse.ContentLength );
          if iResponse.ContentLength > 0 then
          begin
            FResponseString := iResponse.ReadAsString;
            SiMain.LogVerbose( FResponseString );
            result := length( FResponseString ) > 0;
          end else
          begin
            { TODO 3 -ctodo : Mist� n�m� contentlength = 0 ongelmat oikeasti johtuvat? }
            SiMain.LogError( 'Response contentlength was 0!' );
            FResponseString := '';
            result := false;
          end;

          FResponseJSON := TJSONObject.create;
          FResponseJSON.Parse(BytesOf( FResponseString ), 0);
        end;
      end;

    finally
      iResponse.Free;
    end;

  finally
    iRequest.Free;
  end;
end;(* SafeGET *)

constructor TMagento2Api.Create(AOwner: TComponent);
begin
  inherited Create( aOwner );

  FAccessToken := '';
  FAccessTokenExpiresAt := IncYear( Now, -1 );
  FResponseJSON := nil;
//  FLastResponse := '';
//  FLastError := '';
end;(* Create *)

destructor TMagento2Api.Destroy;
begin
  FreeAndNil( FResponseJSON );

  inherited Destroy;
end;(* Destroy *)

function TMagento2Api.Connect( const aUserName, aPassword : string ) : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iBuff: TBytes;
begin
  result := false;
  iResponse := nil;
  FAccessTokenExpiresAt := IncYear( Now, -1 );

  iRequest := TScHttpWebRequest.Create( FMagentoURL + '/rest/V1/integration/admin/token' );
  try
    iRequest.ContentType := 'application/json';
    iRequest.Method := ScHttp.rmPOST;

    iBuff := TEncoding.UTF8.GetBytes( '{"username":"'+aUserName+'", "password":"'+aPassword+'"}' );
    iRequest.ContentLength := Length(iBuff);
    iRequest.WriteBuffer(iBuff);

    SiMain.LogVerbose( 'EXECUTE: '+ FMagentoURL + '/rest/V1/integration/admin/token' );

    try
      try
        iResponse := iRequest.GetResponse;
      except
        on E: HttpException do
        begin
          //if HttpException(E).StatusCode = scForbidden then
          SiMain.LogError( HttpException(E).ServerMessage +
                           ' Msg:' + HttpException(E).Message +
                           ' Trace:' + HttpException(E).StackTrace );
        end;
        on E: Exception do
          SiMain.LogError( E.Message );
      end;(* Except *)

      if iResponse <> nil then
      begin
        SiMain.LogString( 'iResponse.StatusDescription', iResponse.StatusDescription );
        //SiMain.LogString( 'iResponse.Headers.Text', iResponse.Headers.Text );

        if iResponse.StatusCode = scOK then
        begin
          FAccessToken := AnsiDequotedStr( iResponse.ReadAsString, '"' );
          SiMain.LogString( 'iAccessToken', FAccessToken );
          result := length( FAccessToken ) > 0;

          (* Expire accesstoken after two hours *)
          if result then
            FAccessTokenExpiresAt := IncHour( Now, 2 );
        end;

        SiMain.LogDateTime( 'Token will expire at', FAccessTokenExpiresAt );
      end;(* iResponse *)

    finally
      iResponse.Free;
    end;

  finally
    iRequest.Free;
  end;
end;(* Connect *)

function TMagento2Api.IsAccessTokenValid : boolean;
begin
  result := false;

  if length( FAccessToken ) > 0 then
    if FAccessTokenExpiresAt > now then
      result := true
    else
      SiMain.LogDebug( 'Token expired' );
end;(* IsAccessTokenValid *)

function TMagento2Api.Product( const aSKU : string ) : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iResponseString : string;
begin
  result := false;
  iResponse := nil;

  iRequest := TScHttpWebRequest.Create( FMagentoURL + '/rest/all/V1/products/'+ aSKU );
  try
    iRequest.ContentType := 'application/json';
    iRequest.Method := ScHttp.rmGET;
    iRequest.Headers.Add( 'Authorization', 'Bearer ' + FAccessToken );
    SiMain.LogVerbose( 'EXECUTE: '+ FMagentoURL + '/rest/all/V1/products/'+ aSKU );

    try
      try
        iResponse := iRequest.GetResponse;
      except
        on E: HttpException do
        begin
          //if HttpException(E).StatusCode = scForbidden then
          SiMain.LogError( HttpException(E).ServerMessage +
                           ' Msg:' + HttpException(E).Message +
                           ' Trace:' + HttpException(E).StackTrace );
        end;
        on E: Exception do
          SiMain.LogError( E.Message );
      end;(* Except *)

      if iResponse <> nil then
      begin
        //SiMain.LogInteger( 'iResponse.StatusCode', iResponse.StatusCode. );
        SiMain.LogString( 'iResponse.StatusDescription', iResponse.StatusDescription );
        //SiMain.LogString( 'iResponse.Headers.Text', iResponse.Headers.Text );

        if iResponse.StatusCode = scOK then
        begin
          iResponseString := iResponse.ReadAsString;
          SiMain.LogVerbose( iResponseString );
          result := length( iResponseString ) > 0;
        end;
      end;

    finally
      iResponse.Free;
    end;

  finally
    iRequest.Free;
  end;
end;(* Product *)

function TMagento2Api.Products( const aFromDateTime : TDateTime; const aFilterParams : string = '' ) : boolean;
var
  iFilterParams : string;
begin
  result := false;

  { TODO 1 -ctodo : implement requrest prepare
  PrepareRequest( rtXML, rtXML );              }
  FSearchCriteriaCurrentPage := 1;
  FSearchCriteriaTotalItemCount := 0;
  FSearchCriteriaTotalItemCounter := 0;

  if length( aFilterParams ) = 0 then
    FSearchCriteriaParameters := '?searchCriteria[filter_groups][0][filters][0][field]=updated_at'+
                                 '&searchCriteria[filter_groups][0][filters][0][value]='+ FormatMagentoDateTime( aFromDateTime ) +
                                 '&searchCriteria[filter_groups][0][filters][0][condition_type]=gt'
  else
    FSearchCriteriaParameters := aFilterParams;

  FSearchCriteriaParameters := 'products' + FSearchCriteriaParameters;
  result := SafeGET( {'products'+ }FSearchCriteriaParameters + IncludePageParameters );
end;(* Products *)

function TMagento2Api.ProductSynkkaChoices( const aFromDateTime : TDateTime ) : boolean;
begin
  result := Products( aFromDateTime, '?searchCriteria[filter_groups][0][filters][0][field]=synkka_ignore_update'+
                                     '&searchCriteria[filter_groups][0][filters][0][value]=1' +
                                     '&searchCriteria[filter_groups][0][filters][0][condition_type]=gt' +
                                        {OR}
                                     '&searchCriteria[filter_groups][0][filters][1][field]=synkka_add_to_description' +
                                     '&searchCriteria[filter_groups][0][filters][1][condition_type]=notnull');
end;(* ProductSynkkaChoices *)

function TMagento2Api.GetNextPage( var aItemsArray : TJSONArray ) : boolean;
(* FSearchCriteriaCurrentPage begins from zero, because Products-call has already retrieved first data page. *)
var
  iTempJSONValue : TJSONValue;
begin
  SiMain.EnterMethod( 'GetNextPage' );
  try
    result := false;
    aItemsArray := nil;

    (* did we already received everything? Check this only if totalitemcount is known *)
    if FSearchCriteriaTotalItemCount > 0 then
      if FSearchCriteriaTotalItemCounter >= FSearchCriteriaTotalItemCount then exit;

    if assigned( FResponseJSON ) then
    begin
      (* need to request more data? Do this after this after totalitemcount is known *)
      if FSearchCriteriaTotalItemCount > 0 then
      begin
        inc( FSearchCriteriaCurrentPage );
        result := SafeGET( FSearchCriteriaParameters + IncludePageParameters );
      end;

      (* get total count, if it is not known already *)
      if FSearchCriteriaTotalItemCount = 0 then
      begin
        iTempJSONValue := FResponseJSON.FindValue( 'total_count' );
        if assigned( iTempJSONValue ) then
        begin
          FSearchCriteriaTotalItemCount := StrToIntDef( iTempJSONValue.Value, 0 );
          SiMain.LogInteger( 'FSearchCriteriaTotalItemCount', FSearchCriteriaTotalItemCount );
        end else
        begin
          SiMain.LogError( 'TMagento2RestApi.GetNextPage:: total_count node was not found!' );
          result := false;
            raise Exception.Create( 'TMagento2RestApi.GetNextPage:: total_count node was not found!' );
        end;
      end;

      (* How many items did we got? *)
      aItemsArray := FResponseJSON.FindValue( 'items' ) as TJSONArray;
      if assigned( aItemsArray ) then
      begin
        if aItemsArray.Count > 0 then
          result := true;

        (* get total count *)
        inc( FSearchCriteriaTotalItemCounter, aItemsArray.Count );
        SiMain.LogInteger( 'iTempNode.ElementCount', aItemsArray.Count );
        SiMain.LogInteger( 'FSearchCriteriaTotalItemCounter', aItemsArray.Count );
      end;(* assigned *)
    end;(* assigned *)

  finally
    SiMain.LeaveMethod( 'GetNextPage' );
  end;
end;(* GetNextPage *)

function TMagento2Api.UpdateProductDescriptions( const aSKU : string; aDescription, aIngredients, aNutritional_values, aSynkka_Updated : string ) : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iResponseString : string;
  iBuff: TBytes;
begin
  result := false;
  iResponse := nil;

  iRequest := TScHttpWebRequest.Create( FMagentoURL + '/rest/all/V1/products' );
  try
    iRequest.ContentType := 'application/json';
    iRequest.Method := ScHttp.rmPOST;
    iRequest.Headers.Add( 'Authorization', 'Bearer ' + FAccessToken );
    SiMain.LogVerbose( 'EXECUTE: '+ FMagentoURL + '/rest/all/V1/products' );

    if length( aDescription ) = 0 then aDescription := ' ';
    if length( aIngredients ) = 0 then aIngredients := ' ';
    if length( aNutritional_values ) = 0 then aNutritional_values := ' ';

    iResponseString := '{"product": { "sku": "'+aSKU+'", "customAttributes": ['+
      '{ "attributeCode": "description", "value": "'+aDescription+'" },'+
      '{ "attributeCode": "ingredients", "value": "'+aIngredients+'" },'+
      '{ "attributeCode": "nutritional_values", "value": "'+ aNutritional_values +'" },'+
      '{ "attributeCode": "synkka_updated", "value": "'+ aSynkka_Updated +'" }]}}';
    SiMain.LogString( 'Payload', iResponseString );

    iBuff := TEncoding.UTF8.GetBytes( iResponseString );
    iRequest.ContentLength := Length(iBuff);
    iRequest.WriteBuffer(iBuff);

    try
      try
        iResponse := iRequest.GetResponse;
      except
        on E: HttpException do
          //if HttpException(E).StatusCode = scForbidden then
          SiMain.LogError( HttpException(E).ServerMessage +
                           ' Msg:' + HttpException(E).Message +
                           ' Trace:' + HttpException(E).StackTrace );
        on E: Exception do
          SiMain.LogError( E.Message );
      end;(* Except *)

      if iResponse <> nil then
      begin
        //SiMain.LogInteger( 'iResponse.StatusCode', iResponse.StatusCode. );
        SiMain.LogString( 'iResponse.StatusDescription', iResponse.StatusDescription );
        //SiMain.LogString( 'iResponse.Headers.Text', iResponse.Headers.Text );

        if iResponse.StatusCode = scOK then
        begin
          iResponseString := iResponse.ReadAsString;
          SiMain.LogVerbose( iResponseString );
          result := length( iResponseString ) > 0;
        end;
      end;(* iResponse *)

    finally
      iResponse.Free;
    end;

  finally
    iRequest.Free;
  end;
end;(* UpdateProductDescriptions *)

end.
