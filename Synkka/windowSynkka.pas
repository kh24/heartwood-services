unit windowSynkka;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MySQL,
  FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  System.Generics.Collections,
  SynkkaCollected,
  DBClient,
  system.json,

  rMagento2Api,
  SynkkaXMLfileProcessingU, SynkkaLocalDatabaseFunctionalitiesU, SynkkaDescriptionsUnit,
  SmartInspect.VCL,SmartInspect.VCL.SiAuto;

type
  TSynkkaMagentoRecord = record
    IgnoreUpdate : boolean;
    AddDescription : string;
  end;

  Twindow_Synkka = class(TForm)
    FDQuery_synkka: TFDQuery;
    FDConnection_synkka: TFDConnection;
    Edit_Host: TEdit;
    Edit_Port: TEdit;
    Edit_user: TEdit;
    Edit_Password: TEdit;
    Edit_Database: TEdit;
    Edit_Table: TEdit;
    Button_Connect: TButton;
    Button_Heartwood: TButton;
    FDQuery_Heartwood: TFDQuery;
    FDConnection_Heartwood: TFDConnection;
    Memo_Log: TMemo;
    Button_CSV_Verify: TButton;
    Button_UpdateOnSale: TButton;
    Button_ParseXML: TButton;
    btn_ExportCSV: TButton;
    btn_ExportMultiLanguage: TButton;
    Button_UpdateWaiting: TButton;
    btn_GetSynkkaChoices: TButton;
    btn_JsonTest: TButton;
    procedure Button_ConnectClick(Sender: TObject);
    procedure Button_HeartwoodClick(Sender: TObject);
    procedure Button_CSV_VerifyClick(Sender: TObject);
    procedure Button_ParseXMLClick(Sender: TObject);
    procedure btn_ExportCSVClick(Sender: TObject);
    procedure btn_ExportMultiLanguageClick(Sender: TObject);
    procedure Button_UpdateOnSaleClick(Sender: TObject);
    procedure Button_UpdateWaitingClick(Sender: TObject);
    procedure btn_JsonTestClick(Sender: TObject);
    procedure btn_GetSynkkaChoicesClick(Sender: TObject);
  private
    FMagento2Api : TMagento2Api;
    FSynkkaDescriptionsHandler : TSynkkaDescriptions;
    FListOfMissingTranslations : TStringList;
    function TranslatePackageLabels( const aText : string ) : string;
    function TranslateContainedNutrient( const aText : string ) : string;
    function TranslateContactTypeCode( const aText : string ) : string;
    class function getTradeItemMeasurementAsString(measurementNode, aListOfMissingTranslations: TStringList): string;
    function tradeItemNutritionalClaimDetailsAsString(tradeItem: TStringList): string;
    function GetFinnishTranslation( aTradeItem: TStringList; const ModuleToFind, keyToFind: string ) : string;
    procedure GetTranslations( aTradeItem: TStringList; const keyToFind: string; var aTranslationFI, aTranslationSV, aTranslationEN : string );
    function CompileDescription( aTradeItem : TStringList; const aHtmlEncode : boolean = false ) : string;
    function CompileAinesosat( aTradeItem : TStringList; const aHtmlEncode : boolean = false ) : string;
    function CompileRavintoarvot( aTradeItem : TStringList; const aHtmlEncode : boolean = false ) : string;
    function HtmlEncode(const AText: string): string;
    function HTMLProcessString( const aString : string; const aHtmlEncode : boolean = false ) : string;
    function RemoveSeparators( const aClearBuffer : string ) : string;
    function Magento2VerifyConnection : boolean;
    function Magento2ReadSynkkaChoices( var aProductData : TDictionary<string, TSynkkaMagentoRecord> ) : boolean;
    function Magento2Process( aProductData : TDictionary<string, TSynkkaMagentoRecord>;
                              const aSKU : string; aDescription, aIngredients, aNutritional_values: string ) : boolean;
  public
    procedure UpdateProductFromSynkkaToMagento2( aQuery : TFDQuery );
    procedure ExportMultilanguageToCSV( const aHtmlEncode : boolean = false );
  end;

var
  window_Synkka: Twindow_Synkka;

implementation

{$R *.dfm}

uses
  gFieldNames,
  USynkkaXMLparserUnit,
  RelaxXML;

const
  html_paragraph_begin = '<p>';
  html_paragraph_end = '</p>';
  html_paragraph_enter = '</br>';
  html_list_begin = '<ul>';
    html_listitem_begin = '<li>';
    html_listitem_end = '</li>';
  html_list_end = '</ul>';
  html_separator = ' / ';
  html_bold_begin = '<strong>';
  html_bold_end = '</strong>';

  csv_separator = ';';
  csv_delimiter = '"';

function Twindow_Synkka.TranslatePackageLabels( const aText : string ) : string;
begin
  if aText = 'Parasta ennen -p�iv�ys' then result := aText + ' /B�st f�re datum'
  else if aText = 'Hyv�� Suomesta (Sininen Joutsen)' then result := aText + ' /H�lsningar fr�n Finland (Bl� svanen)'

  else if aText = 'Pohjoismainen ymp�rist�merkki (Joutsen)' then result := aText + ' /Svanenm�rkt (svanen)'
  else if aText = 'Valmistusp�iv�' then result := aText + ' /Tillverkningsdatum'
  else if aText = 'Virallinen luomumerkki (Aurinko)' then result := aText + ' /Officiell ekologisk m�rkning (Sun)'
  else if aText = 'Krav Merkki' then result := aText + ' /Krav Mark'
  else if aText = 'MSC-sertifikaatti (Marine Stewardship council)' then result := aText + ' /MSC-certifikat (Marine Stewardship Council)'
  else if aText = 'Avainlippu' then result := aText + ' /Nyckelbiljett'
  else if aText = 'Rainforest alliance certified (Sammakko-merkki)' then result := aText + ' /Rainforest Alliance certifierad (grodm�rke)'
  else if aText = 'Suojattu alkuper�nimitys' then result := aText + ' /Skyddad ursprungsbeteckning'
  else if aText = 'EU:n luomutunnus (lehti)' then result := aText + ' /EU:s ekologiska kod (tidning)'
  else if aText = 'FSC-sertifikaatti (Forest Stewardship Council)' then result := aText + ' /FSC-certifikat (Forest Stewardship Council)'
  else if aText = 'Reilu Kauppa' then result := aText + ' /R�ttvis handel'
  else if aText = 'Kierr�tysmerkki' then result := aText + ' /�tervinning symbol'
  else if aText = 'Viimeinen k�ytt�p�iv�' then result := aText + ' /Utg�ngsdatum'
  else if aText = 'Syd�nmerkki' then result := aText + ' /Hj�rtat tecken'
  else if aText = 'Viimeinen myyntip�iv�' then result := aText + ' /Sista readagen'
  else if aText = 'Pakkausp�iv�' then result := aText + ' /Packningsdatum'
  else if aText = 'Allergia- ja astmaliiton merkki' then result := aText + ' /Allergi- och astmaf�rbundets m�rke'
  else if aText = 'Cosmebio' then result := aText
  else if aText = 'Vegan - VegeCert' then result := aText
  else if aText = 'NaTrue Label' then result := aText
  else if aText = 'UTZ_certification' then result := aText
  else if aText = 'Green Dot' then result := aText
  else if aText = 'Laatuvastuu' then result := aText + ' /Kvalitetsansvar'
  else if aText = 'Gluteeniton elintarvike (yliviivattu t�hk�)' then result := aText + ' /Glutenfri mat (�verstruken majs)'
  else if aText = 'Ecologo (Pohjois-Amerikka)' then result := aText + ' /Ecologo (Nordamerika)'
  else if aText = 'Soil Association -merkki' then result := aText + ' /Markf�reningens m�rkning'
  else if aText = '�-m�rke' then result := aText
  else if aText = 'NON_GMO_PROJECT' then result := aText
  else if aText = 'USDA_ORGANIC' then result := aText
  else if aText = 'VEGAN_SOCIETY_VEGAN_LOGO' then result := aText
  else if aText = 'AQUACULTURE_STEWARDSHIP_COUNCIL' then result := aText
  else if aText = 'Organic Certifying Body � Debio' then result := aText
  else if aText = 'DOLPHIN_SAFE' then result := aText
  else if aText = 'Nyckelhalet' then result := aText
  else
  begin
    result := aText;

    if FListOfMissingTranslations.IndexOf( 'TranslatePackageLabels: '+ aText ) = -1 then
    begin
      FListOfMissingTranslations.Add( 'TranslatePackageLabels: '+ aText );
      SiMain.LogWarning( 'Untranslated TranslatePackageLabels "'+ aText +'" !' );
    end;
  end;
end;(* TranslatePackageLabels *)

function Twindow_Synkka.TranslateContainedNutrient( const aText : string ) : string;
(* Nutriens
   https://gs1.se/en/guides/documentation/code-lists/t4073-nutrient-type-code/
*)
begin
  if aText = 'Energia' then result := aText +'/ Energi'
  else if aText = 'Rasvaa' then result := aText +'/ Fett '
  else if aText = 'Rasvaa, josta tyydyttyneit� rasvoja' then result := aText +'/ Fett, inklusive m�ttat fett'
  else if aText = 'Rasvaa, josta kertatyydyttym�tt�mi� rasvoja' then result := aText +'/ Fett, varav enkelom�ttade fetter'
  else if aText = 'Rasvaa, josta monityydyttym�tt�mi� rasvoja' then result := aText +'/ Fett, varav flerom�ttade fetter'
  else if aText = 'Hiilihydraattia' then result := aText +'/ Kolhydrater'
  else if aText = 'Sokeria' then result := aText +'/ varav sockerarter'
  else if aText = 'Ravintokuitua' then result := aText +'/ Kostfiber'
  else if aText = 'Proteiinia' then result := aText +'/ Protein'
  else if aText = 'Suola' then result := aText +'/ Salt'
  else if aText = 'Kalsium' then result := aText +'/ Kalcium'
  else if aText = 'Kalium' then result := aText +'/ Kalium'
  else if aText = 'Natrium' then result := aText +'/ Natrium'
  else if aText = 'Hiilihydraattia, josta laktoosia' then result := aText +'/ Kolhydrat, varav laktos'
  else if aText = 'C-vitamiini' then result := aText +'/ C-vitamin'
  else if aText = 'Fosfori' then result := aText +'/ Fosfor'
  else if aText = 'Rauta' then result := aText +'/ J�rn'
  else if aText = 'Hiilihydraattia josta sokerialkoholeja (polyolit)' then result := aText +'/ Kolhydrater fr�n vilka sockeralkoholer (polyoler)'
  else if aText = 'K-vitamiini' then result := aText +'/ Vitamin K'
  else if aText = 'Kupari' then result := aText +'/ Koppar'
  else if aText = 'Mangaani' then result := aText +'/ Mangan'
  else if aText = 'B6-vitamiini' then result := aText +'/ Vitamin B6'
  else if aText = 'D-vitamiini' then result := aText +'/ Vitamin D'
  else if aText = 'B12-vitamiini' then result := aText +'/ Vitamin B12'
  else if aText = 'E-vitamiini' then result := aText +'/ Vitamin E'
  else if aText = 'Niasiini' then result := aText +'/ Niacin'
  else if aText = 'Tiamiini' then result := aText +'/ Tiamin'
  else if aText = 'Riboflaviini' then result := aText +'/ Riboflavin'
  else if aText = 'Pantoteenihappo' then result := aText +'/ Pantotensyra'
  else if aText = 'Biotiini' then result := aText +'/ Biotin'
  else if aText = 'A-vitamiini' then result := aText +'/ Vitamin A'
  else if aText = 'Magnesium' then result := aText +'/ Magnesium'
  else if aText = 'Seleeni' then result := aText +'/ Selen'
  else if aText = 'Sinkki' then result := aText +'/ Zink'
  else if aText = 'Foolihappo' then result := aText +'/ Folsyra'
  else if aText = 'Hiilihydraattia josta t�rkkelyst�' then result := aText +'/ En kolhydrat fr�n vilken st�rkelse'
  else if aText = 'Jodi' then result := aText +'/ Jod'
  else if aText = 'Kloridi' then result := aText +'/ Klorid'
  else if aText = 'Beeta-karoteeni' then result := aText +'/ Betakaroten'
  else if aText = 'Betaglukaani' then result := aText +'/ Betaglukaani'
  else if aText = 'CHOL-' then result := 'Fosfatidyylikoliini / Fosfatidylkolin'
  else if aText = 'FAPUN3F' then result := 'Rasva, monityydyttym�t�n - Omega 3 -hapot / Fett, flerom�ttade- Omega 3-syror'
  else if aText = 'Beeta-' then result := aText +'/ Beta-'
  else if aText = 'Fluori' then result := aText +'/ Fluor'
  else if aText = 'FOLDFE-' then result := 'Foolihappo / Folsyra'
  else if aText = 'Karnitiini' then result := aText +'/ Karnitin'
  else if aText = 'Kofeiini' then result := aText +'/ Koffein'
  else if aText = 'Kromi' then result := aText +'/ Krom'
  else if aText = 'Molybdenum' then result := aText +'/ Molybden'
  else if aText = 'Nukleotidi' then result := aText +'/ Nukleotider'
  else if aText = 'Oligosakkarideja' then result := aText +'/ Oligosackarider'
  else if aText = 'Rasvaa josta kolesterolia' then result := aText +'/ Fett fr�n vilket kolesterol'
  else if aText = 'Rasvaa josta monityydyttym�tt�mia rasvahappoja' then result := aText +'/ Fett fr�n flerom�ttade fettsyror'
  else if aText = 'Tauriini' then result := aText +'/ Taurin'
  else if aText = 'VITC-' then result := 'C-vitamiini / C-vitamin'
  else if aText = 'VITE-' then result := 'E-vitamiini / Vitamin E'
  else
    if FListOfMissingTranslations.IndexOf( 'nutrient: '+ aText ) = -1 then
    begin
      FListOfMissingTranslations.Add( 'nutrient: '+ aText );
      SiMain.LogWarning( 'Untranslated nutrient "'+ aText +'" !' );
    end;
end;(* TranslateContainedNutrient *)

function Twindow_Synkka.TranslateContactTypeCode( const aText : string ) : string;
(* HAE SIS�LT�:
    SELECT GdsnCode, CodeListName, FinnishDescription, EnglishDefinition, EnglishDescription
    from attributecodes
    WHERE GdsnCode = 'BVP'

    Contact type
    https://gs1.se/en/guides/documentation/code-lists/t3792-contact-type-code/
*)
begin
  if aText = 'DIS' then result := 'J�lleenmyyj� /�terf�rs�ljare' //Distributor
  else if aText = 'BVP' then result := 'Tuotantolaitos /Fabrik' //Production Facility
  else if aText = 'BZL' then result := 'Lisenssinhaltija rekisterinpit�j� /Licenstagarens kontrollant' //	Licensee Registrar
  else if aText = 'CXC' then result := 'Kuluttajatuki /Konsumentst�d' //	Consumer Support
  else if aText = 'IMP' then result := 'Maahantuoja /Import�r' //Importer
  else if aText = 'LO' then result := 'Noutopaikan yhteystiedot /Kontaktuppgifter till upph�mtningsst�llet' //Place of collection contact
  else if aText = 'MAN' then result := 'Valmistaja /Tillverkare' //	Manufacturer
  else if aText = 'PM' then result := 'Tuotehallinnan yhteyshenkil� /Kontaktperson f�r produktledning' //Product management contact
  else if aText = 'SA' then result := 'Myynnin hallinto /F�rs�ljningsadministration' //Sales administration

  else if aText = 'CR' then result := 'Asiakassuhteet /Kundrelationer'
  else if aText = 'CYC' then result := 'Asiakaspalvelu /Kundservice'
  else if aText = 'DSU' then result := '-' { TODO 1 : p�ivit� attributecodes saadaksesi n�ihin vastaukset }
  else if aText = 'PAC' then result := 'Pakkaaja /Packare'
  else if aText = 'PRF' then result := 'tuottaja varten /producent f�r'
  else
  begin
    result := aText;
    if FListOfMissingTranslations.IndexOf( 'ContactTypeCode: '+ aText ) = -1 then
    begin
      FListOfMissingTranslations.Add( 'ContactTypeCode: '+ aText );
      SiMain.LogWarning( 'Untranslated ContactTypeCode "'+ aText +'" !' );
    end;
  end;
end;(* TranslateContactTypeCode *)

class function Twindow_Synkka.getTradeItemMeasurementAsString(measurementNode, aListOfMissingTranslations: TStringList): string;
(* FIND VALUES FROM
    SELECT GdsnCode, CodeListName, FinnishDescription, EnglishDefinition, EnglishDescription
    from attributecodes
    WHERE GdsnCode = 'MGM'

    Measurement unit code
    https://gs1.se/en/guides/documentation/code-lists/t3780-measurement-unit-code/
*)
var
  iMeasurementUnitCode : string;
begin
  try
    Result := '';

    //SiMain.LogStringList( 'getTradeItemMeasurementAsString', measurementNode );

    (* translate measurement unit code *)
    iMeasurementUnitCode := measurementNode.Values['measurementUnitCode'];
    if iMeasurementUnitCode = 'GRM' then iMeasurementUnitCode := 'g' //gramma
    else if iMeasurementUnitCode = 'E14' then iMeasurementUnitCode := 'kcal' //Kilokalori
    else if iMeasurementUnitCode = 'KJO' then iMeasurementUnitCode := 'kJ' //Kilojoule
    else if iMeasurementUnitCode = 'MGM' then iMeasurementUnitCode := 'mg' //Milligramma
    else if iMeasurementUnitCode = 'MC' then iMeasurementUnitCode := 'ug' //Mikrogramma
    else if iMeasurementUnitCode = 'MLT' then iMeasurementUnitCode := 'ml' //Millilitra
    else if iMeasurementUnitCode = 'H87' then iMeasurementUnitCode := 'kpl' //Piece
    else if iMeasurementUnitCode = 'GL' then iMeasurementUnitCode := 'GL' //Grammaa per litra
    else if iMeasurementUnitCode = 'NIU' then iMeasurementUnitCode := 'NIU' //Kansainv�linen yksikk�
    else if iMeasurementUnitCode = 'P1' then iMeasurementUnitCode := '%' //Prosentti
    else if iMeasurementUnitCode = 'PTN' then iMeasurementUnitCode := 'Annos' //Portion
    else if iMeasurementUnitCode = 'SET' then iMeasurementUnitCode := 'Setti' //Set
    else
      if aListOfMissingTranslations.IndexOf( 'measurement: '+iMeasurementUnitCode ) = -1 then
      begin
        aListOfMissingTranslations.Add( 'measurement: '+iMeasurementUnitCode );
        SiMain.LogWarning( 'Untranslated Measurement "'+ iMeasurementUnitCode +'" !' );
      end;
    if (measurementNode <> nil) then Result := measurementNode.Values['measurementValue'] + ' ' + iMeasurementUnitCode;
  except
    on E : Exception do begin
      // error
      SiMain.LogException(E, 'cant get trade item measurements as string');
    end;
  end;
end;(* getTradeItemMeasurementAsString *)


function Twindow_Synkka.tradeItemNutritionalClaimDetailsAsString(tradeItem: TStringList): string;
var
  nodeObject: TStringList;
  nodeObjects: TObjectList<TStringList>;
  first: boolean;
  nutritionalClaimTypeCode, nutritionalClaimNutrientElementCode: string;
begin
  Result := '';
  first := true;
  nodeObjects := TSynkkaXMLparser.getAllObjectsForKeyFromStringList(tradeItem, 'nutritionalClaimDetail');
  try
    try
      for nodeObject in nodeObjects do begin
        if ((length(nodeObject.Values['nutritionalClaimTypeCode']) > 0) or (length(nodeObject.Values['nutritionalClaimNutrientElementCode']) > 0)) then begin
          if (first) then first := false else Result := Result + ', ';
          nutritionalClaimTypeCode := FSynkkaDescriptionsHandler.getFinnishDescrpitionForAttributeValue(nodeObject.Values['ModuleName'], 'NutritionalClaimTypeCode', nodeObject.Values['nutritionalClaimTypeCode']);
          if not (length(nutritionalClaimTypeCode) > 0) then nutritionalClaimTypeCode := nodeObject.Values['nutritionalClaimTypeCode'];
          nutritionalClaimNutrientElementCode := FsynkkaDescriptionsHandler.getFinnishDescrpitionForAttributeValue(nodeObject.Values['ModuleName'], 'NutritionalClaimNutrientElementCode', nodeObject.Values['nutritionalClaimNutrientElementCode']);
          if not (length(nutritionalClaimNutrientElementCode) > 0) then nutritionalClaimNutrientElementCode := nodeObject.Values['nutritionalClaimNutrientElementCode'];
          Result := Result + nutritionalClaimTypeCode + ': ' + nutritionalClaimNutrientElementCode;
        end;
      end;
    except
      on E : Exception do begin
        SiMain.LogException(E, 'cant get nutritional claim details as string');
      end;
    end;
  finally
    if (nodeObjects <> nil) then nodeObjects.Free;
  end;
end;(* tradeItemNutritionalClaimDetailsAsString *)

function Twindow_Synkka.GetFinnishTranslation( aTradeItem: TStringList; const ModuleToFind, keyToFind: string ) : string;
var
  nodeObject: TStringList;
  translatedValue, countryCode: string;
begin
  result := '';

  nodeObject := TSynkkaXMLparser.getAllStringValuesForKeyFromStringList( aTradeItem, keyToFind );
  try
    try
       if (nodeObject <> nil) then
       begin
         for countryCode in nodeObject do
         begin
           { TODO 4 -ctodo : Varmista, ett� UTF8 conversio on t�ss� tarpeen }
           translatedValue := UTF8ToString( FSynkkaDescriptionsHandler.getFinnishDescrpitionForAttributeValue( ModuleToFind, KeyToFind, countryCode));
           if not (length(translatedValue) > 0) then translatedValue := countryCode;
           if length( result ) > 0 then result := result + ',';
           result := result + translatedValue;
         end;
      end;

    except
      on E : Exception do begin
        // error
        SiMain.LogException(E, 'cant get all string values from list');
      end;
    end;

  finally
    if (nodeObject <> nil) then nodeObject.Free;
  end;
end;(* GetFinnishTranslation *)

procedure Twindow_Synkka.GetTranslations( aTradeItem: TStringList; const keyToFind: string; var aTranslationFI, aTranslationSV, aTranslationEN : string );
var
  translationObject: TStringList;
  nodeObjects: TObjectList<TStringList>;
begin
  aTranslationFI := '';
  aTranslationSV := '';
  aTranslationEN := '';

  nodeObjects := TSynkkaXMLparser.getAllObjectsForKeyFromStringList( aTradeItem, keyToFind);
  try
    try
      if (nodeObjects.Count > 0) then begin
        for translationObject in nodeObjects do begin
          if (length(translationObject.Values['languageCode']) > 0) then begin
            if (translationObject.Values['languageCode'] = 'fi') then begin
              if length( aTranslationFI ) > 0 then aTranslationFI := aTranslationFI + ', ';
              aTranslationFI := aTranslationFI + UTF8ToString( translationObject.Values['translation']);
            end;
            if (translationObject.Values['languageCode'] = 'sv') then begin
              if length( aTranslationSV ) > 0 then aTranslationSV := aTranslationSV + ', ';
              aTranslationSV := aTranslationSV + UTF8ToString( translationObject.Values['translation']);
            end;
            if (translationObject.Values['languageCode'] = 'en') then begin
              if length( aTranslationEN ) > 0 then aTranslationEN := aTranslationEN + ', ';
              aTranslationEN := aTranslationEN + UTF8ToString( translationObject.Values['translation']);
            end;
          end;
        end;
      end;

    except
      on E : Exception do begin
        SiMain.LogException(E, ' cant set translations for csv table');
        exit;
      end;
    end;

  finally
    if (nodeObjects <> nil) then nodeObjects.Free;
  end;
end;(* GetTranslations *)


function Twindow_Synkka.CompileDescription( aTradeItem : TStringList; const aHtmlEncode : boolean = false ) : string;
var
  nodeObject, nodeObject2: TStringList;
  tradeItemDateOnPackagingTypeCode, tradeItemDateOnPackagingTypeCodeValue, tradeItemDateOnPackagingTypeCodes,
  packagingMarkedLabelAccreditationCode, packagingMarkedLabelAccreditationCodeValue, packagingMarkedLabelAccreditationCodes: string;
  iTranslationFI, iTranslationSV, iTranslationEN : string;
begin
  result := '';

  // Tuotekuvaus -> tuotenimi 3318 suomi  ja ruotsi
  GetTranslations( aTradeItem, 'tradeItemDescription', iTranslationFI, iTranslationSV, iTranslationEN );
    result := html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_separator +
                                   HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;

  // Markkinointi -> markkinointiteksti 1083 suomi  ja ruotsi
  GetTranslations( aTradeItem, 'tradeItemMarketingMessage', iTranslationFI, iTranslationSV, iTranslationEN );
  if length( iTranslationFI ) > 0 then
    result := result + html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end;
  if length( iTranslationSV ) > 0 then
    result := result + html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;

  //S�ILYTYS-, KIERR�TYS- JA K�YTT�OHJEET/ F�RVARING, �TERVINNING INSTRUKTIONER OCH ANV�NDARINSTRUKTIONER
  //Kuluttajaohjeet 352 + muut tarvittavat synkasta 955, 356,4659
  GetTranslations( aTradeItem, 'consumerStorageInstructions', iTranslationFI, iTranslationSV, iTranslationEN );
  if ( length( iTranslationFI ) > 0 ) or ( length( iTranslationSV ) > 0 ) then
    result := result + html_paragraph_begin + html_bold_begin + HTMLProcessString( 'S�ILYTYSOHJEET/ F�RVARING INSTRUKTIONER', aHtmlEncode ) + html_bold_end + html_paragraph_end +
       html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_separator +
                              HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;

  GetTranslations( aTradeItem, 'consumerUsageInstructions', iTranslationFI, iTranslationSV, iTranslationEN );
  if ( length( iTranslationFI ) > 0 ) or ( length( iTranslationSV ) > 0 ) then
    result := result + html_paragraph_begin + html_bold_begin + HTMLProcessString( 'K�YTT�OHJEET/ ANV�NDARINSTRUKTIONER', aHtmlEncode ) + html_bold_end + html_paragraph_end +
            html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end +
            html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;

  //1017 Pakolliset lis�huomautukset kuluttajalle
  nodeObject := TSynkkaXMLparser.getObjectFromStringListByKey( aTradeItem, 'HealthRelatedInformation');
  try
    if nodeObject <> nil then
    begin
      GetTranslations( nodeObject, 'compulsoryAdditiveLabelInformation', iTranslationFI, iTranslationSV, iTranslationEN );
      if ( length( iTranslationFI ) > 0 ) or ( length( iTranslationSV ) > 0 ) then
        result := result + html_paragraph_begin + html_bold_begin + HTMLProcessString( 'LIS�HUOMAUTUKSET/ YTTERLIGARE ANM�RKNINGAR', aHtmlEncode ) + html_bold_end + html_paragraph_end +
                html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end +
                html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;
      GetTranslations( nodeObject, 'healthClaimDescription', iTranslationFI, iTranslationSV, iTranslationEN );
      if ( length( iTranslationFI ) > 0 ) or ( length( iTranslationSV ) > 0 ) then
        result := result + html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end +
                           html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;
    end;

  finally
    nodeObject.Free;
  end;

  //4659 Kierr�tysohje kuluttajalle
  GetTranslations( aTradeItem, 'consumerRecyclingInstructions', iTranslationFI, iTranslationSV, iTranslationEN );
  if ( length( iTranslationFI ) > 0 ) or ( length( iTranslationSV ) > 0 ) then
    result := result + html_paragraph_begin + html_bold_begin + HTMLProcessString( 'KIERR�TYSOHJEET/ �TERVINNINGSINSTRUKTIONER', aHtmlEncode ) + html_bold_end + html_paragraph_end +
            html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end +
            html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;

  iTranslationFI := GetFinnishTranslation( aTradeItem, 'PlaceOfItemActivityModule', 'countryCode' );
  if ( length( iTranslationFI ) > 0 ) then
    result := result + html_paragraph_begin + html_bold_begin + HTMLProcessString( 'ALKUPER�MAA/ VALMISTUSMAA/ URSPRUNGSLAND/TILLVERKNINGSLAND', aHtmlEncode ) + html_bold_end + html_paragraph_end +
              html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end;

  //3182 Yhteystyyppi J�lleenmyyj�
  //3195 Yrityksen nimi Valio Oy
  // lis�sin alla olevat
  // <tradeItemContactInformation>
  //                    <contactTypeCode>DIS</contactTypeCode>
  //                    <contactName>Valio Oy</contactName>
  iTranslationFI := UTF8ToString( aTradeItem.Values[ 'tradeItemContactInformationContactName' ]);
  iTranslationSV := TranslateContactTypeCode( UTF8ToString( aTradeItem.Values[ 'tradeItemContactInformationContactTypeCode' ]));
  if length( iTranslationFI ) > 0 then
  begin
    result := result + html_paragraph_begin + html_bold_begin + HTMLProcessString( 'MAAHANTUOJA / IMPORT�R', aHtmlEncode ) + html_bold_end + html_paragraph_end;
    //3182 Yhteystyyppi J�lleenmyyj�
    if length( iTranslationSV ) > 0 then
      result := result + html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;
    //3195 Yrityksen nimi Valio Oy
    if length( iTranslationFI ) > 0 then
      result := result + html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end;
  end;

  // lis�sin alla olevat
  // <informationProviderOfTradeItem>
  //                  <partyName>Valio Oy</partyName>
  iTranslationFI := UTF8ToString( aTradeItem.Values[ 'informationProviderOfTradeItemPartyName' ]);
  if length( iTranslationFI ) > 0 then
  begin
    result := result + html_bold_begin + html_paragraph_begin + HTMLProcessString( 'VALMISTAJA/ TILLVERKARE', aHtmlEncode ) + html_bold_end + html_paragraph_end +
              html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end;
  end;

  //PAKKAUSMERKINN�T/ F�RPACKNINGSETIKETTER
  iTranslationFI := '';
  iTranslationSV := '';
  nodeObject := TSynkkaXMLparser.getObjectFromStringListByKey( aTradeItem, '  ');
  try
    try
      if not (nodeObject <> nil) then exit;
      // packagingMarking -> 0..n pcs tradeItemDateOnPackagingTypeCode
      tradeItemDateOnPackagingTypeCodes := '';
      nodeObject2 := TSynkkaXMLparser.getAllStringValuesForKeyFromStringList(nodeObject, 'tradeItemDateOnPackagingTypeCode');
      try
        if ((nodeObject2 <> nil) and (nodeObject2.Count > 0)) then
        begin
          for tradeItemDateOnPackagingTypeCodeValue in nodeObject2 do
          begin
            tradeItemDateOnPackagingTypeCode := TranslatePackageLabels( UTF8ToString( FsynkkaDescriptionsHandler.getFinnishDescrpitionForAttributeValue(nodeObject.Values['ModuleName'], 'TradeItemDateOnPackagingTypeCode', tradeItemDateOnPackagingTypeCodeValue)));
            if length( tradeItemDateOnPackagingTypeCodes) > 0 then
              tradeItemDateOnPackagingTypeCodes := tradeItemDateOnPackagingTypeCodes + ', ';
            tradeItemDateOnPackagingTypeCodes := tradeItemDateOnPackagingTypeCodes + tradeItemDateOnPackagingTypeCode;
          end;
          iTranslationFI := tradeItemDateOnPackagingTypeCodes;
        end;
      finally
        if (nodeObject2 <> nil) then nodeObject2.Free;
      end;

      // packagingMarking -> 0..n pcs packagingMarkedLabelAccreditationCode
      nodeObject2 := TSynkkaXMLparser.getAllStringValuesForKeyFromStringList(nodeObject, 'packagingMarkedLabelAccreditationCode');
      try
        if ((nodeObject2 <> nil) and (nodeObject2.Count > 0)) then
        begin
          packagingMarkedLabelAccreditationCodes := '';
          for packagingMarkedLabelAccreditationCodeValue in nodeObject2 do
          begin
            packagingMarkedLabelAccreditationCode := TranslatePackageLabels(UTF8ToString( FsynkkaDescriptionsHandler.getFinnishDescrpitionForAttributeValue(nodeObject.Values['ModuleName'], 'PackagingMarkedLabelAccreditationCode', packagingMarkedLabelAccreditationCodeValue)));
            if length( packagingMarkedLabelAccreditationCodes ) > 0 then
              packagingMarkedLabelAccreditationCodes := packagingMarkedLabelAccreditationCodes + ', ';
            packagingMarkedLabelAccreditationCodes := packagingMarkedLabelAccreditationCodes + packagingMarkedLabelAccreditationCode;
          end;
          iTranslationSV := packagingMarkedLabelAccreditationCodes;
        end;
      finally
        if (nodeObject2 <> nil) then nodeObject2.Free;
      end;

    if (length( iTranslationFI ) > 0) or (length( iTranslationSV ) > 0 ) then
    begin
      result := result + html_paragraph_begin + html_bold_begin + HTMLProcessString( 'PAKKAUSMERKINN�T/ F�RPACKNINGSETIKETTER', aHtmlEncode ) + html_bold_end + html_paragraph_end;
      if length( iTranslationFI ) > 0 then result := result + html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end;
      if length( iTranslationSV ) > 0 then result := result + html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;
    end;

    finally
      nodeObject.Free;
    end;

  except
    on E : Exception do
    begin
      SiMain.LogException(E, ' cant get list by packaging marking');
      exit;
    end;
  end;
end;(* CompileDescription *)

function Twindow_Synkka.CompileAinesosat( aTradeItem : TStringList; const aHtmlEncode : boolean = false ) : string;
var
  nodeObject : TStringList;
  iTranslationFI, iTranslationSV, iTranslationEN : string;
begin
  result := '';

  { TODO 3 -ctodo : Allergeeni sanat t�ytyy laittaa isoilla kirjaimilla}
  try
    (* food *)
    nodeObject := TSynkkaXMLparser.getObjectFromStringListByKey( aTradeItem, 'foodAndBeverageIngredientModule');
    try
      if nodeObject <> nil then
      begin
        GetTranslations( nodeObject, 'ingredientStatement', iTranslationFI, iTranslationSV, iTranslationEN );
        if length( iTranslationFI ) > 0 then
          result := result + html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end +
                             html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;
      end;

    finally
      nodeObject.Free;
    end;

    (* none food *)
    nodeObject := TSynkkaXMLparser.getObjectFromStringListByKey( aTradeItem, 'nonfoodIngredientModule');
    try
      if nodeObject <> nil then
      begin
        GetTranslations( nodeObject, 'nonfoodIngredientStatement', iTranslationFI, iTranslationSV, iTranslationEN );
        if length( iTranslationFI ) > 0 then
          result := result + html_paragraph_begin + HTMLProcessString( iTranslationFI, aHtmlEncode ) + html_paragraph_end +
                             html_paragraph_begin + HTMLProcessString( iTranslationSV, aHtmlEncode ) + html_paragraph_end;
      end;

    finally
      nodeObject.Free;
    end;

  except
    on E : Exception do begin
      // error
      SiMain.LogException(E, 'cant get food and beverage ingredients to csv table');
    end;
  end;
end;(* CompileAinesosat *)

function Twindow_Synkka.CompileRavintoarvot( aTradeItem : TStringList; const aHtmlEncode : boolean = false ) : string;
//tradeItemNutritionalInformationModuleToCSVtable
var
  nodeObject, nodeObject2, nodeObject3, servingSize: TStringList;
  nodeObjects, servingSizes, nodeObjects2, nodeObjects3: TObjectList<TStringList>;
  quantityContained, containedNutrient, allContainedNutrients, preparationStateCode, servingSizeString: string;
  iTranslationFI, iTranslationSV, iTranslationEN : string;
begin
  result := '';

  iTranslationFI := '';
  iTranslationSV := '';
  iTranslationEN := '';

  //Ei sis�ll�: V�ri lis�aineena, V�h�n: Natrium tai suola, Ei sis�ll�: S�il�nt�aine, Ei sis�ll�: Makeutusaineita, Ei sis�ll�: Lis�aineita, Ei sis�ll�: Laktoosi, Ei lis�tty�: Sokereita
//  try
//    result := html_paragraph_begin + UTF8ToString( tradeItemNutritionalClaimDetailsAsString( aTradeItem)) + html_paragraph_end;
//  except
//    on E : Exception do
//      SiMain.LogException(E, 'cant get column config by key');
//  end;

  nodeObjects := TSynkkaXMLparser.getAllObjectsForKeyFromStringList(aTradeItem, 'nutrientHeader');
  try
    try
      if not ((nodeObjects <> nil) and (nodeObjects.Count > 0)) then exit;

      for nodeObject in nodeObjects do
      begin
//        if (length(nodeObject.Values['preparationStateCode']) > 0) then
//        begin
//          result := result + html_paragraph_begin + 'preparationStateCode: '+
//                    UTF8ToString( FsynkkaDescriptionsHandler.getFinnishDescrpitionForAttributeValue(nodeObject.Values['ModuleName'], 'PreparationTypeCode', nodeObject.Values['preparationStateCode'])) +
//                    html_paragraph_end;
//        end;

        try
          servingSizeString := '';
          servingSizes := TSynkkaXMLparser.getAllObjectsForKeyFromStringList(nodeObject, 'servingSize');
          try
            if (servingSizes.Count > 0) then begin
              for servingSize in servingSizes do begin
                if length( servingSizeString ) > 0 then servingSizeString := servingSizeString + ', ';
                servingSizeString := servingSizeString + getTradeItemMeasurementAsString(servingSize, FListOfMissingTranslations);
              end;

              // Ravintosis�lt� 100 g:sta/ N�ringsv�rde per 100 g
              if length( servingSizeString ) > 0 then
                result := result + html_paragraph_begin +
                          HTMLProcessString( 'Ravintosis�lt� '+ UTF8ToString( servingSizeString ) + ':sta/ N�ringsv�rde per '+
                                             UTF8ToString( servingSizeString ),aHtmlEncode ) + html_paragraph_end;
            end;
          finally
            servingSizes.Free;
          end;
        except
          on E : Exception do
            SiMain.LogException(E, 'cant get all objects for key from list');
        end;

        (* if servicesizestring is missing, use nutrientBasisQuantity instead *)
        if length( servingSizeString ) = 0 then
        begin
          try
            servingSizeString := '';
            servingSizes := TSynkkaXMLparser.getAllObjectsForKeyFromStringList(nodeObject, 'nutrientBasisQuantity');
            try
              if (servingSizes.Count > 0) then begin
                for servingSize in servingSizes do begin
                  if length( servingSizeString ) > 0 then servingSizeString := servingSizeString + ', ';
                  servingSizeString := servingSizeString + getTradeItemMeasurementAsString(servingSize, FListOfMissingTranslations);
                end;

                // Ravintosis�lt� 100 g:sta/ N�ringsv�rde per 100 g
                if length( servingSizeString ) > 0 then
                  result := result + html_paragraph_begin +
                            HTMLProcessString( 'Ravintosis�lt� '+ UTF8ToString( servingSizeString ) + ':sta/ N�ringsv�rde per '+
                                               UTF8ToString( servingSizeString ),aHtmlEncode ) + html_paragraph_end;
              end;
            finally
              servingSizes.Free;
            end;
          except
            on E : Exception do
              SiMain.LogException(E, 'cant get all objects for key from list');
          end;
        end;

        // nutrientDetail
        allContainedNutrients := '';
        nodeObjects2 := TSynkkaXMLparser.getAllObjectsForKeyFromStringList(nodeObject, 'nutrientDetail');
        try
          if (nodeObjects2.Count > 0) then
          begin
            for nodeObject2 in nodeObjects2 do
            begin
              nodeObjects3 := TSynkkaXMLparser.getAllObjectsForKeyFromStringList(nodeObject2, 'quantityContained');
              quantityContained := '';
              try
                if (nodeObjects3.Count > 0) then
                begin
                  for nodeObject3 in nodeObjects3 do
                  begin
                    if (length(quantityContained) > 0) then quantityContained := quantityContained + ' / ';
                    quantityContained := quantityContained + getTradeItemMeasurementAsString(nodeObject3, FListOfMissingTranslations );
                  end;
                end;
              finally
                nodeObjects3.Free;
              end;

              containedNutrient := UTF8ToString( FsynkkaDescriptionsHandler.getFinnishDescrpitionForAttributeValue(nodeObject2.Values['ModuleName'], 'NutrientTypeCode', nodeObject2.Values['nutrientTypeCode']));
              if not (length(containedNutrient) > 0) then containedNutrient := nodeObject2.Values['nutrientTypeCode'];

              (* omat k��nn�kset, kun synkka taulusta ei l�ydy ollenkaan "FinnishDescription" vastinetta ruotsiksi *)
              containedNutrient := TranslateContainedNutrient( trim( containedNutrient ));

              allContainedNutrients := allContainedNutrients + html_listitem_begin + HTMLProcessString( containedNutrient + ': ' + quantityContained, aHtmlEncode ) + html_listitem_end;
            end; // end -- for obj2 in nodeObjects2 do
          end; // end -- if (nodeObjects2.Count > 0) then
        finally
          nodeObjects2.Free;
        end;

        result := result + html_paragraph_begin + html_list_begin + allContainedNutrients + html_list_end + html_paragraph_end;
      end;
    except
      on E : Exception do begin
        // error
        SiMain.LogException(E, 'cant put item nutritional info to csv table');
      end;
    end;
  finally
    if (nodeObjects <> nil) then nodeObjects.Free;
  end;
end;(* CompileRavintoarvot *)

function Twindow_Synkka.HtmlEncode(const AText: string): string;
(* https://mateam.net/html-escape-characters/ *)
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(AText) do
  begin
    case AText[I] of
      '�': Result := Result + '&auml;';
      '�': Result := Result + '&ouml;';
      '�': Result := Result + '&aring;';
      '�': Result := Result + '&Auml;';
      '�': Result := Result + '&Ouml;';
      '�': Result := Result + '&Aring;';
    //  '<': Result := Result + '&lt;';    these we need
    //  '>': Result := Result + '&gt;';
      '&': Result := Result + '&amp;';
      '"': Result := Result + '&quot;';
      '''': Result := Result + '&#39;';
      '�' : result := result + '&#176;';
      '+' : result := result + '&#43;';
      '-' : result := result + '&#8722;';
      '/' : result := result + '&#47;';
      else Result := Result + AText[I];
    end;
  end;
end;(* HtmlEncode *)

function Twindow_Synkka.HTMLProcessString( const aString : string; const aHtmlEncode : boolean = false ) : string;
begin
  if aHtmlEncode then
    result := HtmlEncode( aString )
  else
    result := aString;
end;(* HTMLProcessString *)

function Twindow_Synkka.RemoveSeparators( const aClearBuffer : string ) : string;
(* clear ; chars from string for CSV file *)
var
  ii : Integer;
begin
  Result := '';
  for ii := 1 to Length(aClearBuffer) do
  begin
    case aClearBuffer[ ii ] of
      csv_separator:;
      else Result := Result + aClearBuffer[ ii ];
    end;(* case *)
  end;(* for *)
end;(* RemoveSeparators *)

function Twindow_Synkka.Magento2VerifyConnection : boolean;
begin
  result := true;

  try
    if not FMagento2Api.IsAccessTokenValid then
    begin
      FMagento2Api.MagentoURL := 'https://www.kauppahalli24.fi';
      result := FMagento2Api.Connect( 'janne2', 'BsBCjPupNJtL3O5euC3iNBPGSwltLETL' );
      if not result then
      begin
        SiMain.LogError( 'Magento connect failed!' );
        exit;
      end;
    end else
      result := true;

  except
    on E: Exception do
      SiMain.LogException( E, 'Magento2VerifyConnection' );
  end;
end;(* Magento2VerifyConnection *)

function Twindow_Synkka.Magento2ReadSynkkaChoices( var aProductData : TDictionary<string, TSynkkaMagentoRecord> ) : boolean;
var
  iItemsArray : TJSONArray;
  iItemObject, iCustomAttributeItemObject : TJSONObject;
  iItemValue : TJSONValue;
  iItemCounter : word;
  iProductRecord : TSynkkaMagentoRecord;
  iGTIN : string;
begin
  result := false;
  try
    FMagento2Api.SearchCriteriaPageSize := 50;

    if Magento2VerifyConnection then
      if FMagento2Api.ProductSynkkaChoices( EncodeDate( 2000, 1, 1 )) then
      begin
        while FMagento2Api.GetNextPage( iItemsArray ) do
        begin
          if Assigned( iItemsArray ) then
          begin
            for iItemCounter := 0 to pred( iItemsArray.Count ) do
            begin
              iItemObject := iItemsArray.Items[ iItemCounter ] as TJSONObject;
              if Assigned( iItemObject ) then
              begin
                //SiMain.LogString( 'item['+ inttostr( iItemCounter ) +']', iItemObject.ToJSON );

                iItemValue := iItemObject.FindValue( 'sku' ) as TJSONValue;
                if Assigned( iItemValue ) then
                begin
                  //SiMain.LogDebug( 'sku: '+ iItemValue.Value );
                  iGTIN := iItemValue.Value;

                  iItemValue := SafeFindCustomAttributeNode( iItemObject, 'synkka_ignore_update' );
                  if assigned( iItemValue ) then
                  begin
                    //SiMain.LogDebug( '--synkka_ignore_update: '+ iItemValue.ToString );
                    iProductRecord.IgnoreUpdate := iItemValue.Value = '1';
                  end;

                  iItemValue := SafeFindCustomAttributeNode( iItemObject, 'synkka_add_to_description' );
                  if Assigned( iItemValue ) then
                  begin
                    //SiMain.LogDebug( '--synkka_add_to_description: '+ iItemValue.ToString );
                    iProductRecord.AddDescription := iItemValue.Value;
                  end;

                  aProductData.Add( iGTIN, iProductRecord );
                end;
              end;(* Assigned *)
            end;(* for iItemCounter *)
          end;(* <> nil *)
        end;(* while *)

        result := aProductData.Keys.Count > 0;

        (* TEMP display all dictionary items *)
        for iGTIN in aProductData.Keys do
          SiMain.LogDebug( iGTIN + ' ' + inttostr( ord( aProductData.Items[ iGTIN ].IgnoreUpdate )) + ' "' +
                           aProductData.Items[ iGTIN ].AddDescription + '"' );

      end else
        Memo_Log.lines.Add( 'failed' )


  except
    on E: Exception do
    begin
      SiMain.LogException( E, 'Magento2Process' );
      raise;
    end;
  end;
end;(* Magento2ReadSynkkaChoices *)

function Twindow_Synkka.Magento2Process( aProductData : TDictionary<string, TSynkkaMagentoRecord>;
                                         const aSKU : string; aDescription, aIngredients, aNutritional_values: string ) : boolean;
var
  iProductRecord : TSynkkaMagentoRecord;
  iSynkka_Updated_MagentoDate : string;
begin
  result := false;

  try
    (* ignore product update *)
    if Assigned( aProductData ) then
      if aProductData.TryGetValue( aSKU, iProductRecord ) then
      begin
        if iProductRecord.IgnoreUpdate then
        begin
          SiMain.LogVerbose( 'Ignoring "'+ aSKU + '" due IgnoreUpdate.' );
          exit;
        end;

        if length( iProductRecord.AddDescription ) > 0 then
          aDescription := iProductRecord.AddDescription + html_paragraph_enter + aDescription;
      end;

    iSynkka_Updated_MagentoDate := FormatMagentoDateTime( Date );
    SiMain.LogString( 'iSynkka_Updated_MagentoDate', iSynkka_Updated_MagentoDate );

    if Magento2VerifyConnection then
      if FMagento2Api.Product( aSKU ) then
        result := FMagento2Api.UpdateProductDescriptions( aSKU, aDescription, aIngredients,
                            aNutritional_values, iSynkka_Updated_MagentoDate );

  except
    on E: Exception do
      SiMain.LogException( E, 'Magento2Process' );
  end;
end;(* Magento2Process *)

procedure Twindow_Synkka.UpdateProductFromSynkkaToMagento2( aQuery : TFDQuery );
var
  dbFunctionality: TSynkkaLocalDatabaseFunctionality;
  gtinInformationRow : TClientDataSet;
  iTradeItemXMLfield, iGTINfield : TField;
  iField_GTIN, iField_Description, iField_Ainesosat, iField_Ravintoarvot : string;
  iGTIN, tradeItemXMLstring : string;
  iMaxCounter, iNotFoundCounter, iAddedCounter : word;
  tradeItem, tradeItemNodeObject: TStringList;
  iProductData : TDictionary<string, TSynkkaMagentoRecord>;
begin
  iMaxCounter := 0;
  iNotFoundCounter := 0;
  iAddedCounter := 0;

  FMagento2Api := TMagento2Api.Create( nil );
  FListOfMissingTranslations := TStringList.create;
  iProductData := TDictionary<string, TSynkkaMagentoRecord>.create;
  try
    FSynkkaDescriptionsHandler := TSynkkaDescriptions.Create(nil);
    FSynkkaDescriptionsHandler.loadDataToMem;
    FSynkkaDescriptionsHandler.createSets;
    try
      if Magento2ReadSynkkaChoices( iProductData ) then
      begin
        dbFunctionality := TSynkkaLocalDatabaseFunctionality.Create(nil);
        try
          SiMain.LogInteger( 'aQuery.RecordCount', aQuery.RecordCount );
          SiMain.WatchInteger( 'Recordcount', aQuery.RecordCount);

          aQuery.First;
          iGTINfield := aQuery.FindField( PRODUCTS_BARCODE );

          while not aQuery.EOF do
          begin
            iGTIN := iGTINfield.AsString;
            if Length( iGTIN ) = 0 then
            begin
              SiMain.LogWarning( 'Barcode was missing from Heartwood' );
              aQuery.Next;
              continue;
            end;

            SiMain.EnterMethod( iGTIN );
            try
              gtinInformationRow := dbFunctionality.fetchGTINinformationFromMySQL( iGTINfield.AsString );
              try
                if not (gtinInformationRow <> nil) then
                begin
                  aQuery.Next;
                  continue;
                end;

                tradeItemXMLstring := '';
                iTradeItemXMLfield := gtinInformationRow.FindField('tradeItemXML');
                if (iTradeItemXMLfield <> nil) then begin
                  tradeItemXMLstring := iTradeItemXMLfield.AsWideString;
                end;
                if not (length(tradeItemXMLstring) > 0) then
                begin
                  SiMain.LogError('trade item XML string doesnt exist for GTIN: ' + iGTINfield.AsString);
                  inc( iNotFoundCounter );
                  SiMain.WatchInteger( 'iNotFoundCounter', iNotFoundCounter);
                  aQuery.Next;
                  continue;
                end;

                tradeItem := TSynkkaXMLparser.parseGTINinformationFromXMLstring(tradeItemXMLstring);
                try
                  if not (tradeItem <> nil) then
                  begin
                    SiMain.LogError('trade item info not found for GTIN: ' + iGTINfield.AsString);
                    inc( iNotFoundCounter );
                    SiMain.WatchInteger( 'iNotFoundCounter', iNotFoundCounter);
                    aQuery.Next;
                    continue;
                  end;

                  iField_GTIN := tradeItem.Values['gtin'];
                  SiMain.LogString( 'Synkka GTIN', iField_GTIN );

                  iField_Description := CompileDescription( tradeItem );
                  iField_Ainesosat := CompileAinesosat( tradeItem );
                  iField_Ravintoarvot := CompileRavintoarvot( tradeItem );

                  SiMain.LogHtml( 'iField_Description', iField_Description );
                  SiMain.LogHtml( 'iField_Ainesosat', iField_Ainesosat );
                  SiMain.LogHtml( 'iField_Ravintoarvot', iField_Ravintoarvot );

                  if Magento2Process( iProductData, iGTINfield.AsString, iField_Description,
                                      iField_Ainesosat, iField_Ravintoarvot ) then
                  begin
                    inc( iAddedCounter );
                    SiMain.WatchInteger( 'iAddedCounter', iAddedCounter);
                  end;

                finally
                  if not (tradeItem <> nil) then tradeItem.Free;
                end;

              finally
                gtinInformationRow.Free;
              end;

            finally
              SiMain.LeaveMethod( iGTIN );
            end;

            aQuery.Next;
          end;

        finally
          dbFunctionality.Free;
        end;
      end else
        SiMain.LogError( 'Magento2ReadSynkkaChoices failed!' );

      if FListOfMissingTranslations.Count > 0 then
      begin
        FListOfMissingTranslations.Sort;
        SiMain.LogStringList( 'FListOfMissingTranslations', FListOfMissingTranslations );
      end;

    finally
      FSynkkaDescriptionsHandler.clearSets;
      FSynkkaDescriptionsHandler.clearDataFromMem;
      FSynkkaDescriptionsHandler.Free;
    end;

  finally
    FreeAndNil( iProductData );
    FreeAndNil( FListOfMissingTranslations );
    FreeAndNil( FMagento2Api );
  end;
end;(* UpdateProductFromSynkkaToMagento2 *)

procedure Twindow_Synkka.ExportMultilanguageToCSV( const aHtmlEncode : boolean = false );
var
  dbFunctionality: TSynkkaLocalDatabaseFunctionality;
  gtinInformationRow, listOfGTINs: TClientDataSet;
  iTradeItemXMLfield, iGTINfield : TField;
  iField_GTIN, iField_Description, iField_Ainesosat, iField_Ravintoarvot : string;
  tradeItemXMLstring : string;
  iMaxCounter, iNotFoundCounter, iAddedCounter : word;
  tradeItem, tradeItemNodeObject: TStringList;
  CSVFile : TextFile;
begin
  iMaxCounter := 0;
  iNotFoundCounter := 0;
  iAddedCounter := 0;

  FListOfMissingTranslations := TStringList.create;
  AssignFile(CSVFile, 'C:\Servers\Heartwood\Synkka\CSVexport\synkka-translations-'+ FormatDateTime( 'yyyy-mm-dd--hh-nn-ss', Now ) +'.csv' );
  try
    Rewrite(CSVFile);
    Writeln(CSVFile, 'sku' + csv_separator + 'description' + csv_separator + 'ingredients'+ csv_separator + 'nutritional_values' );

    FSynkkaDescriptionsHandler := TSynkkaDescriptions.Create(nil);
    FSynkkaDescriptionsHandler.loadDataToMem;
    FSynkkaDescriptionsHandler.createSets;
    try
      dbFunctionality := TSynkkaLocalDatabaseFunctionality.Create(nil);
      try
        try
          listOfGTINs := dbFunctionality.fetchRowsFromTable('select * from sydanpuu.' + 'exporttocsv' );
        except
          on E: Exception do
          begin
           SiMain.LogException(E, 'cant fetch rows from table sydanpuu');
           exit;
          end;
        end;

        if not ((listOfGTINs <> nil) and (listOfGTINs.RecordCount > 0) and not listOfGTINs.IsEmpty) then
        begin
          SiMain.LogError(' list of GTINs is empty');
          exit;
        end;

        listOfGTINs.EmptyDataSet;
        listOfGTINs.Append;
        listOfGTINs.FieldByName( 'GTIN' ).AsLargeInt := 6408430030019;
        listOfGTINs.post;
        listOfGTINs.Append;
        listOfGTINs.FieldByName( 'GTIN' ).AsLargeInt := 5702071381341;
        listOfGTINs.post;
        listOfGTINs.Append;
        listOfGTINs.FieldByName( 'GTIN' ).AsLargeInt := 0000040084107;
        listOfGTINs.post;

        SiMain.LogInteger( 'listOfGTINs.RecordCount', listOfGTINs.RecordCount );
        SiMain.WatchInteger( 'Recordcount', listOfGTINs.RecordCount);

        listOfGTINs.First;
        while not listOfGTINs.EOF do
        begin
          { TODO 2 -ctodo : Varmista ett� iGTINField tietotyyppi ei h�vit� etunollia! }
          iGTINfield := listOfGTINs.FindField('GTIN');
          if not (iGTINfield <> nil) then
          begin
            listOfGTINs.Next;
            continue;
          end;


//JAT          gtinInformationRow := dbFunctionality.fetchGTINinformationFromMySQL( 6408430408900 ); //VALIO A+ JOGURTTI KAURA 1KG
//JAT          gtinInformationRow := dbFunctionality.fetchGTINinformationFromMySQL( 6408430030019 ); //valio kirnuttu voi
//JAT          gtinInformationRow := dbFunctionality.fetchGTINinformationFromMySQL( 5702071381341 ); //vitamiini
//JAT          gtinInformationRow := dbFunctionality.fetchGTINinformationFromMySQL( 0000040084107 ); //Kinder
//JAT          gtinInformationRow := dbFunctionality.fetchGTINinformationFromMySQL( 6408430040308 ); //jat iGTINfield.AsLargeInt ); VIILI

          gtinInformationRow := dbFunctionality.fetchGTINinformationFromMySQL( iGTINfield.AsString );
          try
            if not (gtinInformationRow <> nil) then
            begin
              listOfGTINs.Next;
              continue;
            end;

            tradeItemXMLstring := '';
            iTradeItemXMLfield := gtinInformationRow.FindField('tradeItemXML');
            if (iTradeItemXMLfield <> nil) then begin
              tradeItemXMLstring := iTradeItemXMLfield.AsWideString;
            end;
            if not (length(tradeItemXMLstring) > 0) then
            begin
              SiMain.LogError('trade item XML string doesnt exist for GTIN: ' + UIntToStr(iGTINfield.AsLargeInt));
              inc( iNotFoundCounter );
              SiMain.WatchInteger( 'iNotFoundCounter', iNotFoundCounter);
              listOfGTINs.Next;
              continue;
            end;

            tradeItem := TSynkkaXMLparser.parseGTINinformationFromXMLstring(tradeItemXMLstring);
            try
              if not (tradeItem <> nil) then
              begin
                SiMain.LogError('trade item info not found for GTIN: ' + UIntToStr(iGTINfield.AsLargeInt));
                inc( iNotFoundCounter );
                SiMain.WatchInteger( 'iNotFoundCounter', iNotFoundCounter);
                listOfGTINs.Next;
                continue;
              end;

              //SiMain.LogStringList( 'tradeItem', tradeItem );
              iField_GTIN := tradeItem.Values['gtin'];
              SiMain.LogDebug( iField_GTIN );

              iField_Description := CompileDescription( tradeItem, aHtmlEncode );
              iField_Ainesosat := CompileAinesosat( tradeItem, aHtmlEncode );
              iField_Ravintoarvot := CompileRavintoarvot( tradeItem, aHtmlEncode );

              //SiMain.LogString( 'iField_Description', iField_Description );
              SiMain.LogHtml( 'iField_Description', iField_Description );
              //SiMain.LogString( 'iField_Ainesosat', iField_Ainesosat );
              SiMain.LogHtml( 'iField_Ainesosat', iField_Ainesosat );
              //SiMain.LogString( 'iField_Ravintoarvot', iField_Ravintoarvot );
              SiMain.LogHtml( 'iField_Ravintoarvot', iField_Ravintoarvot );

              // use syd�npuu GTIN, because Synkka GTIN has a prefix 0 to archive a certain length
              Writeln(CSVFile, AnsiToUtf8( RemoveSeparators( iGTINfield.AsString {iField_GTIN} )) + csv_separator +
                               AnsiToUtf8( RemoveSeparators( iField_Description )) + csv_separator +
                               AnsiToUtf8( RemoveSeparators( iField_Ainesosat )) + csv_separator +
                               AnsiToUtf8( RemoveSeparators( iField_Ravintoarvot )));
              inc( iAddedCounter );
              SiMain.WatchInteger( 'iAddedCounter', iAddedCounter);

            finally
              if not (tradeItem <> nil) then tradeItem.Free;
            end;

          finally
            gtinInformationRow.Free;
          end;

          if iAddedCounter > 500 then break;

          //break; // jat export only one product

          listOfGTINs.Next;
        end;

      finally
        dbFunctionality.Free;
        if (listOfGTINs <> nil) then listOfGTINs.Free;
      end;

    finally
      FSynkkaDescriptionsHandler.clearSets;
      FSynkkaDescriptionsHandler.clearDataFromMem;
      FSynkkaDescriptionsHandler.Free;
    end;

    if FListOfMissingTranslations.Count > 0 then
    begin
      FListOfMissingTranslations.Sort;
      SiMain.LogStringList( 'FListOfMissingTranslations', FListOfMissingTranslations );
    end;

  finally
    CloseFile(CSVFile);
    FreeAndNil( FListOfMissingTranslations );
  end;

  Memo_Log.Lines.Add( 'Export complete!');
end;(* ExportMultilanguageToCSV *)

procedure Twindow_Synkka.btn_ExportCSVClick(Sender: TObject);
begin
  SynkkaXMLfileProcessing.exportGTINtoCSV( 'exporttocsv' );
  Memo_Log.Lines.Add( 'done' );
end;

procedure Twindow_Synkka.btn_ExportMultiLanguageClick(Sender: TObject);
begin
  ExportMultilanguageToCSV( false );
end;

procedure Twindow_Synkka.btn_GetSynkkaChoicesClick(Sender: TObject);
var
  iProductData : TDictionary<string, TSynkkaMagentoRecord>;
begin
  iProductData := TDictionary<string, TSynkkaMagentoRecord>.create;
  FMagento2Api := TMagento2Api.Create( nil );
  try
    Magento2ReadSynkkaChoices( iProductData );

  finally
    FreeAndNil( FMagento2Api );
    FreeAndNil( iProductData );
  end;
end;

procedure Twindow_Synkka.btn_JsonTestClick(Sender: TObject);
var
  iJSONString : string;
  iResponseJSON : TJSONObject;
  iItemValue : TJSONValue;
  iItemObject : TJSONObject;
  iItemsArray : TJSONArray;
  ii : byte;
begin
  iJSONString := '{"items":[{"name":1,"value":1},{"name":2,"value":2}],"search_criteria":{"filter_groups":[{"filters":'+
    '[{"field":"synkka_ignore_update","value":"1","condition_type":"gt"},{"field":'+
    '"synkka_add_to_description","value":null,"condition_type":"notnull"}]}],'+
    '"page_size":0,"current_page":1},"total_count":1}';

  iResponseJSON := TJSONObject.create;
  try
    iResponseJSON.Parse(BytesOf(iJSONString), 0);
    Memo_Log.Lines.text := iResponseJSON.ToJSON;
    Memo_Log.Lines.Add( '' );

    iItemsArray := iResponseJSON.FindValue( 'items' ) as TJSONArray;
    if assigned( iItemsArray ) then
    begin
      Memo_Log.Lines.Add( inttostr( iItemsArray.Count ));
      if iItemsArray.Count > 0 then
        for ii := 0 to pred( iItemsArray.Count ) do
        begin
          iItemObject := iItemsArray.Items[ ii ] as TJSONObject;
          Memo_Log.Lines.Add( 'item[ '+ inttostr( ii ) + ']' );
          if Assigned( iItemObject ) then
          begin
            iItemValue := iItemObject.FindValue( 'name' ) as TJSONValue;
            if assigned( iItemValue ) then Memo_Log.Lines.Add( 'name = '+ iItemValue.ToString );

            iItemValue := iItemObject.FindValue( 'value' ) as TJSONValue;
            if assigned( iItemValue ) then Memo_Log.Lines.Add( 'value = '+ iItemValue.ToString );
          end;
        end;
    end;

    iItemValue := iResponseJSON.FindValue( 'total_count' ) as TJSONValue;
    if assigned( iItemValue ) then Memo_Log.Lines.Add( 'total_count = '+ iItemValue.ToString );

    iItemObject := iResponseJSON.FindValue( 'search_criteria' ) as TJSONObject;
    if assigned( iItemObject ) then Memo_Log.Lines.Add( 'search_criteria found!' );

    iItemValue := iResponseJSON.FindValue( 'search_criteria.page_size' ) as TJSONValue;
    if assigned( iItemValue ) then Memo_Log.Lines.Add( 'page_size = '+ iItemValue.ToString );

    iItemValue := iResponseJSON.FindValue( 'search_criteria.current_page' ) as TJSONValue;
    if assigned( iItemValue ) then Memo_Log.Lines.Add( 'current_page = '+ iItemValue.ToString );

  finally
    iResponseJSON.free;
  end;
end;

procedure Twindow_Synkka.Button_ConnectClick(Sender: TObject);
begin
{  FDConnection_synkka.params.Password := Edit_Host.Text;
  //FDConnection.params.port := StrToIntDef( Edit_Port.Text, 0 );
  FDConnection_synkka.params.UserName := Edit_user.Text;
  FDConnection_synkka.params.Password := Edit_Password.Text;
  FDConnection_synkka.params.Database := Edit_Database.Text;
 }

  Memo_Log.Lines.Add( 'Opening MariaDB...');
  FDConnection_synkka.Open;

//  FDQuery_synkka.SQL.Text := 'SELECT GTIN, tradeitemXML FROM iteminformation ORDER BY lastChangeDateTime DESC';// LIMIT 100';
//  FDQuery_synkka.Open;
  Memo_Log.Lines.Add( 'Done.');
end;

procedure Twindow_Synkka.Button_CSV_VerifyClick(Sender: TObject);
var
  iFound, iNotFound : word;
  iTradeItem : TStringList;
begin
  SiMain.EnterMethod(Self, 'Button_CSV_VerifyClick');

  iTradeItem := TStringList.Create;
  try    
    Memo_Log.Lines.Add( 'Processing...');

    iFound := 0;
    iNotFound := 0;

    FDQuery_Heartwood.First;
    while not FDQuery_Heartwood.eof do
    begin
      FDQuery_synkka.SQL.Text := 'SELECT GTIN, tradeitemXML FROM iteminformation WHERE GTIN = "'+
                                 FDQuery_Heartwood.FieldByName( 'Barcode' ).AsString + '"';
      //ORDER BY lastChangeDateTime DESC';// LIMIT 100';
      FDQuery_synkka.Open;

      //if FDQuery_synkka.FindKey([ FDQuery_Heartwood.FieldByName( 'Barcode' ).AsString ]) then
      //if ( not FDQuery_synkka.Eof ) and ( not FDQuery_synkka.bof ) then
      if FDQuery_synkka.RecordCount = 1 then
      begin
        inc( iFound );
        iTradeItem := SynkkaXMLparser.parseGTINinformationFromXMLstring( FDQuery_synkka.FieldByName( 'tradeitemXML' ).AsWideString );
        iTradeItem.SaveToFile( IncludeTrailingPathDelimiter( ExtractFilePAth( Application.ExeName )) + 'traditem '+ iTradeItem.Values['gtin'] + '.txt' );

        if iFound > 10 then
        begin
          Memo_Log.Lines.Add( 'Exiting after 10 found xml structures.' );
          break;
        end;

      end else
        inc( iNotFound );

      FDQuery_Heartwood.next;

    end;

    Memo_Log.Lines.Add( 'Found products ' + inttostr( iFound ));
    Memo_Log.Lines.Add( 'Not found products ' + inttostr( iNotFound ));

  finally
    iTradeItem.Free;
    SiMain.LeaveMethod(Self, 'Button_CSV_VerifyClick');    
  end;
end;

procedure Twindow_Synkka.Button_HeartwoodClick(Sender: TObject);
begin
  Memo_Log.Lines.Add( 'Opening Heartwood...');
  FDConnection_Heartwood.Open;

  FDQuery_Heartwood.SQL.Text := 'SELECT * FROM PRODUCTS';
  FDQuery_Heartwood.Open;
  Memo_Log.Lines.Add( 'Done.');
end;

procedure Twindow_Synkka.Button_ParseXMLClick(Sender: TObject);
var
  iProductXML : TStringList;
  iTradeItem : TStringList;

  procedure _AddSimpleItem( const aItemName : string; const aTradeItem : TStringList );
  begin
    Memo_Log.Lines.Add( aItemName + ': '+ aTradeItem.Values[ aItemName ]);
  end;(* _AddItem *)

  procedure _AddTranslations( const aItemName : string; const aTradeItem : TStringList );
  var iFinnish, iSwedish, iEnglish: string;
  begin
    iFinnish := '';
    iSwedish := '';
    iEnglish := '';

    {    GetTranslations( iFinnish, iSwedish, iEnglish, aTradeItem, aItemName );}

    Memo_Log.Lines.Add( aItemName + '(fin): '+ iFinnish );
    Memo_Log.Lines.Add( aItemName + '(swe): '+ iSwedish );
    Memo_Log.Lines.Add( aItemName + '(eng): '+ iEnglish );
  end;(* _AddTranslations *)

begin
  iProductXML := TStringList.Create;
  iTradeItem := TStringList.Create;
  try
    iProductXML.LoadFromFile( IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + 'synkka 8033712790695.xml' );

    iTradeItem := SynkkaXMLparser.parseGTINinformationFromXMLstring( iProductXML.Text );
    //iTradeItem.SaveToFile( IncludeTrailingPathDelimiter( ExtractFilePAth( Application.ExeName )) + 'traditem '+ iTradeItem.Values['gtin'] + '.txt' );

    (* tradeItemInformation *)
    _AddSimpleItem( 'gtin', itradeItem );
    _AddSimpleItem( 'additionalTradeItemIdentification', itradeItem );
    _AddSimpleItem( 'brandName', itradeItem );
    _AddSimpleItem( 'subBrand', itradeItem );
    _AddSimpleItem( 'gpcCategoryCode', itradeItem );
    _AddSimpleItem( 'gpcCategoryName', itradeItem );

    (* tradeItemDescriptionsModule *)
    _AddTranslations( 'tradeItemDescription', iTradeItem );
    _AddTranslations( 'descriptionShort', iTradeItem );
    _AddTranslations( 'functionalName', iTradeItem );

    (* tradeItemMarketingInformation  *)
    _AddTranslations( 'tradeItemMarketingMessage', iTradeItem );

    (* tradeItemPlaceOfItemActivityModule *)
    _AddTranslations( 'provenanceStatement', iTradeItem );

    //tradeItemReferencedFilesToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);

    (* tradeItemConsumerInstructions *)
    _AddTranslations( 'consumerStorageInstructions', iTradeItem );
    _AddTranslations( 'consumerUsageInstructions', iTradeItem );

    {

            tradeItemNutritionalInformationModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemLifeSpanModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            dutyFeeTaxInformationToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemMeasurementsModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemTemperatureInformationToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemSalesInformationToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemPackagingMarkingToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemFoodAndBeverageIngredientModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemAllergenRelatedInformationToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemPackagingInformationToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemDietInformationToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            foodAndBeveragePreparationServingModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemHealthRelatedInformationModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemFarmingAndProcessingInformationModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemHandlingInformationToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemNonfoodIngredientModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemSafetyDataSheetModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemSustainabilityModuleToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            tradeItemSpecialAttributesToCSVtable(csvDataInTable, tradeItem, rowIndex, csvFileConfig);
            }

  finally
    iProductXML.Free;
    iTradeItem.Free;
  end;
end;

procedure Twindow_Synkka.Button_UpdateOnSaleClick(Sender: TObject);
begin
  SiMain.EnterMethod( 'Button_UpdateOnSaleClick' );
  try
    FDConnection_Heartwood.Open;

    {PRODUCTS_LIFECYCLE_INCOMING = 1;
     PRODUCTS_LIFECYCLE_ONSALE = 2;
     PRODUCTS_LIFECYCLE_TAUKO = 3; }

    { testaa yksitt�isell� tuotteella synkan arvojen nouto magentosta ja k�sittely oikein
     mik� GTIN koodi l�ytyy?
     }
    FDQuery_Heartwood.SQL.Text := 'SELECT ID,BARCODE,CREATED,MODIFIED,LIFECYCLE,PROVIDERID FROM PRODUCTS '+
                                  //'WHERE BARCODE = ''6414886550766'' or BARCODE = ''6414889041858'' or BARCODE = ''9789513248581'' ' +
                                  'WHERE LIFECYCLE = 2 '+
                                  'ORDER BY LIFECYCLE,PROVIDERID,BARCODE';
                                  //'ROWS 5';
    FDQuery_Heartwood.Open;
    SiMain.LogInteger( 'FDQuery_Heartwood.RecordCount', FDQuery_Heartwood.RecordCount );

    UpdateProductFromSynkkaToMagento2( FDQuery_Heartwood );

    Memo_Log.Lines.Add( 'Button_UpdateOnSaleClick complete' );

  finally
    SiMain.LeaveMethod( 'Button_UpdateOnSaleClick' );
  end;
end;(* Button_UpdateOnSaleClick *)

procedure Twindow_Synkka.Button_UpdateWaitingClick(Sender: TObject);
begin
  SiMain.EnterMethod( 'Button_UpdateWaitingClick' );
  try
    FDConnection_Heartwood.Open;

    {PRODUCTS_LIFECYCLE_INCOMING = 1;
     PRODUCTS_LIFECYCLE_ONSALE = 2;
     PRODUCTS_LIFECYCLE_TAUKO = 3; }
    FDQuery_Heartwood.SQL.Text := 'SELECT ID,BARCODE,CREATED,MODIFIED,LIFECYCLE,PROVIDERID FROM PRODUCTS '+
                                  //'WHERE BARCODE = ''6408430030019'' ' +
                                  'WHERE (LIFECYCLE = 1 OR LIFECYCLE = 3 )'+
                                  'ORDER BY LIFECYCLE,PROVIDERID,BARCODE';
                                  //'ROWS 5';
    FDQuery_Heartwood.Open;
    SiMain.LogInteger( 'FDQuery_Heartwood.RecordCount', FDQuery_Heartwood.RecordCount );

    UpdateProductFromSynkkaToMagento2( FDQuery_Heartwood );

    Memo_Log.Lines.Add( 'Button_UpdateOnSaleClick complete' );

  finally
    SiMain.LeaveMethod( 'Button_UpdateWaitingClick' );
  end;
end;



end.
