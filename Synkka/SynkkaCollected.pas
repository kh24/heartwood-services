unit SynkkaCollected;

(* this is at least temporarely removed from ... *)

(* following content is cathered from SynkkaDataPoolOperations project,
   since they cannot be directly linked. *)

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  USynkkaXMLparserUnit,
  System.Variants;

(* SynkkaLocalDatabaseFunctionalitiesU *)


(*  SynkkaDescriptionsUnit *)
type
  TSynkkaDescriptions = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    var
//      dbFunctionality: TSynkkaLocalDatabaseFunctionality;

    class function createNewStringListWithSetOfStrings(values: array of string): TStringList; static;
  public
    var
      NonFoodIngredientAdditiveList: TStringList;
      LevelOfContainmentCodeList: TStringList;
      AllergenTypeCodeList: TStringList;
      NutritionalClaimNutrientElementCodeList: TStringList;
      NutritionalClaimTypeCodeList: TStringList;
      DietTypeCodeList: TStringList;
      PackagingMarkedLabelAccreditationCodeList: TStringList;

    function findCodeListForAttribute(moduleName: string; attributeName: string): string;
    function findRecordForAttribute(moduleName: string; attributeName: string): boolean;
    function findDescriptionForCode(codeListName: string; codeValue: string): string;
    function findRecordForCode(codeListName: string; codeValue: string): boolean;
    function getFinnishDescrpitionForAttributeName(moduleName: string; attributeName: string): string;
    function getFinnishDescrpitionForAttributeValue(moduleName: string; attributeName: string; attributeValue: string): string;
    function getValueForAllergenAttribute(const containmentType: string; const allergenName: string; containmentTypeValue: string; allergenValue: string): integer;
    function getValueForNutritionalAttribute(const containmentType: string; const nutrientName: string; containmentTypeValue: string; nutrientNameValue: string): integer;
    function getValueForNonFoodAdditiveAttribute(const containmentType: string; const additiveName: string; containmentTypeValue: string; additiveNameValue: string): integer;
    function getFinnishTranslationForGPCbrickCode(gpcCode: string): string;
    function findRecordForGPCbrickCode(gpcCode: string): boolean;
    function getValueForPackagingMarkedLabelAccreditationCodeAttribute(const key: string; const value: string): integer;

    procedure createSets();
    procedure clearSets();
  end;

var
  SynkkaDescriptions: TSynkkaDescriptions;

implementation

uses
  SmartInspect.VCL,SmartInspect.VCL.SiAuto;

(*  SynkkaDescriptionsUnit *)
procedure TSynkkaDescriptions.DataModuleDestroy(Sender: TObject);
begin
//  clearDataFromMem;
//  dbFunctionality.Free;
//  LevelOfContainmentCodeList.Free;
//  AllergenTypeCodeList.Free;
//  NutritionalClaimNutrientElementCodeList.Free;

end;

procedure TSynkkaDescriptions.DataModuleCreate(Sender: TObject);
begin
//  dbFunctionality := TSynkkaLocalDatabaseFunctionality.Create(nil);
//  loadDataToMem;
//  createSets;

end;

procedure TSynkkaDescriptions.clearSets();
begin
  try
    LevelOfContainmentCodeList.Free;
    AllergenTypeCodeList.Free;
    NutritionalClaimNutrientElementCodeList.Free;
  except
    on E: Exception do begin
      // write log E.Message;
      SiMain.LogException(E, 'error deallocating attribute sets' );
    end;
  end;

end;

class function TSynkkaDescriptions.createNewStringListWithSetOfStrings(values: array of string): TStringList;
var
  currentString: string;
begin
  Result := TStringList.Create(true);
  try
    Result.Duplicates := dupIgnore;
    Result.CaseSensitive := false;
    for currentString in values do begin
      try
        if (length(currentString) > 0) then Result.Add(currentString);
      except
        on E: Exception do begin
          // write log E.Message;
          begin
            SiMain.LogException(E, 'cant create string list, list is empty' );
          end;
          continue;
        end;
      end;
    end;
  except
    on E: Exception do begin
      // write log E.Message;
      SiMain.LogException(E, 'error creating new string list with set pf strings' );
    end;
  end;
end;

procedure TSynkkaDescriptions.createSets();
var
  newList: TStringList;
begin
  try
    LevelOfContainmentCodeList := createNewStringListWithSetOfStrings([]); // CONTAINS,FREE_FROM,MAY_CONTAIN,UNDECLARED
    LevelOfContainmentCodeList.AddObject('contains', createNewStringListWithSetOfStrings(['CONTAINS', 'MAY_CONTAIN']));
    LevelOfContainmentCodeList.AddObject('freeFrom', createNewStringListWithSetOfStrings(['FREE_FROM']));
    LevelOfContainmentCodeList.AddObject('low', createNewStringListWithSetOfStrings([]));
    LevelOfContainmentCodeList.AddObject('added', createNewStringListWithSetOfStrings([]));
    AllergenTypeCodeList := createNewStringListWithSetOfStrings([]);
    AllergenTypeCodeList.AddObject('milk', createNewStringListWithSetOfStrings(['AM']));
    AllergenTypeCodeList.AddObject('gluten', createNewStringListWithSetOfStrings(['AW', 'AX', 'GB', 'GK', 'GL', 'GO', 'GS', 'NR', 'UW']));
    AllergenTypeCodeList.AddObject('egg', createNewStringListWithSetOfStrings(['AE']));
    AllergenTypeCodeList.AddObject('lactose', createNewStringListWithSetOfStrings(['ML']));
    AllergenTypeCodeList.AddObject('peanut', createNewStringListWithSetOfStrings(['AP']));
    AllergenTypeCodeList.AddObject('nuts', createNewStringListWithSetOfStrings(['AN', 'SH', 'SM', 'SP', 'SR', 'SQ', 'SW']));
    AllergenTypeCodeList.AddObject('soy', createNewStringListWithSetOfStrings(['AY']));
    AllergenTypeCodeList.AddObject('pork', createNewStringListWithSetOfStrings(['PO']));
    AllergenTypeCodeList.AddObject('fish', createNewStringListWithSetOfStrings(['ABD', 'ABE', 'ACP', 'ADE', 'ADF', 'ADG', 'ADH', 'ADI', 'ADJ', 'ADK', 'ADL', 'ADM', 'ADP', 'ADQ', 'ADR', 'ADS', 'ADT', 'ADV', 'ADW', 'ADU', 'AF', 'ANO', 'AWF']));
    AllergenTypeCodeList.AddObject('shellfish', createNewStringListWithSetOfStrings(['ABI', 'ABK', 'ABL', 'AC', 'UN']));
    AllergenTypeCodeList.AddObject('additives', createNewStringListWithSetOfStrings([]));
    AllergenTypeCodeList.AddObject('birdmeat', createNewStringListWithSetOfStrings(['CM']));
    AllergenTypeCodeList.AddObject('beefmeat', createNewStringListWithSetOfStrings(['BF']));
    AllergenTypeCodeList.AddObject('rye', createNewStringListWithSetOfStrings(['NR']));
    AllergenTypeCodeList.AddObject('wheat', createNewStringListWithSetOfStrings(['UW']));
    AllergenTypeCodeList.AddObject('oat', createNewStringListWithSetOfStrings(['GO']));
    AllergenTypeCodeList.AddObject('caffeine', createNewStringListWithSetOfStrings([]));
    AllergenTypeCodeList.AddObject('nitrate', createNewStringListWithSetOfStrings([]));
    AllergenTypeCodeList.AddObject('sweetener', createNewStringListWithSetOfStrings([]));
    AllergenTypeCodeList.AddObject('nitrites', createNewStringListWithSetOfStrings([]));
    AllergenTypeCodeList.AddObject('carbonHydrates', createNewStringListWithSetOfStrings([]));
    AllergenTypeCodeList.AddObject('noAllergens', createNewStringListWithSetOfStrings(['X99']));
//    AllergenTypeCodeList.AddObject('perfume', createNewStringListWithSetOfStrings([]));

//mysql> select GdsnCode,CodeListName,FinnishDescription from sydanpuu.attributecodes where codelistname='AllergenTypeCodeList';
//+----------+----------------------+----------------------------------------------------------------------------------+
//| GdsnCode | CodeListName         | FinnishDescription                                                               |
//+----------+----------------------+----------------------------------------------------------------------------------+
//| AA       | AllergenTypeCodeList | Amylcinnamyl Alcohol                                                             |
//| ABD      | AllergenTypeCodeList | Tonnikala ja tonnikalatuotteet                                                   |
//| ABE      | AllergenTypeCodeList | Kuha ja kuhatuotteet                                                             |
//| ABF      | AllergenTypeCodeList | Siimajalkainen ja siimajalkaistuotteet                                           |
//| ABG      | AllergenTypeCodeList | Rapu ja raputuotteet                                                             |
//| ABH      | AllergenTypeCodeList | Jokirapu ja jokiraputuotteet                                                     |
//| ABI      | AllergenTypeCodeList | Krilli ja krillituotteet                                                         |
//| ABJ      | AllergenTypeCodeList | Hummeri ja hummerituotteet                                                       |
//| ABK      | AllergenTypeCodeList | Katkarapu (Prawns) ja katkaraputuotteet                                          |
//| ABL      | AllergenTypeCodeList | Katkarapu (Shrimp) ja katkaraputuotteet                                          |
//| AC       | AllergenTypeCodeList | Äyriäiset ja äyriäistuotteet                                                     |
//| ACP      | AllergenTypeCodeList | Karppi ja karppituotteet                                                         |
//| AD       | AllergenTypeCodeList | 3-Amino-2,4-dichlorophenol                                                       |
//| ADB      | AllergenTypeCodeList | Bassi ja bassituotteet                                                           |
//| ADC      | AllergenTypeCodeList | Anjovis ja anjovistuotteet                                                       |
//| ADE      | AllergenTypeCodeList | Monni ja monnituotteet                                                           |
//| ADF      | AllergenTypeCodeList | Turska ja turskatuotteet                                                         |
//| ADG      | AllergenTypeCodeList | Kampela ja kampelatuotteet                                                       |
//| ADH      | AllergenTypeCodeList | Meriahven ja meriahventuotteet                                                   |
//| ADI      | AllergenTypeCodeList | Kolja ja koljatuotteet                                                           |
//| ADJ      | AllergenTypeCodeList | Kummeliturska ja kummeliturskatuotteet                                           |
//| ADK      | AllergenTypeCodeList | Ruijanpallas ja ruijanpallastuotteet                                             |
//| ADL      | AllergenTypeCodeList | Silli ja sillituotteet                                                           |
//| ADM      | AllergenTypeCodeList | Dolfiini (Mahi mahi) ja dolfiinituotteet                                         |
//| ADP      | AllergenTypeCodeList | Hauki ja haukituotteet                                                           |
//| ADQ      | AllergenTypeCodeList | Alaskanseiti ja alaskanseitituotteet                                             |
//| ADR      | AllergenTypeCodeList | Lohi ja lohituotteet                                                             |
//| ADS      | AllergenTypeCodeList | Napsijat ja napsijatuotteet                                                      |
//| ADT      | AllergenTypeCodeList | Meriantura ja merianturatuotteet                                                 |
//| ADU      | AllergenTypeCodeList | Miekkakala ja miekkakalatuotteet                                                 |
//| ADV      | AllergenTypeCodeList | Tilapia ja tilapiatuotteet                                                       |
//| ADW      | AllergenTypeCodeList | Taimen ja taimentuotteet                                                         |
//| AE       | AllergenTypeCodeList | Munat ja munatuotteet                                                            |
//| AF       | AllergenTypeCodeList | Kalat ja kalatuotteet                                                            |
//| AH       | AllergenTypeCodeList | Anise Alcohol                                                                    |
//| AI       | AllergenTypeCodeList | Alpha-Isomethyl Ionone                                                           |
//| AL       | AllergenTypeCodeList | Amyl Cinnamal                                                                    |
//| AM       | AllergenTypeCodeList | Maito ja maitotuotteet, myös laktoosi                                            |
//| AN       | AllergenTypeCodeList | Pähkinät ja pähkinätuotteet                                                      |
//| ANO      | AllergenTypeCodeList | Ahven ja ahventuotteet                                                           |
//| AP       | AllergenTypeCodeList | Maapähkinät ja maapähkinätuotteet                                                |
//| AS       | AllergenTypeCodeList | Seesaminsiemenet ja seesaminsiementuotteet                                       |
//| AU       | AllergenTypeCodeList | Rikkidioksidi ja sulfiitit                                                       |
//| AW       | AllergenTypeCodeList | Gluteiinia sisältävät viljat ja viljatuotteet                                    |
//| AWF      | AllergenTypeCodeList | Siika ja siikatuotteet                                                           |
//| AX       | AllergenTypeCodeList | Muut gluteenia sisältävt viljat                                                  |
//| AY       | AllergenTypeCodeList | Soijapavut ja soijapaputuotteet                                                  |
//| BA       | AllergenTypeCodeList | Benzyl Alcohol                                                                   |
//| BB       | AllergenTypeCodeList | Benzyl Benzoate                                                                  |
//| BC       | AllergenTypeCodeList | Selleri ja sellerituotteet                                                       |
//| BE       | AllergenTypeCodeList | 2-(4-tert-Butylbenzyl)                                                           |
//| BF       | AllergenTypeCodeList | Naudanliha ja naudanlihatuotteet                                                 |
//| BI       | AllergenTypeCodeList | Benzyl Cinnamate                                                                 |
//| BM       | AllergenTypeCodeList | Sinappi ja sinappituotteet                                                       |
//| BN       | AllergenTypeCodeList | Isoeugenol                                                                       |
//| BO       | AllergenTypeCodeList | d-Limonene                                                                       |
//| BP       | AllergenTypeCodeList | Linalool                                                                         |
//| BQ       | AllergenTypeCodeList | Methyl heptin carbonate                                                          |
//| BR       | AllergenTypeCodeList | 1,3-Bis-(2,4-diaminophenoxy)propane                                              |
//| BS       | AllergenTypeCodeList | Benzyl Salicylate                                                                |
//| CA       | AllergenTypeCodeList | Cinnamyl Alcohol                                                                 |
//| CL       | AllergenTypeCodeList | Cinnamal                                                                         |
//| CM       | AllergenTypeCodeList | Kananliha ja kananlihatuotteet                                                   |
//| CN       | AllergenTypeCodeList | Citronellol                                                                      |
//| CO       | AllergenTypeCodeList | Coumarin                                                                         |
//| CS       | AllergenTypeCodeList | Puuvillansiemen ja puuvillansiementuotteet                                       |
//| CT       | AllergenTypeCodeList | Citral                                                                           |
//| DA       | AllergenTypeCodeList | 2,6-Dimethoxy-3,5-pyridinediamine HCl                                            |
//| DP       | AllergenTypeCodeList | Diaminophenols                                                                   |
//| EG       | AllergenTypeCodeList | Eugenol                                                                          |
//| EP       | AllergenTypeCodeList | Evernia Prunastri (Oak Moss extract)                                             |
//| EV       | AllergenTypeCodeList | Evernia Furfuracea (Treemoss extract)                                            |
//| FA       | AllergenTypeCodeList | Farnesol                                                                         |
//| FH       | AllergenTypeCodeList | 4-Hydroxy-propylamino-3-nitrophenol                                              |
//| FT       | AllergenTypeCodeList | 4-Amino-3-nitrophenol                                                            |
//| GB       | AllergenTypeCodeList | Ohra ja ohratuotteet (gluteenia sisältävät viljat)                               |
//| GE       | AllergenTypeCodeList | Geraniol                                                                         |
//| GK       | AllergenTypeCodeList | Kamut-vilja ja kamut-viljatuotteet (gluteenia sisältävät viljat)                 |
//| GL       | AllergenTypeCodeList | Glutamaatti                                                                      |
//| GO       | AllergenTypeCodeList | Kaura ja kauratuotteet (gluteenia sisältävät viljat)                             |
//| GS       | AllergenTypeCodeList | Speltti ja spelttituotteet (gluteenia sisältävät viljat)                         |
//| HB       | AllergenTypeCodeList | HC Blue No 12                                                                    |
//| HC       | AllergenTypeCodeList | Hydroxy-Methylplentylcyclohexencarboxaldehyd                                     |
//| HD       | AllergenTypeCodeList | HC Blue No 11                                                                    |
//| HE       | AllergenTypeCodeList | Hydroxybenzomorpholine                                                           |
//| HH       | AllergenTypeCodeList | Hydroxypropyl bis(N-hydroxyethyl-p-phenyldiamine) HCl                            |
//| HN       | AllergenTypeCodeList | Hydroxyethyl-2-nitro-p-toluidine                                                 |
//| HP       | AllergenTypeCodeList | 2-Hydroxyethyl-picramic acid                                                     |
//| HX       | AllergenTypeCodeList | Hexyl Cinnamaldehyd                                                              |
//| HY       | AllergenTypeCodeList | Hydroxycitronellal                                                               |
//| MH       | AllergenTypeCodeList | 2-Methyl-5-hydroxyethylaminophenol                                               |
//| ML       | AllergenTypeCodeList | Laktoosi                                                                         |
//| NC       | AllergenTypeCodeList | Kaakao ja kaakaotuotteet                                                         |
//| NE       | AllergenTypeCodeList | Herne ja hernetuotteet                                                           |
//| NK       | AllergenTypeCodeList | Korianteri                                                                       |
//| NL       | AllergenTypeCodeList | Lupiini ja lupiinituotteet                                                       |
//| NM       | AllergenTypeCodeList | Maissi ja maissituotteet                                                         |
//| NP       | AllergenTypeCodeList | Palkohedelmät ja palkohedelmätuotteet (ent.Kuorihedelmät ja hedelmätuotteet)     |
//| NR       | AllergenTypeCodeList | Ruis ja ruistuotteet (gluteenia sisältävät viljat)                               |
//| NW       | AllergenTypeCodeList | Porkkana ja porkkanatuotteet                                                     |
//| ON       | AllergenTypeCodeList | 1-Naphthol                                                                       |
//| PM       | AllergenTypeCodeList | Methylaminophenol                                                                |
//| PN       | AllergenTypeCodeList | Pinjansiemen ja pinjansiementuotteet                                             |
//| PO       | AllergenTypeCodeList | Porsaanliha ja porsaanlihatuotteet                                               |
//| PP       | AllergenTypeCodeList | P-fenyleenidiamiini                                                              |
//| PS       | AllergenTypeCodeList | Unikonsiemenet                                                                   |
//| SA       | AllergenTypeCodeList | Mantelit ja mantelituotteet                                                      |
//| SB       | AllergenTypeCodeList | Siemenet ja siementuotteet                                                       |
//| SC       | AllergenTypeCodeList | Cashewpähkinät ja cashew-pähkinätuotteet                                         |
//| SD       | AllergenTypeCodeList | Pyökki siemenet                                                                  |
//| SE       | AllergenTypeCodeList | Amerikanjalopähkinä                                                              |
//| SF       | AllergenTypeCodeList | Chinquapin                                                                       |
//| SG       | AllergenTypeCodeList | Neidonhiuspuun siemen / ginkgo                                                   |
//| SH       | AllergenTypeCodeList | Hasselpähkinät ja hasselpähkinätuotteet                                          |
//| SI       | AllergenTypeCodeList | Hikkoripuun pähkinä                                                              |
//| SJ       | AllergenTypeCodeList | Karitepähkinä / Shea                                                             |
//| SK       | AllergenTypeCodeList | Pili hedelmä                                                                     |
//| SL       | AllergenTypeCodeList | Litsi                                                                            |
//| SM       | AllergenTypeCodeList | Macadamia-pähkinät ja Macadamia-pähkinätuotteet                                  |
//| SN       | AllergenTypeCodeList | Kastanja                                                                         |
//| SO       | AllergenTypeCodeList | Kookos ja kookostuotteet                                                         |
//| SP       | AllergenTypeCodeList | Pekaanipähkinät ja pekaanipähkinätuotteet                                        |
//| SQ       | AllergenTypeCodeList | Queensland-pähkinät ja Queensland-pähkinätuotteet                                |
//| SR       | AllergenTypeCodeList | Parapähkinät ja parapähkinätuotteet                                              |
//| SS       | AllergenTypeCodeList | Auringonkukan siemenet                                                           |
//| ST       | AllergenTypeCodeList | Pistaasipähkinät ja pistaasipähkinätuotteet                                      |
//| SW       | AllergenTypeCodeList | Saksanpähkinät ja saksanpähkinätuotteet                                          |
//| SX       | AllergenTypeCodeList | Palkokasvit                                                                      |
//| TA       | AllergenTypeCodeList | Aminophenol                                                                      |
//| TD       | AllergenTypeCodeList | Toluene-2,5-diamine                                                              |
//| TR       | AllergenTypeCodeList | Triticale                                                                        |
//| UM       | AllergenTypeCodeList | Nilviäiset ja nilviäistuotteet                                                   |
//| UN       | AllergenTypeCodeList | Shellfish                                                                        |
//| UW       | AllergenTypeCodeList | Vehnä ja vehnätuotteet (gluteenia sisältävät viljat)                             |
//| X99      | AllergenTypeCodeList | Ei pakollisesti ilmoitettavia allergeeneja                                       |
//+----------+----------------------+----------------------------------------------------------------------------------+
//135 rows in set (0,06 sec)

    NonFoodIngredientAdditiveList := createNewStringListWithSetOfStrings([]);
    NonFoodIngredientAdditiveList.AddObject('unfragnanced', createNewStringListWithSetOfStrings(['Hajustamaton', 'Unperfumed']));
    NonFoodIngredientAdditiveList.AddObject('sensitiveSkin', createNewStringListWithSetOfStrings(['Herkän ihon tuote']));
    NonFoodIngredientAdditiveList.AddObject('alcohol', createNewStringListWithSetOfStrings(['Alkoholia']));
    NonFoodIngredientAdditiveList.AddObject('noBleach', createNewStringListWithSetOfStrings(['Ei sisällä valkaisuainetta', 'No bleacher']));
    NonFoodIngredientAdditiveList.AddObject('noChlorine', createNewStringListWithSetOfStrings(['Klooriton', 'Chlorine free']));
    NonFoodIngredientAdditiveList.AddObject('oxygenBleached', createNewStringListWithSetOfStrings(['Happivalkaistu']));
    NonFoodIngredientAdditiveList.AddObject('sensitive', createNewStringListWithSetOfStrings(['Sensitive']));


//n:gs1:gdsn:nonfood_ingredient:xsd:3">
//                      <additiveInformation>
//                        <additiveName>Hajustamaton</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>Ei sisällä valkaisuainetta</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>Klooriton</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>Happivalkaistu</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>Herkän ihon tuote</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>Unperfumed</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>No bleacher</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>Chlorine free</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>Oxygen bleached</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>
//                      </additiveInformation>
//                      <additiveInformation>
//                        <additiveName>Sensitive</additiveName>
//                        <levelOfContainmentCode>CONTAINS</levelOfContainmentCode>



    NutritionalClaimTypeCodeList := createNewStringListWithSetOfStrings([]); // ADDED,CONTAINS,ENRICHED_WITH,EXTRA_LEAN,FREE_FROM,FRESH,GUARANTEED_FREE_FROM,GUARANTEED_SOURCE_OF,HEALTHY,HIGH,HIGH_POTENCY,INCREASED,LEAN,LIGHT_LITE,LOW,MADE_WITH,NATURAL,NATURAL_SOURCE_OF,NON_ALCOHOLIC,NO_ADDED,PURE,REAL,REDUCED_LESS,SOURCE_OF,SWEETENED_WITH,UNSWEETENED,VERY_LOW
    NutritionalClaimTypeCodeList.AddObject('contains', createNewStringListWithSetOfStrings(['ADDED', 'CONTAINS', 'ENRICHED_WITH', 'GUARANTEED_SOURCE_OF', 'HIGH', 'INCREASED', 'NATURAL_SOURCE_OF', 'PURE', 'REAL', 'SOURCE_OF', 'MADE_WITH', 'SWEETENED_WITH']));
    NutritionalClaimTypeCodeList.AddObject('freeFrom', createNewStringListWithSetOfStrings(['FREE_FROM', 'GUARANTEED_FREE_FROM', 'UNSWEETENED']));
    NutritionalClaimTypeCodeList.AddObject('low', createNewStringListWithSetOfStrings(['FREE_FROM', 'GUARANTEED_FREE_FROM', 'EXTRA_LEAN', 'LEAN', 'LOW', 'UNSWEETENED', 'VERY_LOW']));
    NutritionalClaimTypeCodeList.AddObject('added', createNewStringListWithSetOfStrings(['ADDED', 'ENRICHED_WITH', 'INCREASED']));
    NutritionalClaimNutrientElementCodeList := createNewStringListWithSetOfStrings([]);
    NutritionalClaimNutrientElementCodeList.AddObject('milk', createNewStringListWithSetOfStrings(['DAIRY', 'MILK', 'MILK_PROTEIN', 'LACTOSE']));
    NutritionalClaimNutrientElementCodeList.AddObject('gluten', createNewStringListWithSetOfStrings(['BARLEY', 'GLUTEN', 'RYE', 'WHEAT', 'WHOLE_WHEAT']));
    NutritionalClaimNutrientElementCodeList.AddObject('egg', createNewStringListWithSetOfStrings(['EGGS']));
    NutritionalClaimNutrientElementCodeList.AddObject('lactose', createNewStringListWithSetOfStrings(['LACTOSE']));
    NutritionalClaimNutrientElementCodeList.AddObject('peanut', createNewStringListWithSetOfStrings(['PEANUTS']));
    NutritionalClaimNutrientElementCodeList.AddObject('nuts', createNewStringListWithSetOfStrings(['BRAZIL_NUTS', 'CASHEWS', 'HAZELNUTS', 'NUTS', 'PECANS', 'PISTACHIOS', 'TREE_NUTS', 'WALNUTS']));
    NutritionalClaimNutrientElementCodeList.AddObject('soy', createNewStringListWithSetOfStrings(['SOY']));
    NutritionalClaimNutrientElementCodeList.AddObject('pork', createNewStringListWithSetOfStrings(['PORK_GELATINE']));
    NutritionalClaimNutrientElementCodeList.AddObject('fish', createNewStringListWithSetOfStrings([]));
    NutritionalClaimNutrientElementCodeList.AddObject('shellfish', createNewStringListWithSetOfStrings(['CRUSTACEAN']));
    NutritionalClaimNutrientElementCodeList.AddObject('additives', createNewStringListWithSetOfStrings(['ADDITIVES', 'ARTIFICIAL_COLOUR', 'ARTIFICIAL_FLAVOUR', 'ARTIFICIAL_PRESERVATIVES', 'ARTIFICIAL_SWEETENERS', 'AZO_DYE', 'COLOURING_AGENTS', 'FLAVOUR_ENHANCERS']));
    NutritionalClaimNutrientElementCodeList.AddObject('birdmeat', createNewStringListWithSetOfStrings([]));
    NutritionalClaimNutrientElementCodeList.AddObject('beefmeat', createNewStringListWithSetOfStrings(['BEEF_GELATINE']));
    NutritionalClaimNutrientElementCodeList.AddObject('rye', createNewStringListWithSetOfStrings(['RYE']));
    NutritionalClaimNutrientElementCodeList.AddObject('wheat', createNewStringListWithSetOfStrings(['WHEAT', 'WHOLE_WHEAT']));
    NutritionalClaimNutrientElementCodeList.AddObject('oat', createNewStringListWithSetOfStrings(['OAT']));
    NutritionalClaimNutrientElementCodeList.AddObject('caffeine', createNewStringListWithSetOfStrings(['CAFFEINE']));
    NutritionalClaimNutrientElementCodeList.AddObject('nitrate', createNewStringListWithSetOfStrings(['NITRATE']));
    NutritionalClaimNutrientElementCodeList.AddObject('sweetener', createNewStringListWithSetOfStrings(['ASPARTAME', 'ARTIFICIAL_SWEETENERS', 'STEVIA', 'SWEETENERS', 'NON_CALORIC_SWEETENERS']));
    NutritionalClaimNutrientElementCodeList.AddObject('nitrites', createNewStringListWithSetOfStrings(['SODIUM_NITRITE']));
    NutritionalClaimNutrientElementCodeList.AddObject('carbonHydrates', createNewStringListWithSetOfStrings(['SUGARS']));
    NutritionalClaimNutrientElementCodeList.AddObject('noAllergens', createNewStringListWithSetOfStrings([]));
//    NutritionalClaimNutrientElementCodeList.AddObject('perfume', createNewStringListWithSetOfStrings([]));


//mysql> select GdsnCode,CodeListName,FinnishDescription from sydanpuu.attributecodes where codelistname='NutritionalClaimNutrientElementCodeList';
//+--------------------------------------+-----------------------------------------+--------------------------------------+
//| GdsnCode                             | CodeListName                            | FinnishDescription                   |
//+--------------------------------------+-----------------------------------------+--------------------------------------+
//| ADDITIVES                            | NutritionalClaimNutrientElementCodeList | Lisäaineita                          |
//| AGAVE_SYRUP                          | NutritionalClaimNutrientElementCodeList | Agave siirappi                       |
//| ALCOHOL                              | NutritionalClaimNutrientElementCodeList | Alkoholi                             |
//| ALMONDS                              | NutritionalClaimNutrientElementCodeList | Manteli                              |
//| ANCIENT_GRAINS                       | NutritionalClaimNutrientElementCodeList | Alkuvilja                            |
//| ANTIBIOTICS                          | NutritionalClaimNutrientElementCodeList | Antibiootti                          |
//| ANTIOXIDANTS                         | NutritionalClaimNutrientElementCodeList | Antioksidantti                       |
//| ARTIFICIAL_COLOUR                    | NutritionalClaimNutrientElementCodeList |                                      |
//| ARTIFICIAL_FLAVOUR                   | NutritionalClaimNutrientElementCodeList | Keinotekoisia aromeita               |
//| ARTIFICIAL_PRESERVATIVES             | NutritionalClaimNutrientElementCodeList | Keinotekoinen säilöntäaine           |
//| ARTIFICIAL_SWEETENERS                | NutritionalClaimNutrientElementCodeList | Keinotekoisia maketusaineita         |
//| ASPARTAME                            | NutritionalClaimNutrientElementCodeList | Aspartaami                           |
//| AZODICARBONAMIDE                     | NutritionalClaimNutrientElementCodeList | Atsodikarbonamidi                    |
//| AZO_DYE                              | NutritionalClaimNutrientElementCodeList | Atsoväri                             |
//| BARLEY                               | NutritionalClaimNutrientElementCodeList | Ohra                                 |
//| BEEF_GELATINE                        | NutritionalClaimNutrientElementCodeList | Liivatetta naudasta                  |
//| BHA                                  | NutritionalClaimNutrientElementCodeList | BHA-happo (butyylihydroksianisoli)   |
//| BHT                                  | NutritionalClaimNutrientElementCodeList | BHT (butyylihydroksitolueeni)        |
//| BIOTIN                               | NutritionalClaimNutrientElementCodeList | Biotiini                             |
//| BRAZIL_NUTS                          | NutritionalClaimNutrientElementCodeList | Parapähkinä                          |
//| CAFFEINE                             | NutritionalClaimNutrientElementCodeList | Kofeiini                             |
//| CALCIUM                              | NutritionalClaimNutrientElementCodeList | Kalsium                              |
//| CALORIE                              | NutritionalClaimNutrientElementCodeList | Kalori                               |
//| CANE_SUGAR                           | NutritionalClaimNutrientElementCodeList | Ruokosokeri                          |
//| CARRAGEENAN                          | NutritionalClaimNutrientElementCodeList | Karrageeni                           |
//| CASHEWS                              | NutritionalClaimNutrientElementCodeList | Cashew-pähkinä                       |
//| CHLORIDE                             | NutritionalClaimNutrientElementCodeList | Kloridi                              |
//| CHOLESTEROL                          | NutritionalClaimNutrientElementCodeList | Kolesteroli                          |
//| CHOLINE                              | NutritionalClaimNutrientElementCodeList | Koliini                              |
//| CHROMIUM                             | NutritionalClaimNutrientElementCodeList | Kromi                                |
//| COLOURING_AGENTS                     | NutritionalClaimNutrientElementCodeList | Väri lisäaineena                     |
//| COPPER                               | NutritionalClaimNutrientElementCodeList | Kupari                               |
//| CORN_SYRUP                           | NutritionalClaimNutrientElementCodeList | Maissisiirappi                       |
//| CRUSTACEAN                           | NutritionalClaimNutrientElementCodeList | Äyriäinen                            |
//| DAIRY                                | NutritionalClaimNutrientElementCodeList | Maitotuote                           |
//| DECAFFEINATED                        | NutritionalClaimNutrientElementCodeList | Kofeiiniton                          |
//| DIETARY_FIBRE                        | NutritionalClaimNutrientElementCodeList | Ravintokuitu                         |
//| EGGS                                 | NutritionalClaimNutrientElementCodeList | Munia                                |
//| ENERGY                               | NutritionalClaimNutrientElementCodeList | Energia                              |
//| FAT                                  | NutritionalClaimNutrientElementCodeList | Rasva                                |
//| FIBRE                                | NutritionalClaimNutrientElementCodeList | Kuitu                                |
//| FLAVOUR_ENHANCERS                    | NutritionalClaimNutrientElementCodeList | Aromivahvenne                        |
//| FLUORIDE                             | NutritionalClaimNutrientElementCodeList | Fluoridi                             |
//| FOLATE                               | NutritionalClaimNutrientElementCodeList | Folaatti                             |
//| FOLIC_ACID                           | NutritionalClaimNutrientElementCodeList | Foolihappo                           |
//| FRUCTOSE                             | NutritionalClaimNutrientElementCodeList | Fruktoosi                            |
//| FRUIT_JUICE                          | NutritionalClaimNutrientElementCodeList | Hedelmä mehu                         |
//| FRUIT_SYRUP                          | NutritionalClaimNutrientElementCodeList | Hedelmä siirappi                     |
//| GELATINE                             | NutritionalClaimNutrientElementCodeList | Liivatetta                           |
//| GLUTEN                               | NutritionalClaimNutrientElementCodeList | Gluteeni                             |
//| GLYZYRRHIZIN                         | NutritionalClaimNutrientElementCodeList | Glysyrritsiini                       |
//| HAZELNUTS                            | NutritionalClaimNutrientElementCodeList | Hasselpähkinä                        |
//| HIGH_FRUCTOSE_CORN_SYRUP             | NutritionalClaimNutrientElementCodeList | Korkeafruktoosinen maissisiirappi    |
//| HONEY                                | NutritionalClaimNutrientElementCodeList | Hunaja                               |
//| HORMONES                             | NutritionalClaimNutrientElementCodeList | Hormonit                             |
//| INSOLUBLE_FIBRE                      | NutritionalClaimNutrientElementCodeList | Liukenematon kuitu                   |
//| IODINE                               | NutritionalClaimNutrientElementCodeList | Jodi                                 |
//| IRON                                 | NutritionalClaimNutrientElementCodeList | Rauta                                |
//| LACTOSE                              | NutritionalClaimNutrientElementCodeList | Laktoosi                             |
//| LIQUORICE                            | NutritionalClaimNutrientElementCodeList | Lakritsi                             |
//| MACADAMIA                            | NutritionalClaimNutrientElementCodeList | Macadamia-pähkinä                    |
//| MAGNESIUM                            | NutritionalClaimNutrientElementCodeList | Magnesium                            |
//| MALT                                 | NutritionalClaimNutrientElementCodeList | Mallas                               |
//| MANGANESE                            | NutritionalClaimNutrientElementCodeList | Mangaani                             |
//| MAPLE_SYRUP                          | NutritionalClaimNutrientElementCodeList | Vaahterasiirappi                     |
//| MILK                                 | NutritionalClaimNutrientElementCodeList | Maito                                |
//| MILK_PROTEIN                         | NutritionalClaimNutrientElementCodeList | Maitoproteiini                       |
//| MOLASSES                             | NutritionalClaimNutrientElementCodeList | Melassi                              |
//| MOLLUSCS                             | NutritionalClaimNutrientElementCodeList | Nilviäinen                           |
//| MOLYBDENUM                           | NutritionalClaimNutrientElementCodeList | Molybdeeni                           |
//| MONO_UNSATURATED_FAT                 | NutritionalClaimNutrientElementCodeList | Kertatyydyttymätöntä rasvaa          |
//| MSG                                  | NutritionalClaimNutrientElementCodeList | Natriumglutamaatti                   |
//| MULTIGRAIN                           | NutritionalClaimNutrientElementCodeList | Monivilja                            |
//| MUSTARD                              | NutritionalClaimNutrientElementCodeList | Sinappi                              |
//| NANOPARTICLE                         | NutritionalClaimNutrientElementCodeList | Nanopartikkeleita                    |
//| NATURALLY_OCCURING_SUGARS            | NutritionalClaimNutrientElementCodeList | Luonnonmukaisia sokereita            |
//| NATURAL_FLAVOUR                      | NutritionalClaimNutrientElementCodeList | Luonnollisesti makeutettu            |
//| NIACIN                               | NutritionalClaimNutrientElementCodeList | Niasiini                             |
//| NITRATE                              | NutritionalClaimNutrientElementCodeList | Nitraattia                           |
//| NON_CALORIC_SWEETENERS               | NutritionalClaimNutrientElementCodeList | Kalorittomat makeutusaineet          |
//| NUTS                                 | NutritionalClaimNutrientElementCodeList | Pähkinöitä                           |
//| OAT                                  | NutritionalClaimNutrientElementCodeList | Kaura                                |
//| OMEGA_3_FATTY_ACIDS                  | NutritionalClaimNutrientElementCodeList | Omega-3 rasvahappoja                 |
//| PANTOTHENIC_ACID                     | NutritionalClaimNutrientElementCodeList | Pantoteenihappo                      |
//| PARTIALLY_HYDROGENATED_VEGETABLE_OIL | NutritionalClaimNutrientElementCodeList | Osittain hydrogenoitu kasviöljy      |
//| PEANUTS                              | NutritionalClaimNutrientElementCodeList | Maapähkinöitä                        |
//| PECANS                               | NutritionalClaimNutrientElementCodeList | Pekaanipähkinä                       |
//| PHENYLALANINE                        | NutritionalClaimNutrientElementCodeList | Fenyylialaniini                      |
//| PHOSPHATE                            | NutritionalClaimNutrientElementCodeList | Fosfaatti                            |
//| PHOSPHORUS                           | NutritionalClaimNutrientElementCodeList | Fosfori                              |
//| PINENUTS                             | NutritionalClaimNutrientElementCodeList | Pinjansiemen                         |
//| PISTACHIOS                           | NutritionalClaimNutrientElementCodeList | Pistaasipähkinä                      |
//| PLANT_STEROLS                        | NutritionalClaimNutrientElementCodeList | Kasvisterolit                        |
//| POLYUNSATURATED_FAT                  | NutritionalClaimNutrientElementCodeList | Monityydyttymättömiä rasvahappoja    |
//| PORK_GELATINE                        | NutritionalClaimNutrientElementCodeList | Liivatetta, siasta                   |
//| POTASSIUM                            | NutritionalClaimNutrientElementCodeList | Kalium                               |
//| PRESERVATIVES                        | NutritionalClaimNutrientElementCodeList | Säilöntäaine                         |
//| PROBIOTICS                           | NutritionalClaimNutrientElementCodeList | Probiootti                           |
//| PROTEIN                              | NutritionalClaimNutrientElementCodeList | Proteiini                            |
//| RAW_BEET_SUGAR                       | NutritionalClaimNutrientElementCodeList | Juurikassokeri                       |
//| RIBOFLAVIN                           | NutritionalClaimNutrientElementCodeList | Riboflaviini                         |
//| RYE                                  | NutritionalClaimNutrientElementCodeList | Ruis                                 |
//| SATURATED_FAT                        | NutritionalClaimNutrientElementCodeList | Tyydyttynyt rasva (-pitoisuus)       |
//| SELENIUM                             | NutritionalClaimNutrientElementCodeList | Seleeni                              |
//| SESAME                               | NutritionalClaimNutrientElementCodeList | Seesaminsiemen                       |
//| SMOKE_FLAVOUR                        | NutritionalClaimNutrientElementCodeList | Savustamalla maustettu               |
//| SODIUM_NITRITE                       | NutritionalClaimNutrientElementCodeList | Natriumnitriitti                     |
//| SODIUM_SALT                          | NutritionalClaimNutrientElementCodeList | Natrium tai suola                    |
//| SOLUBLE_FIBRE                        | NutritionalClaimNutrientElementCodeList | Liukoinen kuitu                      |
//| SORBITOL                             | NutritionalClaimNutrientElementCodeList | Sorbitolia                           |
//| SOY                                  | NutritionalClaimNutrientElementCodeList | Soija                                |
//| STEVIA                               | NutritionalClaimNutrientElementCodeList | Stevia                               |
//| SUGARS                               | NutritionalClaimNutrientElementCodeList | Sokereita                            |
//| SWEETENERS                           | NutritionalClaimNutrientElementCodeList | Makeutusaineita                      |
//| THIAMIN                              | NutritionalClaimNutrientElementCodeList | Tiamiini                             |
//| TRANS_FAT                            | NutritionalClaimNutrientElementCodeList | Transrasva                           |
//| TREE_NUTS                            | NutritionalClaimNutrientElementCodeList | Puupähkinä                           |
//| TRITICALE                            | NutritionalClaimNutrientElementCodeList | Ruisvehnä, tritikali                 |
//| UNSATURATED_FAT                      | NutritionalClaimNutrientElementCodeList | Tyydyttymätön rasva (-pitoisuus)     |
//| VITAMINS_AND_OR_MINERALS             | NutritionalClaimNutrientElementCodeList | Vitamiineja ja/tai mineraaleja       |
//| VITAMIN_A                            | NutritionalClaimNutrientElementCodeList | A-vitamiini                          |
//| VITAMIN_B12                          | NutritionalClaimNutrientElementCodeList | B12-vitamiini                        |
//| VITAMIN_B6                           | NutritionalClaimNutrientElementCodeList | B6-vitamiini                         |
//| VITAMIN_C                            | NutritionalClaimNutrientElementCodeList | C-vitamiini                          |
//| VITAMIN_D                            | NutritionalClaimNutrientElementCodeList | D-vitamiini                          |
//| VITAMIN_E                            | NutritionalClaimNutrientElementCodeList | E-vitamiini                          |
//| VITAMIN_K                            | NutritionalClaimNutrientElementCodeList | K-vitamiini                          |
//| WALNUTS                              | NutritionalClaimNutrientElementCodeList | Saksanpähkinä                        |
//| WATER                                | NutritionalClaimNutrientElementCodeList | Vettä                                |
//| WHEAT                                | NutritionalClaimNutrientElementCodeList | Vehnä                                |
//| WHITE_SUGAR                          | NutritionalClaimNutrientElementCodeList | Valkoinen sokeri                     |
//| WHOLE_GRAIN                          | NutritionalClaimNutrientElementCodeList | Täysjyvä                             |
//| WHOLE_WHEAT                          | NutritionalClaimNutrientElementCodeList | Kokojyvä                             |
//| YEAST                                | NutritionalClaimNutrientElementCodeList | Hiiva                                |
//| ZINC                                 | NutritionalClaimNutrientElementCodeList | Sinkki                               |
//+--------------------------------------+-----------------------------------------+--------------------------------------+
//135 rows in set (0,05 sec)


//  csvFileConfig.AddObject('containsPerfume', csvFileColumn.Create(columnIndex, 'containsPerfume', 'Sisältää hajusteita', -1, -1, -1, '', ''));    *************************************
//  csvFileConfig.AddObject('suitableForSensitiveSkin', csvFileColumn.Create(columnIndex, 'suitableForSensitiveSkin', 'Sopii herkälle iholle', -1, -1, -1, '', '')); *******************

    PackagingMarkedLabelAccreditationCodeList := createNewStringListWithSetOfStrings([]);
    PackagingMarkedLabelAccreditationCodeList.AddObject('bioOrganic', createNewStringListWithSetOfStrings(['AGENCE_BIO','AUSTRIA_BIO_GARANTIE','BETER_LEVEN_3_STER','BIODYNAMIC_CERTIFICATION','BIOLAND','BIO_AUSTRIA_LABEL','BIO_CZECH_LABEL','BIO_FISCH','BIO_LABEL_GERMAN','BIO_RING_ALLGAEU','BIO_SUISSE_BUD_SEAL','ECOCERT_COSMOS_ORGANIC','ECO_LABEL_LADYBUG','EU_ORGANIC_FARMING','OECD_BIO_INGREDIENTS','OFFICIAL_ECO_LABEL_SUN']));


  except
    on E: Exception do begin
      // write log E.Message;
      SiMain.LogException(E,'cant create sets');
    end;
  end;
end;

function TSynkkaDescriptions.findCodeListForAttribute(moduleName: string; attributeName: string): string;
var
  found: boolean;
  //field: TField;
begin
  Result := '';
  Simain.LogWarning( 'findCodeListForAttribute not implemented' );
//  try
//    found := findRecordForAttribute(moduleName, attributeName);
//    if (found) then begin
//      field := attributeHierarchy.FindField('CodeListName');
//      if (field <> nil) then Result := field.AsString;
//    end;
//  except
//    on E: Exception do begin
//      SiMain.LogException(E, 'cant find codelists for attributes');
//      Result := '';
//    end;
//  end;
end;

function TSynkkaDescriptions.findRecordForAttribute(moduleName: string; attributeName: string): boolean;
var
  found: boolean;
begin
  Result := false;
  Simain.LogWarning( 'findRecordForAttribute not implemented' );
//  try
//    if not (attributeHierarchy <> nil) then exit;
//    Result := attributeHierarchy.Locate('ModuleName;AttributeName', VarArrayOf([moduleName, attributeName]), [loCaseInsensitive]);
//  except
//    on E: Exception do begin
//      SiMain.LogException(E, 'cant find record for attribute in hierarchy');
//      Result := false;
//    end;
//  end;
end;

function TSynkkaDescriptions.findDescriptionForCode(codeListName: string; codeValue: string): string;
var
  found: boolean;
//  field: TField;
begin
  Result := '';
  Simain.LogWarning( 'findDescriptionForCode not implemented' );
//  try
//    found := findRecordForCode(codeListName, codeValue);
//    if (found) then begin
//      field := codeList.FindField('FinnishDescription');
//      if (field <> nil) then Result := field.AsString;
//    end;
//  except
//    on E: Exception do begin
//      SiMain.LogException(E, 'cant find description for code');
//      Result := '';
//    end;
//  end;
end;

function TSynkkaDescriptions.findRecordForCode(codeListName: string; codeValue: string): boolean;
var
  found: boolean;
begin
  Result := false;
  Simain.LogWarning( 'findRecordForCode not implemented' );
//  try
//    if not (codeList <> nil) then exit;
//    Result := codeList.Locate('CodeListName;GdsnCode', VarArrayOf([codeListName, codeValue]), [loCaseInsensitive]);
//  except
//    on E: Exception do begin
//      SiMain.LogException(E, 'cant find record for code');
//      Result := false;
//    end;
//  end;
end;

function TSynkkaDescriptions.findRecordForGPCbrickCode(gpcCode: string): boolean;
var
  found: boolean;
begin
  Result := false;
  Simain.LogWarning( 'findRecordForGPCbrickCode not implemented' );
//  try
//    if not (gpcTranslations <> nil) then exit;
//    Result := gpcTranslations.Locate('brickCode', VarArrayOf([gpcCode]), [loCaseInsensitive]);
//  except
//    on E: Exception do begin
//      SiMain.LogException(E, 'cant find record for GP brick code');
//      Result := false;
//    end;
//  end;
end;

function TSynkkaDescriptions.getFinnishTranslationForGPCbrickCode(gpcCode: string): string;
var
  found: boolean;
//  field: TField;
begin
  Result := '';
  Simain.LogWarning( 'getFinnishTranslationForGPCbrickCode not implemented' );
//  try
//    found := findRecordForGPCbrickCode(gpcCode);
//    if (found) then begin
//      field := gpcTranslations.FindField('brickDescription');
//      if (field <> nil) then Result := field.AsString;
//    end;
//  except
//    on E: Exception do begin
//      SiMain.LogException(E, 'cant get finnish translation for GP brick code');
//      Result := '';
//    end;
//  end;
end;

function TSynkkaDescriptions.getFinnishDescrpitionForAttributeName(moduleName: string; attributeName: string): string;
var
  found: boolean;
//  field: TField;
begin
  Result := '';
  Simain.LogWarning( 'getFinnishDescrpitionForAttributeName not implemented' );
//  try
//    found := findRecordForAttribute(moduleName, attributeName);
//    if (found) then begin
//      field := attributeHierarchy.FindField('FinnishLabelText');
//      if (field <> nil) then Result := field.AsString;
//    end;
//  except
//    on E: Exception do begin
//     // write log E.Message;
//      SiMain.LogException(E, 'cant get finnish description for attribute name');
//      Result := '';
//    end;
//  end;
end;

function TSynkkaDescriptions.getFinnishDescrpitionForAttributeValue(moduleName: string; attributeName: string; attributeValue: string): string;
var
  found: boolean;
//  field: TField;
  CodeListName: string;
begin
  Result := '';
  Simain.LogWarning( 'getFinnishDescrpitionForAttributeValue not implemented' );
//  try
//    if not ((length(moduleName) > 0) and (length(attributeName) > 0) and (length(attributeValue) > 0)) then exit;
//    CodeListName := findCodeListForAttribute(moduleName, attributeName);
//    if not (length(CodeListName) > 0) then exit;
//    Result := findDescriptionForCode(CodeListName, attributeValue);
//  except
//    on E: Exception do begin
//      SiMain.LogException(E, ' cant get finnish description for attribute value');
//      Result := '';
//    end;
//  end;
end;

function TSynkkaDescriptions.getValueForAllergenAttribute(const containmentType: string; const allergenName: string; containmentTypeValue: string; allergenValue: string): integer;
var
  containmentValues, allergenValues: TStringList;
begin
  Result := null;
  try
    if not ((length(containmentType) > 0) and (length(allergenName) > 0) and (length(containmentTypeValue) > 0) and (length(allergenValue) > 0)) then exit;
    containmentValues := TSynkkaXMLparser.getObjectFromStringListByKey(LevelOfContainmentCodeList, containmentType);
    allergenValues := TSynkkaXMLparser.getObjectFromStringListByKey(AllergenTypeCodeList, allergenName);
    if ((containmentValues.IndexOf(containmentTypeValue) > -1) and (allergenValues.IndexOf(allergenValue) > -1)) then Result := 1 else Result := 0;
  except
    on E: Exception do begin
     // write log E.Message;
      SiMain.LogException(E,' cant get value for allergen attributes');
    end;
  end;
end;

function TSynkkaDescriptions.getValueForNonFoodAdditiveAttribute(const containmentType: string; const additiveName: string; containmentTypeValue: string; additiveNameValue: string): integer;
var
  containmentValues, additiveValues: TStringList;
begin
  Result := null;
  try
    if not ((length(containmentType) > 0) and (length(additiveName) > 0) and (length(containmentTypeValue) > 0) and (length(additiveNameValue) > 0)) then exit;
    containmentValues := TSynkkaXMLparser.getObjectFromStringListByKey(LevelOfContainmentCodeList, containmentType);
    additiveValues := TSynkkaXMLparser.getObjectFromStringListByKey(NonFoodIngredientAdditiveList, additiveName);
    if ((containmentValues.IndexOf(containmentTypeValue) > -1) and (additiveValues.IndexOf(additiveNameValue) > -1)) then Result := 1 else Result := 0;
  except
    on E: Exception do begin
     // write log E.Message;
      SiMain.LogException(E, 'cant get value for nonfood additives');
    end;
  end;
end;

function TSynkkaDescriptions.getValueForNutritionalAttribute(const containmentType: string; const nutrientName: string; containmentTypeValue: string; nutrientNameValue: string): integer;
var
  containmentValues, nutrientValues: TStringList;
begin
  Result := null;
  try
    if not ((length(containmentType) > 0) and (length(nutrientName) > 0) and (length(containmentTypeValue) > 0) and (length(nutrientNameValue) > 0)) then exit;
    containmentValues := TSynkkaXMLparser.getObjectFromStringListByKey(NutritionalClaimTypeCodeList, containmentType);
    nutrientValues := TSynkkaXMLparser.getObjectFromStringListByKey(NutritionalClaimNutrientElementCodeList, nutrientName);
    if ((containmentValues.IndexOf(containmentTypeValue) > -1) and (nutrientValues.IndexOf(nutrientNameValue) > -1)) then Result := 1 else Result := 0;
  except
    on E: Exception do begin
     // write log E.Message;
      SiMain.LogException(E, ' cant get value for nutritional attributes');
    end;
  end;
end;

function TSynkkaDescriptions.getValueForPackagingMarkedLabelAccreditationCodeAttribute(const key: string; const value: string): integer;
var
  allValues: TStringList;
begin
  Result := null;
  try
    if not ((length(key) > 0) and (length(value) > 0)) then exit;
    allValues := TSynkkaXMLparser.getObjectFromStringListByKey(PackagingMarkedLabelAccreditationCodeList, key);
    if ((allValues.IndexOf(value) > -1)) then Result := 1 else Result := 0;
  except
    on E: Exception do begin
     // write log E.Message;
      SiMain.LogException(E, ' cant get value for getValueForPackagingMarkedLabelAccreditationCodeAttribute');
    end;
  end;
end;

end.
