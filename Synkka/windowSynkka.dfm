object window_Synkka: Twindow_Synkka
  Left = 0
  Top = 0
  Caption = 'Synkka for Magento'
  ClientHeight = 333
  ClientWidth = 556
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    556
    333)
  PixelsPerInch = 96
  TextHeight = 13
  object Edit_Host: TEdit
    Left = 8
    Top = 8
    Width = 65
    Height = 21
    Hint = 'host'
    TabOrder = 0
    Text = '127.0.0.1'
  end
  object Edit_Port: TEdit
    Left = 79
    Top = 8
    Width = 42
    Height = 21
    Hint = 'port'
    TabOrder = 1
    Text = '3306'
  end
  object Edit_user: TEdit
    Left = 127
    Top = 8
    Width = 42
    Height = 21
    Hint = 'user'
    TabOrder = 2
    Text = 'prosys'
  end
  object Edit_Password: TEdit
    Left = 175
    Top = 8
    Width = 50
    Height = 21
    Hint = 'password'
    TabOrder = 3
    Text = 'lumik0la'
  end
  object Edit_Database: TEdit
    Left = 231
    Top = 8
    Width = 65
    Height = 21
    Hint = 'database'
    TabOrder = 4
    Text = 'sydanpuu'
  end
  object Edit_Table: TEdit
    Left = 231
    Top = 35
    Width = 98
    Height = 21
    Hint = 'table'
    TabOrder = 5
    Text = 'iteminformation'
  end
  object Button_Connect: TButton
    Left = 8
    Top = 35
    Width = 129
    Height = 25
    Caption = 'Prepare MariaDB'
    TabOrder = 6
    OnClick = Button_ConnectClick
  end
  object Button_Heartwood: TButton
    Left = 8
    Top = 66
    Width = 129
    Height = 25
    Caption = 'Open Heartwood'
    TabOrder = 7
    OnClick = Button_HeartwoodClick
  end
  object Memo_Log: TMemo
    Left = 8
    Top = 97
    Width = 540
    Height = 228
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo_Log')
    ScrollBars = ssBoth
    TabOrder = 8
  end
  object Button_CSV_Verify: TButton
    Left = 143
    Top = 66
    Width = 82
    Height = 25
    Caption = 'Verify CSV'
    Enabled = False
    TabOrder = 9
    OnClick = Button_CSV_VerifyClick
  end
  object Button_UpdateOnSale: TButton
    Left = 335
    Top = 8
    Width = 132
    Height = 25
    Caption = 'Update OnSale'
    TabOrder = 10
    OnClick = Button_UpdateOnSaleClick
  end
  object Button_ParseXML: TButton
    Left = 473
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Parse XML'
    TabOrder = 11
    OnClick = Button_ParseXMLClick
  end
  object btn_ExportCSV: TButton
    Left = 473
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Export CSV'
    TabOrder = 12
    OnClick = btn_ExportCSVClick
  end
  object btn_ExportMultiLanguage: TButton
    Left = 473
    Top = 66
    Width = 75
    Height = 25
    Caption = 'Export Multi'
    TabOrder = 13
    OnClick = btn_ExportMultiLanguageClick
  end
  object Button_UpdateWaiting: TButton
    Left = 335
    Top = 39
    Width = 132
    Height = 25
    Caption = 'Update Incoming+Tauko'
    TabOrder = 14
    OnClick = Button_UpdateWaitingClick
  end
  object btn_GetSynkkaChoices: TButton
    Left = 335
    Top = 66
    Width = 132
    Height = 25
    Caption = 'Get Synkka choices'
    TabOrder = 15
    OnClick = btn_GetSynkkaChoicesClick
  end
  object btn_JsonTest: TButton
    Left = 280
    Top = 66
    Width = 49
    Height = 25
    Caption = 'Json T'
    TabOrder = 16
    OnClick = btn_JsonTestClick
  end
  object FDQuery_synkka: TFDQuery
    IndexFieldNames = 'GTIN'
    Connection = FDConnection_synkka
    Left = 136
    Top = 200
  end
  object FDConnection_synkka: TFDConnection
    Params.Strings = (
      'User_Name=prosys'
      'Password=lumik0la'
      'Database=sydanpuu'
      'DriverID=MySQL')
    Left = 136
    Top = 152
  end
  object FDQuery_Heartwood: TFDQuery
    Connection = FDConnection_Heartwood
    Left = 392
    Top = 184
  end
  object FDConnection_Heartwood: TFDConnection
    Params.Strings = (
      'Database=C:\Servers\Heartwood\production\HEARTWOOD.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=ISO8859_1'
      'ExtendedMetadata=True'
      'DriverID=FB')
    LoginPrompt = False
    Left = 392
    Top = 136
  end
end
