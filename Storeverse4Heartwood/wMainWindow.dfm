object window_Server: Twindow_Server
  Left = 0
  Top = 0
  Caption = 'Heartwood 4 Storeverse'
  ClientHeight = 320
  ClientWidth = 531
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    531
    320)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 333
    Top = 8
    Width = 190
    Height = 13
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    Caption = 'PRODUCTS autoinc disabled in DB -hack'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label_InstanceInfo: TLabel
    Left = 8
    Top = 302
    Width = 93
    Height = 13
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Label_InstanceInfo'
  end
  object btn_GetModifiedOrders: TButton
    Left = 136
    Top = 8
    Width = 105
    Height = 25
    Caption = 'Get modified orders'
    TabOrder = 0
    OnClick = btn_GetModifiedOrdersClick
  end
  object Memo_Log: TMemo
    Left = 8
    Top = 39
    Width = 515
    Height = 125
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object btn_GetModifiedProducts: TButton
    Left = 8
    Top = 8
    Width = 122
    Height = 25
    Caption = 'Get modified products'
    TabOrder = 2
    OnClick = btn_GetModifiedProductsClick
  end
  object Memo_TimedLog: TMemo
    Left = 8
    Top = 170
    Width = 515
    Height = 126
    Anchors = [akLeft, akRight, akBottom]
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=C:\Servers\Luomuntarina\HEARTWOOD.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=ISO8859_1'
      'ExtendedMetadata=True'
      'DriverID=FB')
    LoginPrompt = False
    Left = 200
    Top = 16
  end
  object FDQuery_Products: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint]
    UpdateOptions.FetchGeneratorsPoint = gpNone
    UpdateOptions.UpdateTableName = 'PRODUCTS'
    UpdateOptions.KeyFields = 'ID'
    SQL.Strings = (
      
        'SELECT ID, PRODUCTNAME, PRODUCTSTATE, PROVIDERID, UNITDEPTH, UNI' +
        'THEIGHT, UNITPRICE, UNITTYPE, UNITVOLUME, UNITWEIGTH, UNITWIDTH,'
      
        '  VAT, WAREHOUSEALARMLIMIT, WAREHOUSECOUNT, WAREHOUSEUSE, BARCOD' +
        'E, STORAGETYPE, INSTOCK, LIFECYCLE, DATETRACKING,'
      
        '  SUPPLIERSNAME, SUPPLIERSCODE, EXTERNALID, PRODUCTCOLLECTINGAND' +
        'CHARGE, TERMINALID, DELIVERYINSALESBATCHES,'
      
        '  SALEBATCHBARCODE, SALEBATCHSIZE, SALEBATCHUNITWIDTH, SALEBATCH' +
        'UNITHEIGHT, SALEBATCHUNITDEPTH, PRODUCTIMAGEURL,'
      
        '  PURCHASEPRICE, PRICE, ORDERAMOUNTMIN, ORDERAMOUNTMAX, MANUFACT' +
        'URER, '
      '  MINIMUMUSAGEDAYS, MANUFACTURERNAME, TUKOPRODUCTTREE, '
      
        '  AWHORDER_ALARMAMOUNT, AWHORDER_ORDERAMOUNTINSALEBATCH, PRODUCT' +
        'ORDER,'
      '  COMPARISONFACTOR, COMPARISONUNIT, '
      '  DELETED, CREATED, MODIFIED'
      'FROM PRODUCTS'
      '')
    Left = 496
    Top = 168
  end
  object FDQuery_UserIDandUniqueEntityID_ByExternalID2: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT'
      '   "ID", "UNIQUEENTITYID", "EXTERNALGUID"'
      'FROM'
      '   USERS'
      'WHERE'
      '   EXTERNALGUID = :paramEXTERNALGUID')
    Left = 200
    Top = 72
    ParamData = <
      item
        Name = 'PARAMEXTERNALGUID'
        ParamType = ptInput
      end>
  end
  object FDQuery_UniqueEntities_Only10Rows: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint, uvGeneratorName]
    UpdateOptions.FetchGeneratorsPoint = gpImmediate
    UpdateOptions.GeneratorName = 'GEN_UNIQUEENTITIES_ID'
    UpdateOptions.UpdateTableName = 'UNIQUEENTITIES'
    UpdateOptions.KeyFields = 'ID'
    UpdateOptions.AutoIncFields = 'ID'
    SQL.Strings = (
      'SELECT'
      '   "ID", "CREATEDATE", "MODIFIEDDATE", "CREDITLEVEL", '
      '   "ENTITYTYPE", "NAME", "ADDRESS", "POSTNUMBER", "POSTOFFICE", '
      '   "INVOICECOMPANY", "INVOICEADDRESS", "INVOICEPOSTNUMBER", '
      '   "INVOICEPOSTOFFICE", "VERIFIED", "YTUNNUS", "INVOICEMETHOD", '
      '   "NOTESFORDELIVERY", "PHONENUMBERFORDELIVERY", "INVOICEOVT",'
      '   "EXTERNALCUSTOMERGROUPID"'
      'FROM'
      '   UNIQUEENTITIES'
      'WHERE'
      '   ID = :paramID'
      'ORDER BY ID DESC'
      'ROWS 10')
    Left = 200
    Top = 120
    ParamData = <
      item
        Name = 'PARAMID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object FDQuery_Users_Only10Rows: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint, uvGeneratorName]
    UpdateOptions.FetchGeneratorsPoint = gpImmediate
    UpdateOptions.GeneratorName = 'GEN_USERS_ID'
    UpdateOptions.UpdateTableName = 'USERS'
    UpdateOptions.KeyFields = 'ID'
    UpdateOptions.AutoIncFields = 'ID'
    SQL.Strings = (
      'SELECT'
      '   "ID", "CREATEDATE", "FIRSTNAME", "LASTNAME", "EMAIL", '
      '   "LASTLOGIN", "MOBILEPHONE", "OTHERPHONE", "USERPASSWORD", '
      '   "MODIFIEDDATE", "TASK", "UNIQUEENTITYID", "USERNAME", '
      '   "USERTYPE", "USERRIGHTS", "USERSETTINGS", "CARD_YB", '
      '   "EXTERNALID", "EXTERNALID2", "USERPROFILE",'
      '   "EXTERNALGUID"'
      'FROM'
      '   USERS'
      'WHERE ID = :paramID'
      'ORDER BY ID DESC'
      'ROWS 10')
    Left = 200
    Top = 168
    ParamData = <
      item
        Name = 'PARAMID'
        ParamType = ptInput
      end>
  end
  object FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint, uvGeneratorName]
    UpdateOptions.FetchGeneratorsPoint = gpImmediate
    UpdateOptions.GeneratorName = 'GEN_CUSTOMERDELIVERIES_ID'
    UpdateOptions.UpdateTableName = 'CUSTOMERDELIVERIES'
    UpdateOptions.KeyFields = 'ID'
    UpdateOptions.AutoIncFields = 'ID'
    SQL.Strings = (
      'SELECT'
      '   "ID", "DELIVERYSTATE", "UNIQUEENTITYID", "DELIVERYAREATYPE",'
      
        '   "DELIVERYDATE", "DELIVERYTIME", "DELIVERYSLOTLENGTHINHOURS", ' +
        '"PICKUPLOCATIONID", "DELIVERYID",'
      
        '   "DELIVERYADDRESS", "DELIVERYCOMPANY", "DELIVERYPOSTNUMBER", "' +
        'DELIVERYPOSTOFFICE",'
      
        '   "DELIVERYPHONENUMBER", "DELIVERYPHONENUMBER2", "DELIVERYDOORC' +
        'ODE", "DELIVERYADDRESSCOMMENTS",'
      '   "DELIVERYAGENTNAME", "DELIVERYAGENTID",'
      '   "DRIVERID", "LOCKEDBYUSERID", "MODIFIEDDATETIME", "DELETED"'
      'FROM'
      '   CUSTOMERDELIVERIES'
      'WHERE'
      
        '   UNIQUEENTITYID = :paramUNIQUEENTITYID AND DELIVERYADDRESS = :' +
        'paramDELIVERYADDRESS AND'
      
        '   DELIVERYDATE = :paramDELIVERYDATE AND DELIVERYTIME = :paramDE' +
        'LIVERYTIME AND'
      
        '   DELIVERYAREATYPE = :paramDELIVERYAREATYPE AND PICKUPLOCATIONI' +
        'D = :paramPICKUPLOCATIONID AND'
      '   DELIVERYSLOTLENGTHINHOURS = :paramDELIVERYSLOTLENGTHINHOURS')
    Left = 200
    Top = 216
    ParamData = <
      item
        Name = 'PARAMUNIQUEENTITYID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'PARAMDELIVERYADDRESS'
        DataType = ftString
        ParamType = ptInput
        Size = 50
      end
      item
        Name = 'PARAMDELIVERYDATE'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'PARAMDELIVERYTIME'
        DataType = ftTime
        ParamType = ptInput
      end
      item
        Name = 'PARAMDELIVERYAREATYPE'
        DataType = ftString
        ParamType = ptInput
        Size = 1
      end
      item
        Name = 'PARAMPICKUPLOCATIONID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'PARAMDELIVERYSLOTLENGTHINHOURS'
        DataType = ftSmallint
        ParamType = ptInput
      end>
  end
  object FDQuery_Payments_ByExternalID: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint, uvGeneratorName]
    UpdateOptions.FetchGeneratorsPoint = gpImmediate
    UpdateOptions.GeneratorName = 'GEN_PAYMENTS_ID'
    UpdateOptions.UpdateTableName = 'PAYMENTS'
    UpdateOptions.KeyFields = 'ID'
    UpdateOptions.AutoIncFields = 'ID'
    SQL.Strings = (
      'SELECT'
      '   "ID", "UNIQUEENTITYID", "PAYMENTTYPE", "PAYMENTSTATE", '
      
        '   "PAYMENTGATEWAYID", "TOTALPRICE", "TOTALPRICEDISCOUNT", "GIFT' +
        'CARDS",'
      '   "PAIDWITHGIFTCARDS", "PAIDWITHKH24CREDIT",'
      '   "DELIVERYPRICE", "DELIVERYPRICEDISCOUNT", "INVOICEADDRESS", '
      '   "INVOICECOMPANYNAME", "INVOICEPOSTNUMBER", "INVOICEDATE", '
      '   "INVOICEDUEDATE", "INVOICENUMBER", "INVOICEPOSTOFFICE", '
      '   "INVOICEREFERENCENUMBER", "CREATED",'
      '   COMMENTSFROMCUSTOMER, COMMENTSTOCUSTOMER,'
      
        '   "PROCESSED", "FULLYPAIDDATE", "EXTERNALID", "CUSTOMERDELIVERY' +
        'ID",'
      '   "DELETED"'
      'FROM'
      '   PAYMENTS'
      'WHERE'
      '   EXTERNALID = :paramEXTERNALID')
    Left = 200
    Top = 264
    ParamData = <
      item
        Name = 'PARAMEXTERNALID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object FDQuery_Orders_ByPaymentID: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint, uvGeneratorName]
    UpdateOptions.FetchGeneratorsPoint = gpImmediate
    UpdateOptions.GeneratorName = 'GEN_ORDERS_ID'
    UpdateOptions.UpdateTableName = 'ORDERS'
    UpdateOptions.KeyFields = 'ID'
    UpdateOptions.AutoIncFields = 'ID'
    SQL.Strings = (
      'SELECT'
      
        '   "ID", "UNIQUEENTITYID", "PROVIDERID", "CUSTOMERID", "PAYMENTI' +
        'D",'
      '   "ORDERSTATE", "ORDERNUMBER", "ORDERDATE", "ORDERTIME", '
      
        '   "ORDERBYUSERID", "PAYMENTTYPE", "PAYMENTSTATE", "PAYMENTGATEW' +
        'AYID", '
      '   "TOTALPRICE", "INVOICEADDRESS", "INVOICECOMPANYNAME", '
      '   "INVOICEPOSTNUMBER", "INVOICEDATE", "INVOICEDUEDATE", '
      
        '   "INVOICENUMBER", "INVOICEPOSTOFFICE", "INVOICEREFERENCENUMBER' +
        '", '
      
        '   "COMMENTSTOCUSTOMER", "COMMENTSTOPROVIDER", "COMMENTSWITHINPR' +
        'OVIDER", '
      '   "DELIVEREDTIME", "DELIVERYDATE", "DELIVERYADDRESS", '
      
        '   "DELIVERYCOMPANY", "DELIVERYPOSTNUMBER", "DELIVERYPOSTOFFICE"' +
        ', '
      '   "DELIVERYID", "DELIVERYTYPE", "FULLYPAIDDATE", "MODIFIED", '
      '   "DELIVERYCOLLECTINGTIMEID"'
      'FROM'
      '   ORDERS'
      'WHERE'
      '   PAYMENTID = :paramPAYMENTID')
    Left = 200
    Top = 312
    ParamData = <
      item
        Name = 'PARAMPAYMENTID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object FDQuery_OrderRows_ByOrdersPaymentID: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint, uvGeneratorName]
    UpdateOptions.FetchGeneratorsPoint = gpImmediate
    UpdateOptions.GeneratorName = 'GEN_ORDERROWS_ID'
    UpdateOptions.UpdateTableName = 'ORDERROWS'
    UpdateOptions.KeyFields = 'ID'
    UpdateOptions.AutoIncFields = 'ID'
    SQL.Strings = (
      'SELECT'
      
        '   ORDERROWS."ID", ORDERROWS."ORDERID", ORDERROWS."PRODUCTNAME",' +
        ' ORDERROWS."AMOUNT", ORDERROWS."PCSAMOUNT", ORDERROWS."VAT",'
      
        '   ORDERROWS."DISCOUNT", ORDERROWS."PRICE", ORDERROWS."PURCHASEP' +
        'RICE", ORDERROWS."PRODUCTID", ORDERROWS."ORDERROWCHANGETYPE",'
      
        '   ORDERROWS."ROWORDER", ORDERROWS."ORDERAMOUNT", ORDERROWS."COL' +
        'LECTEDAMOUNT", ORDERROWS."COLLECTNOTES", ORDERROWS."EXTERNALID"'
      'FROM ORDERROWS'
      'JOIN ORDERS ON ORDERS.ID = ORDERROWS.ORDERID'
      'WHERE ORDERS.PAYMENTID = :paramPAYMENTID')
    Left = 200
    Top = 360
    ParamData = <
      item
        Name = 'PARAMPAYMENTID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object FDQuery_KH24_Product_ByGTIN: TFDQuery
    CachedUpdates = True
    Connection = FDConnection_KH24
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint, uvGeneratorName]
    UpdateOptions.FetchGeneratorsPoint = gpImmediate
    UpdateOptions.GeneratorName = 'GEN_PRODUCTS_ID'
    UpdateOptions.UpdateTableName = 'PRODUCTS'
    UpdateOptions.KeyFields = 'ID'
    UpdateOptions.AutoIncFields = 'ID'
    SQL.Strings = (
      
        'SELECT ID, PRODUCTNAME, PRODUCTSTATE, PROVIDERID, UNITDEPTH, UNI' +
        'THEIGHT, UNITPRICE, UNITTYPE, UNITVOLUME, UNITWEIGTH, UNITWIDTH,'
      
        '  VAT, WAREHOUSEALARMLIMIT, WAREHOUSECOUNT, WAREHOUSEUSE, BARCOD' +
        'E, STORAGETYPE, INSTOCK, LIFECYCLE, DATETRACKING,'
      
        '  SUPPLIERSNAME, SUPPLIERSCODE, EXTERNALID, PRODUCTCOLLECTINGAND' +
        'CHARGE, TERMINALID, DELIVERYINSALESBATCHES,'
      
        '  SALEBATCHBARCODE, SALEBATCHSIZE, SALEBATCHUNITWIDTH, SALEBATCH' +
        'UNITHEIGHT, SALEBATCHUNITDEPTH, PRODUCTIMAGEURL,'
      
        '  PURCHASEPRICE, PRICE, ORDERAMOUNTMIN, ORDERAMOUNTMAX, MANUFACT' +
        'URER, '
      '  MINIMUMUSAGEDAYS, MANUFACTURERNAME, TUKOPRODUCTTREE, '
      '  AWHORDER_ALARMAMOUNT, AWHORDER_ORDERAMOUNTINSALEBATCH,'
      '  COMPARISONFACTOR, COMPARISONUNIT, PRODUCTORDER,'
      '  DELETED, CREATED, MODIFIED'
      'FROM PRODUCTS'
      'WHERE BARCODE = :paramBARCODE'
      '')
    Left = 504
    Top = 352
    ParamData = <
      item
        Name = 'PARAMBARCODE'
        DataType = ftString
        ParamType = ptInput
        Size = 15
        Value = '6413300024722'
      end>
  end
  object FDConnection_KH24: TFDConnection
    Params.Strings = (
      'Database=C:\Servers\Kauppahalli24\HEARTWOOD.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=ISO8859_1'
      'ExtendedMetadata=True'
      'DriverID=FB')
    LoginPrompt = False
    Left = 504
    Top = 304
  end
  object FDQuery_Product_ByGTIN: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvFetchGeneratorsPoint, uvGeneratorName]
    UpdateOptions.FetchGeneratorsPoint = gpImmediate
    UpdateOptions.GeneratorName = 'GEN_PRODUCTS_ID'
    UpdateOptions.UpdateTableName = 'PRODUCTS'
    UpdateOptions.KeyFields = 'ID'
    UpdateOptions.AutoIncFields = 'ID'
    SQL.Strings = (
      'SELECT '
      
        '  ID, PRODUCTNAME, PRODUCTSTATE, PROVIDERID, VAT, BARCODE, STORA' +
        'GETYPE, LIFECYCLE, '
      '  PRODUCTCOLLECTINGANDCHARGE, SALEBATCHBARCODE, SALEBATCHSIZE, '
      '  PURCHASEPRICE, PRICE, '
      '  DELETED, CREATED, MODIFIED'
      'FROM PRODUCTS'
      'WHERE BARCODE = :paramBARCODE'
      '')
    Left = 312
    Top = 336
    ParamData = <
      item
        Name = 'PARAMBARCODE'
        DataType = ftString
        ParamType = ptInput
        Size = 15
        Value = '6413300024722'
      end>
  end
  object Timer_Events: TTimer
    Interval = 15000
    OnTimer = Timer_EventsTimer
    Left = 496
    Top = 48
  end
end
