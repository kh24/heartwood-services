unit mwTimedEvents;

interface

//{$DEFINE UseSmartInspect}


(* example for event ini file structure

[Events]
Seulo Deliverydetails=1

[Seulo Deliverydetails]
Weekdays=12345                    << required
Time=05:45                        << required
//rest of the parameters are specific for event content

Example for OnValidateEvent

function Tdm_ProcessEvent.ValidateEvent( Sender: TObject; const aEventName : string ) : boolean;
  procedure _ReportError( const aMessage : string );
  begin
    result := false;
    {$IFDEF UseSmartInspect}  SiMain.LogError( aMessage + ' ['+ aEventName +']' );{$ENDIF}
  end;

begin
  result := true;

  case RzRegIniFile.ReadInteger( aEventName, 'Action', 0 ) of
    ACTION_EmailWithPdfAttachement :
      begin
        if length( RzRegIniFile.ReadString( aEventName, 'AttachementFileName', '' )) < 5 then _ReportError( 'Attachement filename must be defined');
      end;
  end;
end;

Example for OnEvent

function Tdm_ProcessEvent.ProcessEvent( Sender: TObject; const aEventName : string ) : boolean;
var
  iEventFile : string;
  iEventFileLoaded : boolean;
  iEventSpecialProcessing : integer;
begin
  result := false;
  {$IFDEF UseSmartInspect}  SiMain.EnterMethod( 'ProcessEvent( '+ aEventName +' )');{$ENDIF}
  iEventFileLoaded := false;
  try
    iEventFile := RzRegIniFile.ReadString( aEventName, 'EventFile', '' );
    if length( iEventFile ) > 0 then
    begin
      iEventFileLoaded := LoadEventFile( iEventFile,
                                         RzRegIniFile.ReadString( aEventName, 'Parameter1', '' ),
                                         RzRegIniFile.ReadString( aEventName, 'Parameter2', '' ));
      if iEventFileLoaded then
      begin
        iEventSpecialProcessing := FEventIni.ReadInteger('SpecialProcessing','routine', 0 );
        if iEventSpecialProcessing > 0 then
          HandleEventSpecialProcessing( iEventSpecialProcessing, aEventName, iEventFile )
        else
        begin
          PrepareEventFile( aEventName, iEventFile );
          ExecuteEventFile( aEventName );
        end;

        result := true;
      end;
    end;

  finally
    if not result then
    begin
      {$IFDEF UseSmartInspect} SiMain.LogError( 'ProcessEvent failed!' );{$ENDIF}
      SendErrorEmail( 'ProcessEvent', 'ProcessEvent failed in event "'+ aEventName+'"' );
    end;

    if iEventFileLoaded then UnLoadEventFile;
    {$IFDEF UseSmartInspect}  SiMain.LeaveMethod( 'ProcessEvent( '+ aEventName +' )');{$ENDIF}
  end;
end;


*)

uses
  {$IFDEF UseSmartInspect} SiAuto, {$ENDIF}
  inifiles, windows,
  sysutils, DateUtils,
  classes, extctrls;

type
  TNotifyTimedEvent = function( Sender: TObject; const aEventName : string ) : boolean of object;

  TmwTimedEvents = class( TComponent )
  private
    FEventList : TStringList;
    FTimer: TTimer;
    FActive : boolean;
    FRebuildEventListDaily, FRebuildEventListHourly : boolean;
    FHourOfTheDay : word;
    FEventIniFile : string;

    FOnAddAdditionalEvents : TNotifyEvent;
    FOnValidateEvent, FOnEvent : TNotifyTimedEvent;
  protected
    procedure SetActive( const aNewValue : boolean );
    function GetTimerInterval : cardinal;
    procedure SetTimerInterval( const aNewValue : cardinal );

    function IsActiveOnThisWeek( const aWeekList : string ) : boolean;
    function IsActiveOnThisWeekDay( const aWeekDayList : string ) : boolean;
    function IsTimeAlreadyPassed( aTime : string ) : boolean;
    function IsDayToday( const aDateTimeString : string ) : boolean;
    function IsWithinActionTypeLimit( const aLimitActionTypeInto, aActionType : byte ) : boolean;

    procedure DoOnTimerTimer(Sender: TObject);
    function ValidateEventSettings( const aEventIni : TIniFile; const aEventName : string ) : boolean;
  public
    constructor Create( aOwner : TComponent ); override;
    destructor Destroy; override;

    function BuildEventList : boolean;
    function ExecuteEvent( const aEventName : string ) : boolean;
    procedure AddAdditionalEvent( const aEventTime : TDateTime; const aEventName : string );
    procedure StartUnstartedEvents( const aLimitActionTypeInto : byte = 0 );

    property EventList : TStringList read FEventList;
    property Active : boolean read FActive write SetActive;
    property EventIniFile : string read FEventIniFile write FEventIniFile;
    property TimerInterval : Cardinal read GetTimerInterval write SetTimerInterval;

    property RebuildEventListDaily : boolean read FRebuildEventListDaily write FRebuildEventListDaily;
    property RebuildEventListHourly : boolean read FRebuildEventListHourly write FRebuildEventListHourly;

    property OnAddAdditionalEvents : TNotifyEvent read FOnAddAdditionalEvents write FOnAddAdditionalEvents;
    property OnValidateEvent : TNotifyTimedEvent read FOnValidateEvent write FOnValidateEvent;
    property OnEvent : TNotifyTimedEvent read FOnEvent write FOnEvent;
  end;

implementation

procedure TmwTimedEvents.SetActive( const aNewValue : boolean );
begin
  if FActive <> aNewValue then
  begin
    if aNewValue then
    begin
      if not Assigned( FOnEvent ) then
      begin
        {$IFDEF UseSmartInspect} SiMain.LogError( 'OnEvent was not assigned' ); {$ENDIF}
        exit;
      end;

      if BuildEventList then
        FTimer.enabled := true
      else
        exit;

    end else
      FTimer.enabled := false;

    FActive := aNewValue;
  end;
end;(* GetActive *)

function TmwTimedEvents.GetTimerInterval : cardinal;
begin
  result := FTimer.Interval;
end;(* GetTimerInterval *)

procedure TmwTimedEvents.SetTimerInterval( const aNewValue : cardinal );
begin
  FTimer.Interval := aNewValue;
end;(* SetTimerInterval *)

function TmwTimedEvents.IsActiveOnThisWeek( const aWeekList : string ) : boolean;
(* if week is not defined, then assume that event is active every week *)
begin
  if length( aWeekList ) = 0 then
    result := true
  else
    result := Pos( IntToStr( WeekOfTheMonth( now )), aWeekList ) <> 0;
end;(* IsActiveOnThisWeek *)

function TmwTimedEvents.IsActiveOnThisWeekDay( const aWeekDayList : string ) : boolean;
begin
  result := Pos( IntToStr( DayOfTheWeek( now )), aWeekDayList ) <> 0;
end;(* IsActiveOnThisWeekDay *)

function TmwTimedEvents.IsTimeAlreadyPassed( aTime : string ) : boolean;
(* Improvement: Compare with current time with same precision.
      1:00    -> will be converted to 01:00 
     12:00    -> precision is minutes *)
var
  iCurrentTimeStr : string;
begin
  if length( aTime ) > 0 then
  begin
    (* make sure that parameter time is in correct format *)
    aTime := FormatDateTime( 'hh:mm', StrToTime( aTime ));
    (* make sure that current time is in correct format *)
    iCurrentTimeStr := FormatDateTime( 'hh:mm', Time );

//    SiMain.LogString( 'aTime', aTime );
//    SiMain.LogString( 'iCurrentTimeStr', iCurrentTimeStr );

    result := aTime <= iCurrentTimeStr;
  end else
    result := false;

  //old routine: result := StrToTime( aTime ) <= Time;
end;(* IsTimeAlreadyPassed *)

function TmwTimedEvents.IsDayToday( const aDateTimeString : string ) : boolean;
begin
  if length( aDateTimeString ) = 0 then
    result := false
  else
  begin
    {$IFDEF UseSmartInspect}
    SiMain.LogString( 'datetime', aDateTimeString );
    SiMain.LogDateTime( 'datetime', StrToDateTime( aDateTimeString ) );
    {$ENDIF}
    result := IsToday( StrToDateTime( aDateTimeString ));
  end;
end;(* IsDayToday *)

function TmwTimedEvents.IsWithinActionTypeLimit( const aLimitActionTypeInto, aActionType : byte ) : boolean;
begin
  if aLimitActionTypeInto = 0 then
    result := true
  else
    result := aLimitActionTypeInto = aActionType; 
end;(* IsWithinActionTypeLimit *)

procedure TmwTimedEvents.DoOnTimerTimer(Sender: TObject);
begin
  FTimer.enabled := false;
  try
    (* has first event time already passed? *)
    if FEventList.count > 0 then
      if IsTimeAlreadyPassed( FEventList.Names[ 0 ]) then
      begin
        try
          ExecuteEvent( FEventList.ValueFromIndex[ 0 ]);

        finally
          FEventList.Delete( 0 );
          {$IFDEF UseSmartInspect}  SiMain.LogStringList( 'Remaining events', FEventList );{$ENDIF}
        end;(* try *)
      end;

    (* rebuild eventlist if day has changed *)
    if FRebuildEventListDaily then
      if FTimer.Tag <> DayOfTheWeek( now ) then
      begin
        FTimer.Tag := DayOfTheWeek( now );
        BuildEventList;
      end;

    (* rebuild eventlist if hour has changed *)
    if FRebuildEventListHourly then
      if FHourOfTheDay <> HourOfTheDay( now ) then
      begin
        FHourOfTheDay := HourOfTheDay( now );
        BuildEventList;
      end;

  finally
    FTimer.enabled := true;
  end;(* try *)
end;(* DoOnTimerTimer *)

function TmwTimedEvents.BuildEventList : boolean;
var
  iEventIni : TIniFile;
  iEventSection : TStringList;
  ii : byte;
begin
  {$IFDEF UseSmartInspect} SiMain.EnterMethod( 'BuildEventList' ); {$ENDIF}
  try
    FEventList.Clear;
    result := true;

    iEventIni := TIniFile.create( FEventIniFile );
    iEventSection := TStringList.create;
    try
      try (* except *)
        iEventIni.ReadSection( 'Events', iEventSection );
        if iEventSection.Count > 0 then
          for ii := 0 to pred( iEventSection.Count ) do
          begin
            if not ValidateEventSettings( iEventIni, iEventSection[ ii ]) then result := false;

            (* event enabled? *)
            if iEventIni.ReadBool( 'Events', iEventSection[ ii ], false ) then
              (* active on this month? *)
              if IsActiveOnThisWeek( iEventIni.ReadString( iEventSection[ ii ], 'Weeks', '' )) then
                (* active on this weekday? *)
                if IsActiveOnThisWeekDay( iEventIni.ReadString( iEventSection[ ii ], 'Weekdays', '' )) then
                  (* time has not yet passed? *)
                  if not IsTimeAlreadyPassed( iEventIni.ReadString( iEventSection[ ii ], 'Time', '' )) then
                    (* add event to list *)
                    FEventList.Add( iEventIni.ReadString( iEventSection[ ii ], 'Time', '' )+'='+ iEventSection[ ii ] );
          end;(* for *)

      Except
        On E : Exception do
        begin
          {$IFDEF UseSmartInspect} SiMain.LogException( E, 'BuildEventList exception' ); {$ENDIF}
        end;
      end;(* try except *)

    finally
      iEventSection.Free;
      iEventIni.free;
    end;(* try *)

    if assigned( FOnAddAdditionalEvents ) then
      FOnAddAdditionalEvents( self );

    FEventList.Sort;

    { TODO 2 -ctodo : perhaps on...event when event list has been created }
    {$IFDEF UseSmartInspect}SiMain.LogStringList( 'BuildEventList', FEventList );{$ENDIF}

  finally
    {$IFDEF UseSmartInspect} SiMain.LeaveMethod( 'BuildEventList' ); {$ENDIF}
  end;
end;(* BuildEventList *)

function TmwTimedEvents.ValidateEventSettings( const aEventIni : TIniFile; const aEventName : string ) : boolean;
  procedure _ReportError( const aMessage : string );
  begin
    result := false;
    {$IFDEF UseSmartInspect}  SiMain.LogError( aMessage + ' ['+ aEventName +']' );{$ENDIF}
  end;(* _ReportError *)

begin
  result := true;
  if length( aEventIni.ReadString( aEventName, 'Time', '' )) <> 5 then _ReportError( 'Time must be defined with five digits');

  if assigned( FOnValidateEvent ) then
    if not FOnValidateEvent( self, aEventName ) then result := false;
end;(* ValidateEventSettings *)

constructor TmwTimedEvents.create( aOwner : TComponent );
begin
  inherited create( aOwner );

  FActive := false;

  FRebuildEventListDaily := false;
  FRebuildEventListHourly := false;
  FHourOfTheDay := HourOfTheDay( now );

  FEventList := TStringList.create;

  FTimer := TTimer.create( self );
  FTimer.Tag := DayOfTheWeek( now );
  FTimer.Interval := 60000;
  FTimer.Enabled := false;
  FTimer.OnTimer := DoOnTimerTimer;
end;(* create *)

destructor TmwTimedEvents.destroy;
begin
  if Active then Active := false;

  FEventList.free;
  FTimer.free;

  inherited destroy;
end;(* destroy *)

function TmwTimedEvents.ExecuteEvent( const aEventName : string ) : boolean;
var
  iEventIni : TIniFile;
  iStartTime : TDateTime;
begin
  result := false;
  if Assigned( FOnEvent ) then
  begin
    iEventIni := TIniFile.create( FEventIniFile );
    try
      iStartTime := now;
      iEventIni.WriteDateTime( aEventName, 'LastStarted', iStartTime );

      result := FOnEvent( Self, aEventName );
      if not result then
      begin
        {$IFDEF UseSmartInspect} SiMain.LogError('Error loading event "'+aEventName+'"');{$ENDIF}
      end;

      iEventIni.WriteDateTime( aEventName, 'LastStopped', Now );
      iEventIni.WriteInteger( aEventName, 'LastDuration', SecondsBetween( now, iStartTime ));
      {$IFDEF UseSmartInspect} SiMain.LogInt64( 'Event duration (seconds)', SecondsBetween( now, iStartTime )); {$ENDIF}

    finally
      iEventIni.Free;
    end;(* try *)
    
  end else
  begin
    {$IFDEF UseSmartInspect} SiMain.LogError( 'TmwTimedEvents.OnEvent was not assigned!' ); {$ENDIF}
  end;
end;(* ExecuteEvent *)

procedure TmwTimedEvents.AddAdditionalEvent( const aEventTime : TDateTime; const aEventName : string );
begin
  { TODO 1 -ctodo : define regional settings => time separator }
  FEventList.Add( FormatDateTime( 'hh:mm', aEventTime ) +'='+ aEventName );

  {$IFDEF UseSmartInspect} SiMain.LogDebug( 'AddAdditionalEvent '+ FormatDateTime( 'hh:mm', aEventTime ) +'='+ aEventName );{$ENDIF}
end;(* AddAdditionalEvent *)

procedure TmwTimedEvents.StartUnstartedEvents( const aLimitActionTypeInto : byte = 0 );
(* This routine starts events, that should have started earlier today, but haven't. Usefull when service has crashed and restarted  
   if aLimitActionTypeInto is <> 0, only events with defined action types are started *)
var
  iEventIni : TIniFile;
  iEventSection : TStringList;
  ii : byte;
begin
  {$IFDEF UseSmartInspect} SiMain.EnterMethod( 'StartUnstartedEvents' ); {$ENDIF}
  iEventIni := TIniFile.create( FEventIniFile );
  iEventSection := TStringList.create;
  try
    iEventIni.ReadSection( 'Events', iEventSection );
    if iEventSection.Count > 0 then
      for ii := 0 to pred( iEventSection.Count ) do
      begin
        (* event enabled? *)
        if iEventIni.ReadBool( 'Events', iEventSection[ ii ], false ) then
          (* active on this month? *)
          if IsActiveOnThisWeek( iEventIni.ReadString( iEventSection[ ii ], 'Weeks', '' )) then
            (* active on this weekday? *)
            if IsActiveOnThisWeekDay( iEventIni.ReadString( iEventSection[ ii ], 'Weekdays', '' )) then
              (* Correct action type? *)
              if IsWithinActionTypeLimit( aLimitActionTypeInto, iEventIni.ReadInteger( iEventSection[ ii ], 'Action', 0 )) then
              (* start time has already passed? *)
              if IsTimeAlreadyPassed( iEventIni.ReadString( iEventSection[ ii ], 'Time', '' )) then
                (* end time not yet passed? *)
                if not IsTimeAlreadyPassed( iEventIni.ReadString( iEventSection[ ii ], 'EndTime', '' )) then
                begin
                  (* event was not started today *)
                  if not IsDayToday( iEventIni.ReadString( iEventSection[ ii ], 'LastStarted', '' )) then
                  begin
                    {$IFDEF UseSmartInspect} SiMain.LogDebug( 'Starting unstarted event. Last run '+ iEventIni.ReadString( iEventSection[ ii ], 'LastStarted', '' )); {$ENDIF}
                    ExecuteEvent( iEventSection[ ii ]);
                  end else
                    {$IFDEF UseSmartInspect} SiMain.LogDebug( 'Ignoring "'+ iEventSection[ ii ] +'" event. Last run '+ iEventIni.ReadString( iEventSection[ ii ], 'LastStarted', '' )); {$ENDIF}
                end else
                  {$IFDEF UseSmartInspect} SiMain.LogDebug( 'Ignoring "'+ iEventSection[ ii ] +'" event. Endtime has passed' ); {$ENDIF}
      end;(* for *)

  finally
    iEventSection.Free;
    iEventIni.free;
    {$IFDEF UseSmartInspect} SiMain.LeaveMethod( 'StartUnstartedEvents' ); {$ENDIF}
  end;
end;(* StartUnstartedEvents *)

end.
