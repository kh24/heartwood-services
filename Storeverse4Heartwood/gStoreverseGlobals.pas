unit gStoreverseGlobals;

interface

const
  (* INI values *******************************************)
  gIniFileName = 'HeardWood4Storeverse.ini';
  gSectionSettings = 'Settings';
  gHeartwoodDBFullFileName = 'HeartwoodDBFullFileName';
  gHeartwoodSourceDBFullFileName = 'HeartwoodSourceDBFullFileName';
  gStoreverseAPIUrl = 'StoreverseAPIUrl';
  gStoreverseAPIToken = 'StoreverseAPIToken';
  gStoreverseUsername = 'StoreverseUsername';
  gStoreversePassword = 'StoreversePassword';
  gStoreverseAPITrustServerCertificate = 'TrustServerCertificate';

  gSectionProcess = 'Process';
  gLastGetOrdersDateTime = 'LastGetOrdersDateTime';

  gLogStartupRoutines = 'StartupRoutines.log';
  gLogGetModifiedOrders = 'GetModifiedOrders.log';
  gLogGetModifiedProducts = 'GetModifiedProducts.log';

  (* ORDER related fieldnames *****************************)
  Storeverse_Order_created_at = 'created_at';
  Storeverse_Order_updated_at = 'updated_at';

  Storeverse_Order_OrderCode = 'O000';
  Storeverse_Order_OrderID = 'O001';
  Storeverse_Order_UserGuidCode = 'O002';
  Storeverse_Order_OrderTimestamp	= 'O003';
  Storeverse_Order_StorefrontID = 'O005';
  Storeverse_Order_MerchantID = 'O006';
  Storeverse_Order_OrderStatus = 'O007'; //Not paid (default) > New order > Processing > Delivered / Failed
    // New order just in
    Storeverse_Order_OrderStatus_PaymentReceived = 901;
    Storeverse_Order_OrderStatus_OrderConfirmationSent = 903;
    // Order can be edited
    Storeverse_Order_OrderStatus_NewOrder = 902;
    Storeverse_Order_OrderStatus_DeliveryInQuenue = 904;
    // Order in processing (locked in collico)
    Storeverse_Order_OrderStatus_DeliveryInProcessing = 905;
    // Order completed
    Storeverse_Order_OrderStatus_DeliveryDispatched = 906;
    Storeverse_Order_OrderStatus_OrderReimbursed = 908;
    Storeverse_Order_OrderStatus_DeliveryConfirmationSent = 907;
    Storeverse_Order_OrderStatus_OrderCompleted = 909; //can be reclamated
    // Order not paid
    Storeverse_Order_OrderStatus_PaymentFailed = 910;
    // Order not delivered
    Storeverse_Order_OrderStatus_OrderCancelled = 911;
    Storeverse_Order_OrderStatus_DeliveryFailed = 912;

  Storeverse_Order_DeliveryMethodCode = 'O010';
  Storeverse_Order_DeliveryMethodName = 'O011';

  Storeverse_Order_ExpectedDeliveryDate = 'O012'; //date is retrieved from O013
  Storeverse_Order_ExpectedDeliveryTimeStart = 'O013';
  Storeverse_Order_ExpectedDeliveryTimeEnd = 'O014';

  Storeverse_Order_CustomerFirstName = 'O020';
  Storeverse_Order_CustomerLastName = 'O021';
  Storeverse_Order_CustomerEmail = 'O022';
  Storeverse_Order_CustomerPhone = 'O023';
  Storeverse_Order_CustomerAddressStreet = 'O024';
  Storeverse_Order_CustomerAddressCity = 'O025';
  Storeverse_Order_CustomerAddressPostalCode = 'O026';

  Storeverse_Order_DeliveryFirstName = 'O030';
  Storeverse_Order_DeliveryLastName = 'O031';
  Storeverse_Order_DeliveryEmail = 'O032';
  Storeverse_Order_DeliveryPhone = 'O033';
  Storeverse_Order_DeliveryAddressStreet = 'O034';
  Storeverse_Order_DeliveryAddressCity = 'O035';
  Storeverse_Order_DeliveryAddressPostalCode = 'O036';

  Storeverse_Order_DeliveryCost = 'O071';
  Storeverse_Order_OrderTotal = 'O072';
  Storeverse_Order_DiscountAtCheckout = 'O084';

  (* ORDER ROW related fieldnames **************************)
  Storeverse_OrderRow_RowID = 'R000'; //Order Row ID	R000
  Storeverse_OrderRow_Amount = 'R020'; //Amount	R020
  Storeverse_OrderRow_ActualSalesPrice = 'R073'; //Actual Sales Price R073
  Storeverse_OrderRow_PurchasePrice = 'R070'; //Purchase Price	R070

  (* PRODUCT related fieldnames ****************************)
  Storeverse_Product_StorageTemperature = 'A006'; //"warm"
  Storeverse_Product_DateTracking = 'A008'; //"true" / "false"
  Storeverse_Product_ShelLife = 'A009';
  Storeverse_Product_FreshnessPromise = 'A010'; // 60
  Storeverse_Product_Frozen = 'A011'; // "true" / "false"' +

  Storeverse_Product_ProductID = 'C000'; // "a29f309c-0e10-4df4-8836-3f4fdfad3ba6"
  Storeverse_Product_ProductCode = 'C001'; //2063
  Storeverse_Product_ProductType = 'C002'; // "PACK"
  Storeverse_Product_IsPublic = 'C003'; // boolean
  Storeverse_Product_ProductGroup = 'C004'; //1190200
  Storeverse_Product_InternalName = 'C005'; //"ELOMENTS ORGANIC VITAMIN TEA DOUBLE LEMON LUOMUTEE 14PSS/ 28G"
  Storeverse_Product_HasVariableWeight = 'C007'; // boolean

  Storeverse_Product_DisplayGroup = 'D001'; //190420
  Storeverse_Product_InAssortment = 'D002'; // boolean

  Storeverse_Product_ProductPicture = 'F000'; //"https://www.kauppahalli24.fi/pub/media/catalog/product/cache/a7fa59de44ae400e652bf7c13548bd46/9/3/9352177000026.jpg"

  Storeverse_Product_PrimaryLabel = 'L005'; //"Eloments Organic Vitamin tea"
  Storeverse_Product_SecondaryLabel = 'L006'; //"Double Lemontee luomu"
  Storeverse_Product_InternalComment = 'L900';

  Storeverse_Product_CampaignPrice = 'M002';
  Storeverse_Product_HighlightedProduct = 'M003'; // false

  Storeverse_Product_PurchasePrice = 'P000'; //"3,37"
  Storeverse_Product_TargetMargin = 'P001'; //31
  Storeverse_Product_SuggestedPrice = 'P002'; //"4,88"
  Storeverse_Product_SalesPrice = 'P003'; //"5,59"
  Storeverse_Product_SalesTax = 'P005'; //14
  Storeverse_Product_ReferenceUnit = 'P006'; //"kg"

  Storeverse_Product_Supplier = 'S002'; //Suppliers code : 193
  Storeverse_Product_DeliveryGTIN = 'S004'; //"9352177000026"
  Storeverse_Product_SupplyGTIN = 'S005'; //19352177000023
  Storeverse_Product_SupplyUnits = 'S006'; // 4
  Storeverse_Product_WarehouseStock = 'S007'; // 11
  Storeverse_Product_NextSupplyTime = 'S008'; //""
  Storeverse_Product_StockIsLimited = 'S009'; //true
  Storeverse_Product_ReferenceKey = 'S900'; //"EL0005"
  Storeverse_Product_ReorderAlertLevel = 'S901'; // 2
  Storeverse_Product_ReorderQuantity = 'S902'; // 1
  Storeverse_Product_SupplierReference = 'S903'; // 194

  Storeverse_Product_SalesUnits = 'U001'; //14
  Storeverse_Product_UnitTitle = 'U002'; //"pkt"
  Storeverse_Product_GrossVolume = 'U003'; //""
  Storeverse_Product_NetVolume = 'U004'; //2
  Storeverse_Product_VolumeTitle = 'U005'; //"g"
  Storeverse_Product_MaxDeviation = 'U006'; // ""
  Storeverse_Product_VolumeConversion = 'U007'; // 1000
  Storeverse_Product_DeliveryWidth = 'U008'; // 68
  Storeverse_Product_DeliveryHeight = 'U009'; // 78
  Storeverse_Product_DeliveryDepth = 'U010'; // 118
  Storeverse_Product_DeliveryWeight = 'U011'; // "0,028"
  Storeverse_Product_MinimumStorageTemperature = 'U015'; // 10
  Storeverse_Product_MaximumStorageTemperature = 'U016'; // 25

implementation

end.
