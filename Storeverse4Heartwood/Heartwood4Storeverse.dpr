program Heartwood4Storeverse;

uses
  Vcl.Forms,
  wMainWindow in 'wMainWindow.pas' {window_Server},
  mwTimedEvents in 'MecwareCommonCode\mwTimedEvents.pas',
  gFieldNames in 'HeartwoodCode\gFieldNames.pas',
  gStoreverseGlobals in 'gStoreverseGlobals.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Twindow_Server, window_Server);
  Application.Run;
end.
