object window_DiffCore: Twindow_DiffCore
  Left = 0
  Top = 0
  Caption = 'Storeverse CORE diff'
  ClientHeight = 320
  ClientWidth = 531
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    531
    320)
  PixelsPerInch = 96
  TextHeight = 13
  object Button_DiffOrders: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Diff orders'
    TabOrder = 0
    OnClick = Button_DiffOrdersClick
  end
  object Memo: TMemo
    Left = 8
    Top = 39
    Width = 515
    Height = 273
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo')
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object Checbox_CallOnlyProduction: TCheckBox
    Left = 89
    Top = 16
    Width = 152
    Height = 17
    Caption = 'Call only production core'
    TabOrder = 2
  end
  object CheckBox_IgnoreComparison: TCheckBox
    Left = 281
    Top = 16
    Width = 152
    Height = 17
    Caption = 'Ignore comparison'
    TabOrder = 3
  end
end
