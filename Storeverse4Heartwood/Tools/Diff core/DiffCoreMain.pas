unit DiffCoreMain;

interface

uses
  SmartInspect.VCL,SmartInspect.VCL.SiAuto,
  schttp, system.json,
  System.DateUtils, 
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TCoreEnviroments = ( ceStaging, ceProduction );
  Twindow_DiffCore = class(TForm)
    Button_DiffOrders: TButton;
    Memo: TMemo;
    Checbox_CallOnlyProduction: TCheckBox;
    CheckBox_IgnoreComparison: TCheckBox;
    procedure Button_DiffOrdersClick(Sender: TObject);
  private
    function GetServiceUrl( aCoreEnviroment : TCoreEnviroments ) : string;
    function GetAPIToken( aCoreEnviroment : TCoreEnviroments ) : string;
    function GetFileName( aCoreEnviroment : TCoreEnviroments ) : string;

    function ConvertDateToStoreverseQueryDate( const aDate : TDate ) : string;
    procedure CompareOrdersArray( const aJSON_production, aJSON_Staging : TJSONArray );
    procedure CompareProductsArray( const aJSON_production, aJSON_Staging : TJSONArray );
    
    procedure CompareJSONObjectsOriginal(const AJSON1, AJSON2: TJSONObject; const APath: string; var ADiffList: TStringList);
    procedure CompareJSONArrays(const AJSON1, AJSON2: TJSONArray);
    procedure CompareJSONObjects(const AJSON1, AJSON2: TJSONObject);
  public
    { Public declarations }
  end;

var
  window_DiffCore: Twindow_DiffCore;

implementation

{$R *.dfm}

function Twindow_DiffCore.GetServiceUrl( aCoreEnviroment : TCoreEnviroments ) : string;
begin
  case aCoreEnviroment of
    ceStaging : 
      if Checbox_CallOnlyProduction.Checked then
        result := 'https://core.lafka.tools/api/services/'
      else
        result := 'https://testing-core.lafka.tools/api/services/';
        
    ceProduction : result := 'https://core.lafka.tools/api/services/';
  end;
end;(* GetServiceUrl *)

function Twindow_DiffCore.GetAPIToken( aCoreEnviroment : TCoreEnviroments ) : string;
begin
  result := 'SWw3a63A6phc9291SgUJTUcmaBaUcKjgkOAeKNGfztIMj7NNFsjQqBfKubwdkf';
end;(* GetAPIToken *)

function Twindow_DiffCore.GetFileName( aCoreEnviroment : TCoreEnviroments ) : string;
begin
  case aCoreEnviroment of
    ceStaging : result := 'staging';
    ceProduction : result := 'production';
  end;  

  result := IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + result;
end;(* GetFileName *)

function Twindow_DiffCore.ConvertDateToStoreverseQueryDate( const aDate : TDate ) : string;
var
  iFormatSettings : TFormatSettings;
begin
  iFormatSettings.Create( 'fi-FI' );
  try
    iFormatSettings.DateSeparator := '-';
    result := FormatDateTime( 'yyyy/mm/dd', aDate, iFormatSettings );
  finally
    FreeAndNil( iFormatSettings );
  end;
end;(* ConvertDateToStoreverseQueryDate *)

procedure Twindow_DiffCore.CompareOrdersArray( const aJSON_production, aJSON_Staging : TJSONArray );
var
  iJSONOrderID : array[ TCoreEnviroments ] of TJSONValue;
  iJSONProperties : array[ TCoreEnviroments ] of TJSONObject;  
  iOrderCounter_Production, iOrderCounter_Staging : word;
  iDiffList : TStringList;
begin
  iDiffList := TStringList.create;
  try  
    (* scroll thrue every order *****************************)
    for iOrderCounter_Production := 0 to pred( aJSON_production.Count ) do
    begin
      (* Find production ID number *)
      iJSONOrderID[ ceProduction ] := aJSON_production.items[ iOrderCounter_Production ].FindValue('id') as TJSONValue;  
      if iJSONOrderID[ ceProduction ] <> nil then
      begin
        Memo.Lines.Add( 'Verifying ID: '+ iJSONOrderID[ ceProduction ].Value );
        iJSONProperties[ ceProduction ] := aJSON_production.items[ iOrderCounter_Production ].FindValue('properties') as TJSONObject;  

        (* find same order from staging ************************)
        for iOrderCounter_Staging := 0 to pred( aJSON_Staging.Count ) do
        begin
          (* Find production ID number *)
          iJSONOrderID[ ceStaging ] := aJSON_Staging.items[ iOrderCounter_Staging ].FindValue('id') as TJSONValue;  
          if iJSONOrderID[ ceStaging ] <> nil then
          begin
            Memo.Lines.Add( 'Comparing to ID: '+ iJSONOrderID[ ceStaging ].Value );
            iJSONProperties[ ceStaging ] := aJSON_Staging.items[ iOrderCounter_Staging ].FindValue('properties') as TJSONObject;  
            CompareJSONObjectsOriginal( iJSONProperties[ ceProduction ], 
                                        iJSONProperties[ ceStaging ], 
                                        '', iDiffList );

            Memo.Lines.Add( '' );
            SiMain.LogStringList( 'JSON iDiffList', iDiffList );
            for var ii := 0 to iDiffList.Count - 1 do
              Memo.Lines.Add( '  DIFF:' + iDiffList[ii]);            
          end;
        end;(* for staging *)
      end;
    end;(* for production *)
  
  finally
    iDiffList.Free;
  end;
end;(* CompareOrdersJSON *)

procedure Twindow_DiffCore.CompareProductsArray( const aJSON_production, aJSON_Staging : TJSONArray );
var
  iJSONProductID : array[ TCoreEnviroments ] of TJSONValue;
  iJSONProperties : array[ TCoreEnviroments ] of TJSONObject;  
  iProductCounter_Production, iProductCounter_Staging : word;
  iDiffList : TStringList;
begin
  iDiffList := TStringList.create;
  try  
    (* scroll thrue every product *****************************)
    for iProductCounter_Production := 0 to pred( aJSON_production.Count ) do
    begin
      (* Find production product_ID number *)
      iJSONProductID[ ceProduction ] := aJSON_production.items[ iProductCounter_Production ].FindValue('product_id') as TJSONValue;  
      if iJSONProductID[ ceProduction ] <> nil then
      begin
        Memo.Lines.Add( 'Verifying ID: '+ iJSONProductID[ ceProduction ].Value );
        iJSONProperties[ ceProduction ] := aJSON_production.items[ iProductCounter_Production ].FindValue('') as TJSONObject;  

        (* find same order from staging ************************)
        for iProductCounter_Staging := 0 to pred( aJSON_Staging.Count ) do
        begin
          (* Find production ID number *)
          iJSONProductID[ ceStaging ] := aJSON_Staging.items[ iProductCounter_Staging ].FindValue('product_id') as TJSONValue;  
          if iJSONProductID[ ceStaging ] <> nil then
          begin
            Memo.Lines.Add( 'Comparing to ID: '+ iJSONProductID[ ceStaging ].Value );
            iJSONProperties[ ceStaging ] := aJSON_Staging.items[ iProductCounter_Staging ].FindValue('') as TJSONObject;  
            CompareJSONObjectsOriginal( iJSONProperties[ ceProduction ], 
                                        iJSONProperties[ ceStaging ], 
                                        '', iDiffList );

            SiMain.LogStringList( 'JSON iDiffList', iDiffList );
            for var ii := 0 to iDiffList.Count - 1 do
              Memo.Lines.Add( '  -' + iDiffList[ii]);            
          end;
        end;(* for staging *)
      end;
    end;(* for production *)
  
  finally
    iDiffList.Free;
  end;
end;(* CompareProductsArray *)

procedure Twindow_DiffCore.CompareJSONObjectsOriginal(const AJSON1, AJSON2: TJSONObject; const APath: string; var ADiffList: TStringList);
var
  i: Integer;
  v1, v2: TJSONValue;
begin
  Memo.Lines.Add( 'CompareJSONObjectsOriginal: '+ aPath );
  // compare the keys in the two JSON objects
  for i := 0 to AJSON1.Count - 1 do
  begin
    v1 := AJSON1.Pairs[i].JsonValue;
    v2 := AJSON2.GetValue(AJSON1.Pairs[i].JsonString.Value);

    if (v1 is TJSONArray) and (v2 is TJSONArray) then
    begin
      Memo.Lines.Add( '      --> here will be products array' )
      //CompareProductsArray( v1 as TJSONArray, v2 as TJSONArray );              
    end else
    
      if (v1 is TJSONObject) and (v2 is TJSONObject) then
      begin
        Memo.Lines.Add( '      --> here should be child structure comparison ' + APath + '.' + AJSON1.Pairs[i].JsonString.Value );
        // recursively compare the two child objects
        CompareJSONObjectsOriginal(TJSONObject(v1), TJSONObject(v2), APath + '.' + AJSON1.Pairs[i].JsonString.Value, ADiffList);
      end else 
      
        if (v1 is TJSONValue) and (v2 is TJSONValue) then
        begin
          Memo.Lines.Add( '    Verifying: '+ v1.ToString + ' ('+ AJSON1.Pairs[i].JsonString.Value +')' );

          if not Assigned(v2) then
          begin
            // key is missing in second object
            ADiffList.Add(APath + '.' + AJSON1.Pairs[i].JsonString.Value + ': missing');
          end
          else if v1.ToString <> v2.ToString then
          begin
            // key has different value in the two objects
            ADiffList.Add(APath + '.' + AJSON1.Pairs[i].JsonString.Value + ': ' + v1.ToString + ' <> ' + v2.ToString);
          end;
        end else 
          if (v1 is TJSONArray) and (v2 is TJSONArray) then
          begin
            Memo.Lines.Add( '      -- skipping array' )
          end;
  end;(* for i *)

  // check for keys in the second object that are missing in the first object
  for i := 0 to AJSON2.Count - 1 do
  begin
    if not Assigned(AJSON1.GetValue(AJSON2.Pairs[i].JsonString.Value)) then
    begin
      ADiffList.Add(APath + '.' + AJSON2.Pairs[i].JsonString.Value + ': missing');
    end;
  end;
end;

procedure Twindow_DiffCore.CompareJSONArrays(const AJSON1, AJSON2: TJSONArray);
var
  i, j: Integer;
  found: Boolean;
begin
  // Compare the first array to the second array
  for i := 0 to AJSON1.Count - 1 do
  begin
    found := False;
    for j := 0 to AJSON2.Count - 1 do
    begin
      if AJSON1.Items[i].Equals(AJSON2.Items[j]) then
      begin
        found := True;
        Break;
      end
      else if (AJSON1.Items[i] is TJSONArray) and (AJSON2.Items[j] is TJSONArray) then
      begin
        // Recursively compare nested JSON arrays
        CompareJSONArrays(TJSONArray(AJSON1.Items[i]), TJSONArray(AJSON2.Items[j]));
        found := True;
        Break;
      end
      else if (AJSON1.Items[i] is TJSONObject) and (AJSON2.Items[j] is TJSONObject) then
      begin
        // Recursively compare nested JSON objects
        CompareJSONObjects(TJSONObject(AJSON1.Items[i]), TJSONObject(AJSON2.Items[j]));
        found := True;
        Break;
      end;
    end;
    if not found then
    begin
      Memo.Lines.add('Array 1 contains "' + AJSON1.Items[i].Value + '", but Array 2 does not.');
    end;
  end;

  // Compare the second array to the first array
  for i := 0 to AJSON2.Count - 1 do
  begin
    found := False;
    for j := 0 to AJSON1.Count - 1 do
    begin
      if AJSON2.Items[i].Equals(AJSON1.Items[j]) then
      begin
        found := True;
        Break;
      end
      else if (AJSON2.Items[i] is TJSONArray) and (AJSON1.Items[j] is TJSONArray) then
      begin
        // Recursively compare nested JSON arrays
        CompareJSONArrays(TJSONArray(AJSON2.Items[i]), TJSONArray(AJSON1.Items[j]));
        found := True;
        Break;
      end
      else if (AJSON2.Items[i] is TJSONObject) and (AJSON1.Items[j] is TJSONObject) then
      begin
        // Recursively compare nested JSON objects
        CompareJSONObjects(TJSONObject(AJSON2.Items[i]), TJSONObject(AJSON1.Items[j]));
        found := True;
        Break;
      end;
    end;
    if not found then
    begin
      Memo.Lines.add('Array 2 contains "' + AJSON2.Items[i].Value + '", but Array 1 does not.');
    end;
  end;
end;

procedure Twindow_DiffCore.CompareJSONObjects(const AJSON1, AJSON2: TJSONObject);
var
  i, j: Integer;
  pair1, pair2: TJSONPair;
  found: Boolean;
begin
  // Compare the first object to the second object
  for i := 0 to AJSON1.Count - 1 do
  begin
    pair1 := AJSON1.Pairs[i];
    found := False;
    for j := 0 to AJSON2.Count - 1 do
    begin
      pair2 := AJSON2.Pairs[j];
      if pair1.JsonString.Value = pair2.JsonString.Value then
      begin
        if pair1.JsonValue.Equals(pair2.JsonValue) then
        begin
          found := True;
        end
        else if (pair1.JsonValue is TJSONArray) and (pair2.JsonValue is TJSONArray) then
        begin
          // Recursively compare nested JSON arrays
          CompareJSONArrays(TJSONArray(pair1.JsonValue), TJSONArray(pair2.JsonValue));
          found := True;
        end
        else if (pair1.JsonValue is TJSONObject) and (pair2.JsonValue is TJSONObject) then
        begin
          // Recursively compare nested JSON objects
          CompareJSONObjects(TJSONObject(pair1.JsonValue), TJSONObject(pair2.JsonValue));
          found := True;
        end;
        Break;
      end;
    end;
    if not found then
    begin
      Memo.Lines.add('Object 1 contains "' + pair1.JsonString.Value + '", but Object 2 does not.');
    end;
  end;

  // Compare the second object to the first object
  for i := 0 to AJSON2.Count - 1 do
  begin
    pair2 := AJSON2.Pairs[i];
    found := False;
    for j := 0 to AJSON1.Count - 1 do
    begin
      pair1 := AJSON1.Pairs[j];
      if pair2.JsonString.Value = pair1.JsonString.Value then
      begin
        found := True;
        Break;
      end;
    end;
    if not found then
    begin
      Memo.Lines.add('Object 2 contains "' + pair2.JsonString.Value + '", but Object 1 does not.');
    end;
  end;
end;

procedure Twindow_DiffCore.Button_DiffOrdersClick(Sender: TObject);
var
  iRequest : array[ TCoreEnviroments ] of TScHttpWebRequest;
  iResponse : array[ TCoreEnviroments ] of TScHttpWebResponse;
  iJSONOrder : array[ TCoreEnviroments ] of TJSONArray;
  iModifiedSince : TDate;
  iEnviroment : TCoreEnviroments;
  iRequestCall, iResponseStr : string;
  iFileStorage : TStringList;
  iOrderCounter : word;
begin
  SiMain.EnterMethod(Self, 'Button_DiffOrdersClick');
  try
    iModifiedSince := EncodeDate( 2023, 03, 04 ); //IncDay( Today, -20 );

    iFileStorage := TStringList.create;
    iJSONOrder[ ceStaging ] := nil;
    iJSONOrder[ ceProduction ] := nil;
    try
    
      for iEnviroment := low( TCoreEnviroments ) to high( TCoreEnviroments ) do
      begin
        SiMain.EnterMethod( GetServiceUrl( iEnviroment ));
        Memo.Lines.Add( GetServiceUrl( iEnviroment ));

        iRequestCall := GetServiceUrl( iEnviroment ) +
                                      'heartwood/order?'+
                                      'X-Token=' + GetAPIToken( iEnviroment ) +
                                      '&filter=status=1'+ // Only paid orders
                                      '&between=updated_at='+ ConvertDateToStoreverseQueryDate( iModifiedSince ) +',2099-01-01';
        Memo.Lines.Add( 'Request: ' + iRequestCall );                              
        
        iRequest[ iEnviroment ] := TScHttpWebRequest.Create( iRequestCall );
        iRequest[ iEnviroment ].SSLOptions.TrustServerCertificate := true;
        iRequest[ iEnviroment ].Credentials.UserName := 'janne.timmerbacka@kauppahalli24.fi.biz';
        iRequest[ iEnviroment ].Credentials.Password := '_!S4f_Fr0NN';

        try
          SiMain.LogDebug( 'GetResponse' );
          Memo.Lines.Add( '  GetResponse at ' + TimeToStr( now ));
          iResponse[ iEnviroment ] := iRequest[ iEnviroment ].GetResponse;
          Memo.Lines.Add( '  GetResponsed at ' + TimeToStr( now ));
          iResponseStr := iResponse[ iEnviroment ].ReadAsString;

          iFileStorage.Clear;
          iFileStorage.text := iResponseStr;
          iFileStorage.SaveToFile( GetFileName( iEnviroment )+'-orders.json' );
          
          Memo.Lines.Add( '  Length ' + inttostr( length( iResponseStr )));
          if Length( iResponseStr ) > 2 then
          begin
            SiMain.LogDebug( 'Parsing' );
            Memo.Lines.Add( '  Parsing');
            //iJSONOrder[ iEnviroment ] := TJSONObject.ParseJSONValue( iResponseStr ) as TJSONObject;
            iJSONOrder[ iEnviroment ] := TJSONObject.ParseJSONValue( iResponseStr ) as TJSONArray;
            
            Memo.Lines.Add( '  Parsed');
          end;

        finally
          iResponse[ iEnviroment ].Free;
          SiMain.LeaveMethod( GetServiceUrl( iEnviroment ));
        end;

        iRequest[ iEnviroment ].disconnect;
        iRequest[ iEnviroment ].free;
      end;

      Memo.Lines.Add( '' );
      if not CheckBox_IgnoreComparison.Checked then
        if ( iJSONOrder[ ceStaging ] <> nil ) and
           ( iJSONOrder[ ceProduction ] <> nil ) then
        begin
          SiMain.LogDebug( 'Compare' );
          Memo.Lines.Add( 'Compare' );

          CompareOrdersArray( iJSONOrder[ ceProduction ], iJSONOrder[ ceStaging ] );
          Memo.Lines.Add( 'Compared' );
        end; 
      
    except
      On E:Exception do
      begin
        Memo.Lines.Add( '    !! EXCEPTION at ' + TimeToStr( now ) + E.Message );
        SiMain.LogException( E, 'EXCEPTION' );
      end;
    end;

  finally
    iFileStorage.Free;
    SiMain.LeaveMethod(Self, 'Button_DiffOrdersClick');
  end;(* try *)
end;

end.
