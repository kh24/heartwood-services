unit wMainWindow;

interface

uses
  (* mecware - heartwood specific *)
  mwTimedEvents,
  (* rest api *)
  schttp, system.json,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.IniFiles,
  Vcl.ExtCtrls;

type
  Twindow_Server = class(TForm)
    FDConnection: TFDConnection;
    FDQuery_Products: TFDQuery;
    FDQuery_UserIDandUniqueEntityID_ByExternalID2: TFDQuery;
    FDQuery_UniqueEntities_Only10Rows: TFDQuery;
    FDQuery_Users_Only10Rows: TFDQuery;
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers: TFDQuery;
    FDQuery_Payments_ByExternalID: TFDQuery;
    FDQuery_Orders_ByPaymentID: TFDQuery;
    FDQuery_OrderRows_ByOrdersPaymentID: TFDQuery;
    btn_GetModifiedOrders: TButton;
    FDQuery_KH24_Product_ByGTIN: TFDQuery;
    FDConnection_KH24: TFDConnection;
    Memo_Log: TMemo;
    btn_GetModifiedProducts: TButton;
    FDQuery_Product_ByGTIN: TFDQuery;
    Label1: TLabel;
    Timer_Events: TTimer;
    Memo_TimedLog: TMemo;
    Label_InstanceInfo: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure btn_TestAutoincClick(Sender: TObject);
    procedure btn_GetModifiedOrdersClick(Sender: TObject);
    procedure btn_GetModifiedProductsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer_EventsTimer(Sender: TObject);
  private
    FTimedEvents : TmwTimedEvents; //remove this
    FIniFile : TIniFile;

    FTimer_Products, FTimer_OrdersPreFetch, FTimer_OrdersFetch : TDate;

    function SafeJSONString( aJSONObject : TJSONObject; const aNamePath : string ) : string;
    function SafeAsInteger( aJSONObject : TJSONObject; const aNamePath : string ) : integer;
    function SafeAsFloatDef( aJSONObject : TJSONObject; const aNamePath : string; const aDefaultValue : double ) : double;
    function StoreverseDateTimeStampAsDateTime( const aDateTimeStamp : string ) : TDateTime;
    function SafeAsDateTime( aField : TField; aJSONObject : TJSONObject; const aNamePath : string ) : TDateTime;
    function SafeAsDeliveryAreaType( aJSONObject : TJSONObject; const aNamePath : string ) : byte;
    function SafeAsOrderStatusType( aJSONObject : TJSONObject; const aNamePath : string ) : word;
    function ConvertStoreverseBooleanToString( const aStoreverseJSONBooleanValue : string ) : string;
    function ConvertDateToStoreverseQueryDate( const aDate : TDate ) : string;
    function InternalRepeatRequest( aRequestURL : string; var aRequestSkipCounter : word; var aJSONResponse : TJSONObject; const aUsername, aPassword : string ) : boolean;

    procedure SaveLog( const aLogFileName : string );
    procedure AddToOverallLog( const aLogMessage : string );

    procedure RequestModifiedProducts;
      function CreateOrUpdateProduct( aJSONProductProperties : TJSONObject ) : boolean;

    procedure RequestModifiedOrders;
      function CreateOrUpdateOrder( aJSONOrder : TJSONValue ) : boolean;
        function CreateCustomerIfNeeded( aJSONOrderDefaults : TJSONObject ) : integer;
        function CreateDeliveryIfNeeded( const aUniqueEntityID : integer; aJSONOrderDefaults : TJSONObject; var aDeliveryDate : TDateTime; const aOrderStatus : word ) : integer;
        function CreatePaymentIfNeeded( const aUniqueEntityID, aCustomerDeliveryID : integer; aJSONOrderProperties, aJSONOrderDefaults : TJSONObject; const aOrderStatus : word ) : integer;
        procedure AddProviderOrderIfNeeded( const aUniqueEntityID, aPaymentID : Integer; aDeliveryDate : TDateTime; aJSONOrderProperties, aJSONOrderDefaults, iJSONProductDefaults : TJSONObject; aProduct_ByGTIN : TFDQuery );
        procedure AddProduct( const aOrderID : integer; iJSONProductDefaults : TJSONObject; aProduct_ByGTIN : TFDQuery );
  public
  end;

var
  window_Server: Twindow_Server;

implementation

{$R *.dfm}

uses
  gFieldNames, gStoreverseGlobals,
  system.DateUtils;

function Twindow_Server.SafeJSONString( aJSONObject : TJSONObject; const aNamePath : string ) : string;
{ TODO 3 -ctodo : Implement option to raise an exception }
var
  iJSONValue : TJSONValue;
begin
  result := '';

  iJSONValue := aJSONObject.FindValue( aNamePath );
  if iJSONValue <> nil then
    result := trim( iJSONValue.Value )
  else
    Memo_Log.Lines.Add( ' ! ERROR in SafeJSONString. "'+ aNamePath +'" item was not found!' );
end;(* SafeJSONString *)

function Twindow_Server.SafeAsInteger( aJSONObject : TJSONObject; const aNamePath : string ) : integer;
var
  iProcessValue : string;
begin
  iProcessValue := SafeJSONString( aJSONObject, aNamePath );
  if iProcessValue <> 'null' then
    result := StrToIntDef( iProcessValue, 0 )
  else
  { TODO 1 -cthink :
    onko 0 sopiva vastine NULL arvolle vai pit��k� koko kent�n arvo j�tt�� asettamatta?
    Magento rajapinnassa ei varmaankaan NULL arvoja k�sitell� sen kummemmin? }
    result := 0;
end;(* SafeAsInteger *)

function Twindow_Server.SafeAsFloatDef( aJSONObject : TJSONObject; const aNamePath : string; const aDefaultValue : double ) : double;
(* Storeverse decimal separator is . dot *)
var
  iProcessValue : string;
  iFormatSettings : TFormatSettings;
begin
  iFormatSettings.Create( 'fi-FI' );
  try
    iFormatSettings.DecimalSeparator := '.';

    iProcessValue := SafeJSONString( aJSONObject, aNamePath );
    if iProcessValue <> 'null' then
      result := StrToFloatDef( iProcessValue, 0, iFormatSettings )
    else
    { TODO 1 -cthink :
      onko 0 sopiva vastine NULL arvolle vai pit��k� koko kent�n arvo j�tt�� asettamatta?
      Magento rajapinnassa ei varmaankaan NULL arvoja k�sitell� sen kummemmin? }
      result := 0;
  finally
    FreeAndNil( iFormatSettings );
  end;
end;(* SafeAsInteger *)

function Twindow_Server.StoreverseDateTimeStampAsDateTime( const aDateTimeStamp : string ) : TDateTime;
(* Storeverse date time format "created_at": "2020-11-11T13:58:58" *)
var
  iYear, iMonth, iDay, iHour, iMinute, iSecond, iMillisecond: Word;
begin
  { TODO 2 -ctodo : Handle invalid dates }
  iYear        := StrToInt(Copy(aDateTimeStamp, 1, 4));
  iMonth       := StrToInt(Copy(aDateTimeStamp, 6, 2));
  iDay         := StrToInt(Copy(aDateTimeStamp, 9, 2));

  if length( aDateTimeStamp ) > 15 then
  begin
    iHour        := StrToInt(Copy(aDateTimeStamp, 12, 2));
    iMinute      := StrToInt(Copy(aDateTimeStamp, 15, 2));
    iSecond      := StrToInt(Copy(aDateTimeStamp, 18, 2));
    iMillisecond := 0;

    result := EncodeDateTime( iYear, iMonth, iDay, iHour, iMinute, iSecond, iMillisecond);
  end else
    result := EncodeDate( iYear, iMonth, iDay );
end;(* StoreverseDateTimeStampAsDateTime *)

procedure Twindow_Server.Timer_EventsTimer(Sender: TObject);
(* timed events. Run once / day *)
begin
  Timer_Events.Enabled := false;
  try
    (* allow only early night processing, before 05:00 *)
    if Time < EncodeTime( 05, 0, 0, 0 ) then
    begin
      (* update products at 01:00 *)
      if FTimer_Products < Date then
        if Time >= EncodeTime( 01, 0, 0, 0 ) then
        begin
          FTimer_Products := Date;
          RequestModifiedProducts;
        end;

      (* prefetch orders at 01:30 *)
      if FTimer_OrdersPreFetch < Date then
        if Time >= EncodeTime( 01, 30, 0, 0 ) then
        begin
          FTimer_OrdersPreFetch := Date;
          RequestModifiedOrders;
        end;

      (* fetch latest orders at 02:10 *)
      if FTimer_OrdersFetch < Date then
        if Time >= EncodeTime( 02, 10, 0, 0 ) then
        begin
          FTimer_OrdersFetch := Date;
          RequestModifiedOrders;
        end;
    end;

  finally
    Timer_Events.Enabled := true;
  end;
end;

function Twindow_Server.SafeAsDateTime( aField : TField; aJSONObject : TJSONObject; const aNamePath : string ) : TDateTime;
var
  iProcessValue : string;
begin
  iProcessValue := SafeJSONString( aJSONObject, aNamePath );
  if iProcessValue <> 'null' then
    aField.AsDateTime := StoreverseDateTimeStampAsDateTime( iProcessValue )
  else
    aField.Clear;
end;(* SafeAsDate *)

function Twindow_Server.SafeAsDeliveryAreaType( aJSONObject : TJSONObject; const aNamePath : string ) : byte;
var
  iProcessValue : string;
begin
  result := DMDELIVERYAREAS_DELIVERYAREATYPE_NONE;

  iProcessValue := SafeJSONString( aJSONObject, aNamePath );
  if iProcessValue <> 'null' then
    if iProcessValue = 'CHD001' then
      result := DMDELIVERYAREAS_DELIVERYAREATYPE_POSTNUMBER
    else
      if iProcessValue = 'PO2103' then
        result := DMDELIVERYAREAS_DELIVERYAREATYPE_SMARTSHIP_POSTPICKUP
      else
        if iProcessValue = 'PO2104' then
          result := DMDELIVERYAREAS_DELIVERYAREATYPE_SMARTSHIP_HOMEDELIVERY
end;(* SafeAsDeliveryAreaType *)

function Twindow_Server.SafeAsOrderStatusType( aJSONObject : TJSONObject; const aNamePath : string ) : word;
var
  iProcessValue : string;
begin
  result := Storeverse_Order_OrderStatus_NewOrder;

  iProcessValue := AnsiLowerCase( SafeJSONString( aJSONObject, aNamePath ));
  if iProcessValue <> 'null' then
    if iProcessValue = 'new order' then
      result := Storeverse_Order_OrderStatus_NewOrder
    else
      if iProcessValue = 'delivery in quenue' then
        result := Storeverse_Order_OrderStatus_DeliveryInQuenue
      else
        if iProcessValue = 'delivery in processing' then
          result := Storeverse_Order_OrderStatus_DeliveryInProcessing
      else
        if iProcessValue = 'delivery dispatched' then
          result := Storeverse_Order_OrderStatus_DeliveryDispatched
      else
        if iProcessValue = 'order reimbursed' then
          result := Storeverse_Order_OrderStatus_OrderReimbursed
      else
        if iProcessValue = 'delivery confirmation sent' then
          result := Storeverse_Order_OrderStatus_DeliveryConfirmationSent
      else
        if iProcessValue = 'order completed' then
          result := Storeverse_Order_OrderStatus_OrderCompleted
      else
        if iProcessValue = 'payment failed' then
          result := Storeverse_Order_OrderStatus_PaymentFailed
      else
        if iProcessValue = 'order cancelled' then
          result := Storeverse_Order_OrderStatus_OrderCancelled
      else
        if iProcessValue = 'delivery failed' then
          result := Storeverse_Order_OrderStatus_DeliveryFailed
        else
          AddToOverallLog( 'Order status was unknown "'+ iProcessValue + '"' );
end;(* SafeAsOrderStatusType *)

function Twindow_Server.ConvertStoreverseBooleanToString( const aStoreverseJSONBooleanValue : string ) : string;
begin
  if AnsiLowerCase( aStoreverseJSONBooleanValue ) = 'true' then
    result := '1'
  else
    result := '0';
end;(* ConvertStoreverseBooleanToString *)

function Twindow_Server.ConvertDateToStoreverseQueryDate( const aDate : TDate ) : string;
var
  iFormatSettings : TFormatSettings;
begin
  iFormatSettings.Create( 'fi-FI' );
  try
    iFormatSettings.DateSeparator := '-';
    result := FormatDateTime( 'yyyy/mm/dd', aDate, iFormatSettings );
  finally
    FreeAndNil( iFormatSettings );
  end;
end;(* ConvertDateToStoreverseQueryDate *)

function Twindow_Server.InternalRepeatRequest( aRequestURL : string; var aRequestSkipCounter : word; var aJSONResponse : TJSONObject; const aUsername, aPassword : string ) : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iResponseStr : string;
  iJSONValue : TJSonValue;
  iMetaTotal, iItemsCount, iMetalSkip : integer;
begin
  result := false;

  if aRequestSkipCounter > 0 then
    aRequestURL := aRequestURL + '&skip='+ inttostr( aRequestSkipCounter );

  Memo_Log.Lines.Add( 'request "'+ aRequestURL + '"' );
  iRequest := TScHttpWebRequest.Create( aRequestURL );
  try
    iRequest.SSLOptions.TrustServerCertificate := FIniFile.ReadBool( gSectionSettings, gStoreverseAPITrustServerCertificate, false );
    iRequest.Credentials.UserName := aUsername;
    iRequest.Credentials.Password := aPassword;

    try
      iResponse := iRequest.GetResponse;
      iResponseStr := iResponse.ReadAsString;
      if Length( iResponseStr ) > 2 then
      //if Request.IsSecure then
      begin
        aJSONResponse := TJSONObject.ParseJSONValue( iResponseStr ) as TJSONObject;
        if aJSONResponse <> nil then
        begin
          iJSONValue := aJSONResponse.FindValue( 'status' );
          if iJSONValue <> nil then
            if iJSONValue.Value <> '200' then
              Memo_Log.Lines.Add( 'Invalid request status = '+ iJSONValue.Value );
              { TODO 2 -cthink : pit�isik� t�h�n kohtaa pys�ytt�� k�sittely jos arvo on <> 200 }

          iJSONValue := aJSONResponse.FindValue( 'meta.total' );
          if iJSONValue <> nil then iMetaTotal := StrToIntDef( iJSONValue.Value, -1 ) else iMetaTotal := -2;
          iJSONValue := aJSONResponse.FindValue( 'items' );
          if iJSONValue <> nil then iItemsCount := StrToIntDef( iJSONValue.Value, -1 ) else iItemsCount := -2;
          iJSONValue := aJSONResponse.FindValue( 'meta.skip' );
          if iJSONValue <> nil then iMetalSkip := StrToIntDef( iJSONValue.Value, -1 ) else iMetalSkip := -2;

          Memo_Log.Lines.Add( 'meta.Total = '+ inttostr( iMetaTotal ));
          Memo_Log.Lines.Add( 'meta.Skip = '+ inttostr( iMetalSkip ));
          Memo_Log.Lines.Add( 'Items = '+ inttostr( iItemsCount ));

          result := iItemsCount > 0;
          Memo_Log.Lines.Add( 'result = '+ inttostr( ord( result )));

          aRequestSkipCounter := aRequestSkipCounter + iItemsCount;
          Memo_Log.Lines.Add( 'new skip value = '+ inttostr( aRequestSkipCounter ));

        end else Memo_Log.Lines.Add( 'InternalRepeatRequest: aJSONResponse = nil' );
      end else Memo_Log.Lines.Add( 'InternalRepeatRequest: aJSONResponse was empty "'+ iResponseStr + '"' );

    finally
      iResponse.Free;
    end;

  finally
    iRequest.disconnect;
    iRequest.Free;
  end;
end;(* InternalRepeatRequest *)

procedure Twindow_Server.RequestModifiedProducts;
const
  LIMITPRODUCTCOUNT = 200;
var
  iJSONResponse : TJSONObject;
  iJSONProductArray: TJSONArray;
  iJSONProductProperties : TJSONObject;
  iJSONValue: TJSonValue;
  iProducts : Integer;
  iRequestSkipCounter : word;
begin
  Memo_Log.Lines.Clear;

  AddToOverallLog( 'RequestModifiedProducts begin' );
  try
    try
      iRequestSkipCounter := 0;
      while InternalRepeatRequest( FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) +
                                   'product?' +
                                   'X-Token=' + FIniFile.ReadString( gSectionSettings, gStoreverseAPIToken, '' ) +
                                   '&order=C001&direction=asc&limit='+ inttostr( LIMITPRODUCTCOUNT ),
                                   iRequestSkipCounter, iJSONResponse,
                                   FIniFile.ReadString( gSectionSettings, gStoreverseUsername, 'janne.timmerbacka@kauppahalli24.fi' ),
                                   FIniFile.ReadString( gSectionSettings, gStoreversePassword, '_!S4f_Fr0NN' )) do
      begin
        if iJSONResponse <> nil then
        begin
          (* PRODUCTS *************************************************)
          if FDQuery_Products.active then FDQuery_Products.close;
          if FDQuery_Products.IndexFieldNames <> PRODUCTS_BARCODE then FDQuery_Products.IndexFieldNames := PRODUCTS_BARCODE;
          FDQuery_Products.Open;

          try
            iJSONProductArray := iJSONResponse.FindValue( 'result' ) as TJSONArray;
            if iJSONProductArray <> nil then
            begin
              if iJSONProductArray.Count > 0 then
              for iProducts := 0 to pred( iJSONProductArray.Count ) do
              begin
                iJSONProductProperties := iJSONProductArray.Items[ iProducts ].FindValue( 'properties' ) as TJSONObject;
                if iJSONProductProperties <> nil then
                begin
                  if not CreateOrUpdateProduct( iJSONProductProperties ) then
                  begin
                    iJSONValue := iJSONProductArray.Items[ iProducts ].FindValue( 'id' );
                    if iJSONValue <> nil then
                      Memo_Log.Lines.Add( 'Updating product id '+ iJSONValue.Value +' failed!' )
                    else
                      Memo_Log.Lines.Add( 'Updating product failed! Did not have id!' );
                  end;
                end;(* iJSONProductProperties <> nil *)
              end;(* for iProducts *)

            end else Memo_Log.Lines.Add( 'iJSONProductArray = nil' );

            Memo_Log.Lines.Add( 'FDQuery_Products.ChangeCount='+ inttostr( FDQuery_Products.ChangeCount ));
            SaveLog( gLogGetModifiedProducts );

          finally
            FreeAndNil( iJSONProductArray );
            if FDQuery_Products.ChangeCount > 0 then
            begin
              FDQuery_Products.ApplyUpdates;
              FDQuery_Products.CommitUpdates;
            end;
            if FDQuery_Products.active then FDQuery_Products.close;
          end;(* try PRODUCTS *)

        end;(* iJSONResponse <> nil *)
          { TODO 2 -cthink : pit�isik� nil case k�sitell� }
      end;(* while *)

    except
      On E:Exception do
      begin
        Memo_Log.Lines.Add( 'RequestModifiedProducts.EXCEPTION '+ E.Message );
        AddToOverallLog( 'RequestModifiedProducts.EXCEPTION '+ E.Message );
      end;
    end;

  finally
    SaveLog( gLogGetModifiedProducts );
    AddToOverallLog( 'RequestModifiedProducts end' );
  end;
end;(* RequestModifiedProducts *)

procedure Twindow_Server.SaveLog( const aLogFileName : string );
begin
  Memo_Log.Lines.SaveToFile( IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + aLogFileName );
end;(* SaveLog *)

procedure Twindow_Server.AddToOverallLog( const aLogMessage : string );
begin
  Memo_TimedLog.Lines.Add( DateTimeToStr( now ) + ': ' + aLogMessage );
  Memo_TimedLog.Lines.SaveToFile( IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + 'Heartwood4Storeverse.log' );
end;(* AddToOverallLog *)

function Twindow_Server.CreateOrUpdateProduct( aJSONProductProperties : TJSONObject ) : boolean;
(* NOTE !!!!!
   PRODUCTS autoinc trigger must be deleted for the product "cloning" to work *)
var
  iJSONValue: TJSonValue;
  iDeliveryGTIN : string;

  procedure LogDifferences( const aHeartwoodFieldName : string; aJSONObject : TJSONObject; const aFieldName : string );
  var
    iJSONValue: TJSonValue;
  begin
    iJSONValue := aJSONObject.FindValue( aFieldName );
    if iJSONValue <> nil then
    begin
      if iJSONValue.Value <> 'null' then { TODO 2 -cthink : pit�isik� null = 0 tai '' vastineet varmuuden vuoksi tarkistaa? }
        if CompareText( FDQuery_Products.FieldByName( aHeartwoodFieldName ).AsString, iJSONValue.Value ) <> 0 then
          Memo_Log.Lines.Add( '  Field "'+ aFieldName + '" diff HW:"' +
                              FDQuery_Products.FieldByName( aHeartwoodFieldName ).AsString + '" JSON:"' +
                              iJSONValue.Value + '" !' );
    end else
      Memo_Log.Lines.Add( '  Field "'+ aFieldName + '" was not found!' );
  end;(* LogDifferences *)

  function SafeAsString( aJSONObject : TJSONObject; const aFieldName : string ) : string;
  var
    iJSONValue: TJSonValue;
  begin
    result := '';
    iJSONValue := aJSONObject.FindValue( aFieldName );
    if iJSONValue <> nil then
      result := iJSONValue.Value
    else
      Memo_Log.Lines.Add( 'SafeAsString: Field "'+ aFieldName + '" was not found!' );
  end;(* SafeAsString *)

  procedure _UpdateProduct;
  begin
    { TODO 1 -ctodo : ProductDataUpdateFromStoreverseIsDisabled }
    FDQuery_Products.FieldByName( PRODUCTS_PRODUCTORDER ).AsInteger := 1; // required for syd�npuu terminal collecting view to display ordered products "in order".

    exit;

    LogDifferences( PRODUCTS_BARCODE, aJSONProductProperties, 'S004' );
    LogDifferences( PRODUCTS_SALEBATCHBARCODE, aJSONProductProperties, 'S005' );
    FDQuery_Products.FieldByName( PRODUCTS_BARCODE ).AsString := SafeAsString( aJSONProductProperties, 'S004' ); //S004 / Delivery GTIN
    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'S005' ); //S005 / Supply GTIN
//PRODUCTS_SALEBATCHSIZE

    //FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'A006' ); //A006 / Storage "A006": "Warm",
//      PRODUCTS_STORAGETYPE = 'STORAGETYPE'; (* added 14.04.2014 *)
//    PRODUCTS_STORAGETYPE_FREEZE = 1;
//    PRODUCTS_STORAGETYPE_COLD = 2;        //04.03.2020 obsolete
//    PRODUCTS_STORAGETYPE_COLDORWARM = 3;  //04.03.2020 obsolete
//    PRODUCTS_STORAGETYPE_WARM = 4;
//    PRODUCTS_STORAGETYPE_COLD2 = 5;       //04.03.2020 added
//    PRODUCTS_STORAGETYPE_COLD6 = 6;       //04.03.2020 added
//    PRODUCTS_STORAGETYPE_COLD8 = 7;       //04.03.2020 added

//PRODUCTS_PURCHASEPRICE
//PRODUCTS_PRICE
//  PRODUCTS_PRODUCTSTATE = 'PRODUCTSTATE';
//    PRODUCTS_PRODUCTSTATE_FUTURENOTVISIBLE = 403;     //20.11.2017 Ei aktiivisessa k�yt�ss� Magento 2 k�ytt��notosta eteenp�in
//    PRODUCTS_PRODUCTSTATE_NEW = 406;                  //20.11.2017 Ei aktiivisessa k�yt�ss� Magento 2 k�ytt��notosta eteenp�in
//    PRODUCTS_PRODUCTSTATE_SPECIALOFFER = 409;         //20.11.2017 Ei aktiivisessa k�yt�ss� Magento 2 k�ytt��notosta eteenp�in
//    PRODUCTS_PRODUCTSTATE_NORMAL = 412;
//    PRODUCTS_PRODUCTSTATE_WILLBEREMOVED = 415;        //20.11.2017 Ei aktiivisessa k�yt�ss� Magento 2 k�ytt��notosta eteenp�in
//    PRODUCTS_PRODUCTSTATE_SPECIALREMOVALSALE = 418;   //20.11.2017 Ei aktiivisessa k�yt�ss� Magento 2 k�ytt��notosta eteenp�in
//    PRODUCTS_PRODUCTSTATE_REMOVED = 421;
//PRODUCTS_VAT
//PRODUCTS_SALEBATCHUNITHEIGHT = 'SALEBATCHUNITHEIGHT';
//PRODUCTS_SALEBATCHUNITWIDTH = 'SALEBATCHUNITWIDTH';
//PRODUCTS_SALEBATCHUNITDEPTH = 'SALEBATCHUNITDEPTH';
//PRODUCTS_SUPPLIERSNAME
//PRODUCTS_SUPPLIERSCODE = 'SUPPLIERSCODE';
//PRODUCTS_EXTERNALID = 'EXTERNALID';
//PRODUCTS_INSTOCK
//PRODUCTS_LIFECYCLE = 'LIFECYCLE'; (* added 14.04.2014 *)
//  PRODUCTS_LIFECYCLE_INCOMING = 1;
//  PRODUCTS_LIFECYCLE_ONSALE = 2;
//  PRODUCTS_LIFECYCLE_TAUKO = 3;
//  PRODUCTS_LIFECYCLE_REMOVED = 4;
//PRODUCTS_DELIVERYINSALESBATCHES = 'DELIVERYINSALESBATCHES'; //terminaalissa k�sittely myyntieritt�in, koska toimitetaan myyntieritt�in loppuasiakkaalle
//  PRODUCTS_PRODUCTCOLLECTINGANDCHARGE = 'PRODUCTCOLLECTINGANDCHARGE'; (* added 12.05.2014 *)
//    PRODUCTS_PRODUCT_CAC_COLLECTPCSANDCHARGEPCS = 1;
//    PRODUCTS_PRODUCT_CAC_COLLECTKGANDCHARGEKG = 2;
//    PRODUCTS_PRODUCT_CAC_COLLECTPCSANDCHARGEKG = 3;
//PRODUCTS_TUKOPRODUCTTREE

    //LogDifferences( PRODUCTS_DATETRACKING, aJSONProductProperties, 'A008' );
    LogDifferences( PRODUCTS_MINIMUMUSAGEDAYS, aJSONProductProperties, 'A010' );
    FDQuery_Products.FieldByName( PRODUCTS_DATETRACKING ).AsString := ConvertStoreverseBooleanToString( SafeAsString( aJSONProductProperties, 'A008' )); //A008 / Date tracking "A008": true,
    FDQuery_Products.FieldByName( PRODUCTS_MINIMUMUSAGEDAYS ).AsInteger := SafeAsInteger( aJSONProductProperties, 'A010' ); //A010 / Freshness promise "A010": 60,
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'A011' ); //A011 / Frozen "A011": false,
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'C001' ); //C001 / Product code "C001": 2063,
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'C002' ); //C002 / Product type "C002": "PACK",
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'C003' ); //C003 / Is public "C003": true,
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'C004' ); //C004 / Product group "C004": 1190200,

    LogDifferences( PRODUCTS_PRODUCTNAME, aJSONProductProperties, 'C005' );
    FDQuery_Products.FieldByName( PRODUCTS_PRODUCTNAME ).AsString := SafeAsString( aJSONProductProperties, 'C005' ); //C005 / Internal name "C005": "ELOMENTS ORGANIC VITAMIN TEA DOUBLE LEMON LUOMUTEE 14PSS/ 28G",
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'C007' ); //C007 / Has variable weight "C007": false,
    FDQuery_Products.FieldByName( PRODUCTS_PRODUCTIMAGEURL ).AsString := SafeAsString( aJSONProductProperties, 'F000' ); //F000 / Product picture "F000": "https://www.kauppahalli24.fi/pub/media/catalog/product/cache/a7fa59de44ae400e652bf7c13548bd46/9/3/9352177000026.jpg",
    //FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'L005' ); //L005 / Primary label "L005": "Eloments Organic Vitamin tea",
    //FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'L006' ); //L006 / Secondary label "L006": "Double Lemontee luomu",
    //FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'L900' ); //L900 / Internal comment "L900": "",
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'M002' ); //M002 / Campaign price "M002": "",
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'S007' ); //S007 / Warehouse stock "S007": 11,
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'S008' ); //S008 / Next supply time "S008": "",
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'S009' ); //S009 / Stock is limited "S009": true,
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'S900' ); //S900 / Reference key "S900": "EL0005",
    LogDifferences( PRODUCTS_AUTOMATICWAREHOUSEORDER_ALARMAMOUNT, aJSONProductProperties, 'S901' );
    LogDifferences( PRODUCTS_AUTOMATICWAREHOUSEORDER_ORDERAMOUNTINSALEBATCHES, aJSONProductProperties, 'S902' );
{ TODO 2 -cbug : T�m� ei toimi, arvo = 0     LogDifferences( PRODUCTS_PROVIDERID, aJSONProductProperties, 'S903' );}
    FDQuery_Products.FieldByName( PRODUCTS_AUTOMATICWAREHOUSEORDER_ALARMAMOUNT ).AsInteger := SafeAsInteger( aJSONProductProperties, 'S901' ); //S901 / Reorder alert level "S901": 2,
    FDQuery_Products.FieldByName( PRODUCTS_AUTOMATICWAREHOUSEORDER_ORDERAMOUNTINSALEBATCHES ).AsInteger := SafeAsInteger( aJSONProductProperties, 'S902' ); //S902 / Reorder quantity "S902": 1,
{ TODO 2 -cbug : T�m� ei toimi, arvo = 0     FDQuery_Products.FieldByName( PRODUCTS_PROVIDERID ).AsInteger := SafeAsInteger( aJSONProductProperties, 'S903' ); //S903 / Supplier reference "S903": 194}
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'U003' ); //U003 / Gross volume "U003": "",
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'U004' ); //U004 / Net volume "U004": 2,
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'U005' ); //U005 / Volume title, default value = g "U005": "g",
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'U007' ); //U007 / Volume conversion "U007": 1000,
    LogDifferences( PRODUCTS_UNITWIDTH, aJSONProductProperties, 'U008' );
    LogDifferences( PRODUCTS_UNITHEIGHT, aJSONProductProperties, 'U009' );
    LogDifferences( PRODUCTS_UNITDEPTH, aJSONProductProperties, 'U010' );
    LogDifferences( PRODUCTS_UNITWEIGHT, aJSONProductProperties, 'U011' );
    FDQuery_Products.FieldByName( PRODUCTS_UNITWIDTH ).AsInteger := SafeAsInteger( aJSONProductProperties, 'U008' ); //U008 / Delivery width "U008": 68,
    FDQuery_Products.FieldByName( PRODUCTS_UNITHEIGHT ).AsInteger := SafeAsInteger( aJSONProductProperties, 'U009' ); //U009 / Delivery height "U009": 78,
    FDQuery_Products.FieldByName( PRODUCTS_UNITDEPTH ).AsInteger := SafeAsInteger( aJSONProductProperties, 'U010' ); //U010 / Delivery depth "U010": 118,
    FDQuery_Products.FieldByName( PRODUCTS_UNITWEIGHT ).AsInteger := SafeAsInteger( aJSONProductProperties, 'U011' ); //U011 / Delivery weight "U011": "0,028",
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'U015' ); //U015 / Minimum storage temperature "U015": 10,
//    FDQuery_Products.FieldByName( PRODUCTS_SALEBATCHBARCODE ).AsString := SafeAsString( aJSONProductProperties, 'U016' ); //U016 / Maximum storage temperature "U016": 25,
//    PRODUCTS_DELETED
    FDQuery_Products.FieldByName( PRODUCTS_MODIFIED ).AsDateTime := Now;
  end;(* _UpdateProduct *)

  procedure _TemporaryUpdate;
  (* temporarely copy content of every field *)
  var
    iFieldCounter : word;
  begin
    for iFieldCounter := 0 to pred( FDQuery_Products.Fields.Count ) do
      if FDQuery_Products.Fields.Fields[ iFieldCounter ].FieldName <> PRODUCTS_ID then
        FDQuery_Products.FieldByName( FDQuery_Products.Fields.Fields[ iFieldCounter ].FieldName ).AsVariant :=
          FDQuery_KH24_Product_ByGTIN.FieldByName( FDQuery_Products.Fields.Fields[ iFieldCounter ].FieldName ).AsVariant;
  end;(* _TemporaryUpdate *)

  function _VerifyProductState( var aReasonForProductFailure : string ) : boolean;
  begin
    aReasonForProductFailure := '';
    result := FDQuery_KH24_Product_ByGTIN.RecordCount = 1;

    if not result then
    begin
      (* if product is missing *)
      if FDQuery_KH24_Product_ByGTIN.RecordCount = 0 then
      begin
        aReasonForProductFailure := 'Product was not found in master db.';
        exit;
      end;

      (* search until valid product is found *)
      aReasonForProductFailure := 'Duplicate products in master db and no valid in sight.';
      while not FDQuery_KH24_Product_ByGTIN.eof do
      begin
        if FDQuery_KH24_Product_ByGTIN.FieldByName( PRODUCTS_LIFECYCLE ).AsInteger < PRODUCTS_LIFECYCLE_REMOVED then
        begin
          aReasonForProductFailure := 'Duplicate products in master db, but valid was found.';
          Memo_Log.Lines.Add( 'Creating '+ iDeliveryGTIN + ' since valid duplicate was found.' );
          result := true;
          exit;
        end;

        FDQuery_KH24_Product_ByGTIN.Next;
      end;
    end;
  end;(* _VerifyProductState  *)

var
  iProductFailedReason : string;
begin
  result := false;

  try
    iJSONValue := aJSONProductProperties.FindValue( 'S004' );
    if iJSONValue <> nil then
    begin
      iDeliveryGTIN := iJSONValue.Value;

      (* does product already exists? *)
      if FDQuery_Products.FindKey( [ iDeliveryGTIN ]) then
      begin
        if FDQuery_Products.Recordcount > 0 then
        begin
          (* update product *)
          FDQuery_Products.edit;

          Memo_Log.Lines.Add( 'Updating '+ iDeliveryGTIN );

          if FDQuery_KH24_Product_ByGTIN.Active then FDQuery_KH24_Product_ByGTIN.Close;
          FDQuery_KH24_Product_ByGTIN.ParamByName( 'paramBARCODE' ).AsString := iDeliveryGTIN;
          FDQuery_KH24_Product_ByGTIN.Active := true;
          if _VerifyProductState( iProductFailedReason ) then
            _TemporaryUpdate
          else
            Memo_Log.Lines.Add( '  Invalid recordcount '+ inttostr( FDQuery_KH24_Product_ByGTIN.RecordCount ));

          _UpdateProduct;
        end else
          Memo_Log.Lines.Add( 'Updating '+ iDeliveryGTIN + ' failed due recordcount = 0' );
      end else
      begin
        (* new product - get KH24 product ID for symmerty in PurchaseOrders *)
        if FDQuery_KH24_Product_ByGTIN.Active then FDQuery_KH24_Product_ByGTIN.Close;
        FDQuery_KH24_Product_ByGTIN.ParamByName( 'paramBARCODE' ).AsString := iDeliveryGTIN;
        FDQuery_KH24_Product_ByGTIN.Active := true;

        if _VerifyProductState( iProductFailedReason ) then
        begin
          FDQuery_Products.Append;

          Memo_Log.Lines.Add( 'Creating '+ iDeliveryGTIN );
          _TemporaryUpdate;

          FDQuery_Products.FieldByName( PRODUCTS_CREATED ).AsDateTime := Now;
          FDQuery_Products.FieldByName( PRODUCTS_ID ).AsInteger := FDQuery_KH24_Product_ByGTIN.FieldByName( PRODUCTS_ID ).AsInteger;

          _UpdateProduct;
        end else (* recordcount = 1 *)
          Memo_Log.Lines.Add( '  Invalid recordcount '+ inttostr( FDQuery_KH24_Product_ByGTIN.RecordCount ));

      end;(* findkey *)

      if FDQuery_Products.state in [dsEdit, dsInsert ] then
        FDQuery_Products.post
      else
      begin
        Memo_Log.Lines.Add( '  Product ' + iDeliveryGTIN + ' was not processed! RecordCount=' + inttostr( FDQuery_KH24_Product_ByGTIN.RecordCount ) + ' "'+ iProductFailedReason + '"' );
        AddToOverallLog( '  Product ' + iDeliveryGTIN + ' was not processed! RecordCount=' + inttostr( FDQuery_KH24_Product_ByGTIN.RecordCount ) + ' "'+ iProductFailedReason + '"' );
      end;

      if FDQuery_Products.ChangeCount > 100 then
      begin
        Memo_Log.Lines.Add( '  Applying '+ inttostr( FDQuery_Products.ChangeCount ) + ' changes.' );
        FDQuery_Products.ApplyUpdates;
        FDQuery_Products.CommitUpdates;
      end;

      result := true;

    end else Memo_Log.Lines.Add( 'S004 delivery GTIN was not found in JSON!' );

  except
    On E:Exception do
    begin
      Memo_Log.Lines.Add( 'CreateOrUpdateProduct.EXCEPTION '+ E.Message );
      AddToOverallLog( 'CreateOrUpdateProduct.EXCEPTION '+ E.Message );
    end;
  end;
end;(* CreateOrUpdateProduct *)

procedure Twindow_Server.RequestModifiedOrders;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iResponseStr : string;
  iJSONOrderArray : TJsonArray;
  iOrderCounter : word;
  iModifiedSince : TDate;
begin
  Memo_Log.Lines.Clear;

  AddToOverallLog( 'RequestModifiedOrders begin' );

  (* when was the last successful get orders check *)
  iModifiedSince := FIniFile.ReadDate( gSectionProcess, gLastGetOrdersDateTime, EncodeDate( 2020, 11, 10 ));
  Memo_Log.Lines.add( 'Get orders since '+ ConvertDateToStoreverseQueryDate( iModifiedSince ));
  Memo_Log.Lines.add( 'Request: ' + FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) +
                                        'heartwood/order?'+
                                        'X-Token=' + FIniFile.ReadString( gSectionSettings, gStoreverseAPIToken, '' ) +
                                        '&filter=status=1'+
                                        '&between=updated_at='+ ConvertDateToStoreverseQueryDate( iModifiedSince ) +',2099-01-01' );

  iRequest := TScHttpWebRequest.Create( FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) +
                                        'heartwood/order?'+
                                        'X-Token=' + FIniFile.ReadString( gSectionSettings, gStoreverseAPIToken, '' ) +
                                        '&filter=status=1'+ // Only paid orders
                                        { TODO 2 -cvarmista miten : Nyt ei ole limitti� k�yt�ss�!
                                        '&limit=100' + }
                                        //'&limit=1&search=default.O001=1095');
                                        '&between=updated_at='+ ConvertDateToStoreverseQueryDate( iModifiedSince ) +',2099-01-01' );
                                        { TODO 1 -ctodo : toteuta tilausten lataus useassa palassa }
  try
    iRequest.SSLOptions.TrustServerCertificate := FIniFile.ReadBool( gSectionSettings, gStoreverseAPITrustServerCertificate, false );
    iRequest.Credentials.UserName := FIniFile.ReadString( gSectionSettings, gStoreverseUsername, 'janne.timmerbacka@kauppahalli24.fi' );
    iRequest.Credentials.Password := FIniFile.ReadString( gSectionSettings, gStoreversePassword, '_!S4f_Fr0NN' );

    try
      try
        iResponse := iRequest.GetResponse;
        iResponseStr := iResponse.ReadAsString;
        if Length( iResponseStr ) > 2 then
        //if Request.IsSecure then
        begin
          iJSONOrderArray := TJSONObject.ParseJSONValue( iResponseStr ) as TJSONArray;
          if iJSONOrderArray <> nil then
          begin
            (* ----debug ----- *)
            Memo_Log.Lines.Add( iResponseStr ); //iJSONOrderArray.Value );
            Memo_Log.Lines.Add( '' );
            SaveLog( gLogGetModifiedOrders );

            try
              for iOrderCounter := 0 to pred( iJSONOrderArray.Count ) do
                CreateOrUpdateOrder( iJSONOrderArray.Items[ iOrderCounter ]);

              (* when succesful -> store today as a last date *)
              FIniFile.WriteDate( gSectionProcess, gLastGetOrdersDateTime, Date );

            finally
              SaveLog( gLogGetModifiedOrders );
              iJSONOrderArray.Free;
            end;
          end else (* <> nil *)
            Memo_Log.Lines.Add( 'iJSONOrderArray was empty "'+ iResponseStr + '"' );
        end else
          Memo_Log.Lines.Add( 'Https request response was empty' );

      finally
        iResponse.Free;
      end;

    except
      On E:Exception do
      begin
        Memo_Log.Lines.Add( 'RequestModifiedOrders.EXCEPTION '+ E.Message );
        AddToOverallLog( 'RequestModifiedOrders.EXCEPTION '+ E.Message );
      end;
    end;

  finally
    SaveLog( gLogGetModifiedOrders );
    AddToOverallLog( 'RequestModifiedOrders end' );
    iRequest.disconnect;
    iRequest.Free;
  end;
end;(* RequestModifiedOrders *)

function Twindow_Server.CreateOrUpdateOrder( aJSONOrder : TJSONValue ) : boolean;
var
  iUniqueEntityID, iCustomerDeliveryID, iPaymentsID : integer;
  iJSONProductArray: TJsonArray;
  iJSONOrderProperties, iJSONOrderDefaults,
  iJSONProductDefaults, iJSONProductReference : TJSONObject;
  iJSONValue: TJSonValue;
  iProducts : Integer;
  iDeliveryDate : TDateTime;
  iOrderStatus : word;
begin
  result := false;

  try
    iJSONOrderProperties := aJSONOrder.FindValue('properties') as TJSONObject;
    if iJSONOrderProperties <> nil then
    begin
  //    iJSONValue := iJSONOrderProperty.FindValue( 'created_at' );
  //    if iJSONValue <> nil then Memo_Log.Lines.Add( ' created_at '+ iJSONValue.Value );
      iJSONOrderDefaults := iJSONOrderProperties.FindValue('default') as TJSONObject;
      if iJSONOrderDefaults <> nil then
      begin
        Memo_Log.Lines.Add( 'ORDER '+ SafeJSONString( iJSONOrderDefaults, Storeverse_Order_OrderID ));

        iOrderStatus := SafeAsOrderStatusType( iJSONOrderDefaults, Storeverse_Order_OrderStatus );

        iUniqueEntityID := CreateCustomerIfNeeded( iJSONOrderDefaults );
        iCustomerDeliveryID := CreateDeliveryIfNeeded( iUniqueEntityID, iJSONOrderDefaults, iDeliveryDate, iOrderStatus );
        iPaymentsID := CreatePaymentIfNeeded( iUniqueEntityID, iCustomerDeliveryID, iJSONOrderProperties, iJSONOrderDefaults, iOrderStatus );

        (* Abort future processing for cancelled orders *)
        if iOrderStatus = Storeverse_Order_OrderStatus_OrderCancelled then exit;

        (* ORDER PRODUCTS **************************************************************)
        iJSONProductArray := iJSONOrderProperties.FindValue( 'products' ) as TJSONArray;
        if iJSONProductArray <> nil then
        begin
          (* Set orderrows parameters and open existing orderrows *)
          with FDQuery_OrderRows_ByOrdersPaymentId do
          begin
            with FDQuery_OrderRows_ByOrdersPaymentId do if active then close;
            FDQuery_OrderRows_ByOrdersPaymentId.IndexFieldNames := ORDERROWS_PRODUCTID;
            FDQuery_OrderRows_ByOrdersPaymentId.ParamByName( 'paramPAYMENTID' ).AsInteger := iPaymentsID;
            FDQuery_OrderRows_ByOrdersPaymentId.Open;
          end;

          try
            for iProducts := 0 to pred( iJSONProductArray.Count ) do
            begin
  //          iJSONValue := iJSONProductArray.Items[ iProducts ].FindValue( 'product_id' );
  //          if iJSONValue <> nil then Memo_Log.Lines.Add( '  - product_id ['+ inttostr( iProducts ) +'] '+ iJSONValue.Value );

              iJSONProductDefaults := iJSONProductArray.Items[ iProducts ].FindValue('default') as TJSONObject;
              if iJSONProductDefaults <> nil then
              begin
                (* Find product *)
                iJSONValue := iJSONProductDefaults.FindValue( 'R090' ); //Product Delivery GTIN	R090
                if iJSONValue <> nil then
                begin
                  Memo_Log.Lines.Add( ' -> GTIN '+ iJSONValue.Value );
                  with FDQuery_Product_ByGTIN do if active then close;
                  FDQuery_Product_ByGTIN.ParamByName( 'paramBARCODE' ).AsString := iJSONValue.Value;
                  FDQuery_Product_ByGTIN.open;

                  if FDQuery_Product_ByGTIN.RecordCount = 1 then
                  begin
                    (* Create Orders and rows *)
                    AddProviderOrderIfNeeded( iUniqueEntityID, iPaymentsID, iDeliveryDate, iJSONOrderProperties, iJSONOrderDefaults, iJSONProductDefaults, FDQuery_Product_ByGTIN );
                    AddProduct( FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ID ).AsInteger, iJSONProductDefaults, FDQuery_Product_ByGTIN );
      //            iJSONValue := iJSONProductDefaults.FindValue( 'R012' );
      //            if iJSONValue <> nil then Memo_Log.Lines.Add( '    * R012 '+ iJSONValue.Value );
                  end else Memo_Log.Lines.Add( ' ---> PRODUCT by GTIN was not found! FDQuery_Product_ByGTIN.RecordCount '+ IntToStr( FDQuery_Product_ByGTIN.RecordCount ));
                end else Memo_Log.Lines.Add( ' ---> R090 Delivery GTIN VALUE was not found!' );
              end else Memo_Log.Lines.Add( 'iJSONProductDefaults = nil' );
            end;(* for iProducts *)

          finally
            FDQuery_OrderRows_ByOrdersPaymentId.ApplyUpdates( 0 );
            FDQuery_OrderRows_ByOrdersPaymentId.CommitUpdates;
          end;
        end else Memo_Log.Lines.Add( 'iJSONProductArray = nil' );

      end else Memo_Log.Lines.Add( 'iJSONOrderDefaults = nil' );
    end else Memo_Log.Lines.Add( 'iJSONOrderProperty = nil' );

  finally
    with FDQuery_UniqueEntities_Only10Rows do if active then close;
    with FDQuery_UserIDandUniqueEntityID_ByExternalID2 do if active then close;
    with FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers do if active then close;
    with FDQuery_Payments_ByExternalID do if active then close;
    with FDQuery_Orders_ByPaymentID do if active then close;
    with FDQuery_OrderRows_ByOrdersPaymentID do if active then close;
  end;
end; (* CreateOrUpdateOrder *)

function Twindow_Server.CreateCustomerIfNeeded( aJSONOrderDefaults : TJSONObject ) : integer;

  procedure __UpdateUniqueEntity;
  begin
    FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_NAME ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryFirstName ) + ' ' +
                                                                                     SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryLastName );
    FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_EXTERNALCUSTOMERGROUPID ).AsInteger := 0;
    FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_ADDRESS ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressStreet );
    FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_POSTNUMBER ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressPostalCode );
    FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_POSTOFFICE ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressCity );
    FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_PHONENUMBERFORDELIVERY ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryPhone );

    with FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_ENTITYTYPE ) do if IsNull then AsInteger := ENTITYandUSERtype_PRIVATECUSTOMER;
    with FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_CREATEDATE ) do if IsNull then AsDateTime := Date;
    FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_MODIFIEDDATE ).AsDateTime := Date;
  end;(* __UpdateUniqueEntity *)

  procedure __UpdateUser;
  var
    iItems : TStringList;
  begin
    with FDQuery_Users_Only10Rows.FieldByName( USERS_USERTYPE ) do if IsNull then AsInteger := ENTITYandUSERtype_PRIVATECUSTOMER;
    FDQuery_Users_Only10Rows.FieldByName( USERS_FIRSTNAME ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryFirstName );
    FDQuery_Users_Only10Rows.FieldByName( USERS_LASTNAME ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryLastName );
    FDQuery_Users_Only10Rows.FieldByName( USERS_EMAIL ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryEmail );
    FDQuery_Users_Only10Rows.FieldByName( USERS_MOBILEPHONE ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryPhone );
    with FDQuery_Users_Only10Rows.FieldByName( USERS_CREATEDATE ) do if IsNull then AsDateTime := Date;
    FDQuery_Users_Only10Rows.FieldByName( USERS_MODIFIEDDATE ).AsDateTime := Date;
  end;(* __UpdateUser *)

begin
  result := 0;

  (* is customer already added? *)
  FDQuery_UserIDandUniqueEntityID_ByExternalID2.ParamByName( 'paramEXTERNALGUID' ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_UserGuidCode );
  FDQuery_UserIDandUniqueEntityID_ByExternalID2.open;
  if FDQuery_UserIDandUniqueEntityID_ByExternalID2.recordcount = 0 then
  begin
    (* Customer does not exists -> add *****************************************)
    FDQuery_UniqueEntities_Only10Rows.open;
    FDQuery_UniqueEntities_Only10Rows.Append;
    __UpdateUniqueEntity;
    FDQuery_UniqueEntities_Only10Rows.Post;
    FDQuery_UniqueEntities_Only10Rows.ApplyUpdates( 0 );
    FDQuery_UniqueEntities_Only10Rows.CommitUpdates;
    Memo_Log.Lines.Add( '-UniqueEntityID '+ FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_ID ).AsString );
    result := FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_ID ).AsInteger;

    FDQuery_Users_Only10Rows.open;
    FDQuery_Users_Only10Rows.Append;
    FDQuery_Users_Only10Rows.FieldByName( USERS_UNIQUEENTITYID ).AsInteger := FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_ID ).AsInteger;
    FDQuery_Users_Only10Rows.FieldByName( USERS_EXTERNALGUID ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_UserGuidCode );
    FDQuery_Users_Only10Rows.FieldByName( USERS_EXTERNALID2 ).AsInteger := 1; // workaround for Syd�npuu to be detected as a valid customer since ExternalGUID is not yet in server/client implementation.
    __UpdateUser;
    FDQuery_Users_Only10Rows.Post;
    FDQuery_Users_Only10Rows.ApplyUpdates( 0 );
    FDQuery_Users_Only10Rows.CommitUpdates;
  end else
  begin
    //SiMain.LogDebug( 'Updating customer "'+ IntToStr( iCustomer.ExternalID ) + '"' );

    (* customer already exists -> update *****************************************************)
    FDQuery_UniqueEntities_Only10Rows.ParamByName( 'paramID' ).AsInteger := FDQuery_UserIDandUniqueEntityID_ByExternalID2.FieldByName( USERS_UNIQUEENTITYID ).AsInteger;
    FDQuery_UniqueEntities_Only10Rows.open;

    if FDQuery_UniqueEntities_Only10Rows.recordcount = 1 then
    begin
      FDQuery_UniqueEntities_Only10Rows.edit;
      __UpdateUniqueEntity;
      FDQuery_UniqueEntities_Only10Rows.Post;
      FDQuery_UniqueEntities_Only10Rows.ApplyUpdates( 0 );
      FDQuery_UniqueEntities_Only10Rows.CommitUpdates;
      Memo_Log.Lines.Add( '-UniqueEntityID '+ FDQuery_UserIDandUniqueEntityID_ByExternalID2.FieldByName( USERS_UNIQUEENTITYID ).AsString );
      result := FDQuery_UserIDandUniqueEntityID_ByExternalID2.FieldByName( USERS_UNIQUEENTITYID ).AsInteger;
    end else
    begin
      //SiMain.LogError( 'UniqueEntity '+ FDQuery_UserIDandUniqueEntityID_ByExternalID2.FieldByName( USERS_UNIQUEENTITYID ).AsString +' was not found!');
      //jat iProblemOccurredRetryLater := true;
    end;

    FDQuery_Users_Only10Rows.ParamByName( 'paramID' ).AsInteger := FDQuery_UserIDandUniqueEntityID_ByExternalID2.FieldByName( USERS_ID ).AsInteger;
    FDQuery_Users_Only10Rows.open;
    if FDQuery_Users_Only10Rows.recordcount = 1 then
    begin
      FDQuery_Users_Only10Rows.edit;
      __UpdateUser;
      FDQuery_Users_Only10Rows.Post;
      FDQuery_Users_Only10Rows.ApplyUpdates( 0 );
      FDQuery_Users_Only10Rows.CommitUpdates;
    end else
    begin
      //SiMain.LogError( 'User '+ FDQuery_UserIDandUniqueEntityID_ByExternalID2.FieldByName( USERS_ID ).AsString +' was not found!');
      //jat iProblemOccurredRetryLater := true;
    end;
  end;(* FDQuery_UserIDandUniqueEntityID_ByExternalID2.recordcount *)

end;(* CreateCustomerIfNeeded *)

function Twindow_Server.CreateDeliveryIfNeeded( const aUniqueEntityID : integer; aJSONOrderDefaults : TJSONObject; var aDeliveryDate : TDateTime; const aOrderStatus : word ) : integer;
var
  { TODO 2 -ctodo : replace these with json values }
  iBufferString : string;
  iDeliveryTime, iDateTimeBuffer : TDateTime;
  iDeliverySlotLengthInHours : integer;
  iDeliveryMethodType : byte;
begin
  (* set local and call variables, that will be used many times below *)
  iDeliveryMethodType := SafeAsDeliveryAreaType( aJSONOrderDefaults, Storeverse_Order_DeliveryMethodCode );

  case iDeliveryMethodType of
    DMDELIVERYAREAS_DELIVERYAREATYPE_POSTNUMBER : (* collico *)
      begin
        iBufferString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_ExpectedDeliveryTimeStart );
        if  ( length( iBufferString ) = 0 ) or ( iBufferString = 'null' )  then
        begin
          aDeliveryDate := IncDay( Today, 2 );
          iDeliveryTime := EncodeTime( 12, 0, 0, 0 );
          iDeliverySlotLengthInHours := 4;
          Memo_Log.Lines.Add(' ! "'+ Storeverse_Order_ExpectedDeliveryTimeStart + '" value is missing, using day after tomorrow instead. DeliveryDate = ' + DateToStr( aDeliveryDate ));
          { TODO 2 -cExceptions : send SMS or email exception! }
        end else
        begin
          (* get Delivery Date and starting time from Storeverse_Order_ExpectedDeliveryTimeStart *)
          iDateTimeBuffer := StoreverseDateTimeStampAsDateTime( iBufferString );
          aDeliveryDate := DateOf( iDateTimeBuffer );
          iDeliveryTime := TimeOf( iDateTimeBuffer );

          (* Delivery slot length *)
          iBufferString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_ExpectedDeliveryTimeEnd );
          if  ( length( iBufferString ) = 0 ) or ( iBufferString = 'null' )  then
          begin
            iDeliverySlotLengthInHours := 4;
            Memo_Log.Lines.Add(' ! "'+ Storeverse_Order_ExpectedDeliveryTimeEnd + '" value is missing, using default 4 hours for delivery slot length. ');
            { TODO 2 -cExceptions : send SMS or email exception! }
          end else
          begin
            (* calculate delivery slot length *)
            iDateTimeBuffer := TimeOf( StoreverseDateTimeStampAsDateTime( iBufferString ));
            iDeliverySlotLengthInHours := HoursBetween( iDeliveryTime, iDateTimeBuffer );
          end;
        end;
      end;(* DMDELIVERYAREAS_DELIVERYAREATYPE_POSTNUMBER *)

    DMDELIVERYAREAS_DELIVERYAREATYPE_SMARTSHIP_POSTPICKUP, DMDELIVERYAREAS_DELIVERYAREATYPE_SMARTSHIP_HOMEDELIVERY :
      begin
        (* Set default values for these *)
        iDeliveryTime := EncodeTime( 12, 0, 0, 0 );
        iDeliverySlotLengthInHours := 4;

        iBufferString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_ExpectedDeliveryDate );
        if  ( length( iBufferString ) = 0 ) or ( iBufferString = 'null' )  then
        begin
          aDeliveryDate := IncDay( Today, 2 );
          Memo_Log.Lines.Add(' ! "'+ Storeverse_Order_ExpectedDeliveryDate + '" value is missing, using day after tomorrow instead. DeliveryDate = ' + DateToStr( aDeliveryDate ));
          { TODO 2 -cExceptions : send SMS or email exception! }
        end else
        begin
          (* get Delivery Date and starting time from Storeverse_Order_ExpectedDeliveryTimeStart *)
          iDateTimeBuffer := StoreverseDateTimeStampAsDateTime( iBufferString );
          aDeliveryDate := DateOf( iDateTimeBuffer );
        end;
      end;(* DMDELIVERYAREAS_DELIVERYAREATYPE_SMARTSHIP_POSTPICKUP, DMDELIVERYAREAS_DELIVERYAREATYPE_SMARTSHIP_HOMEDELIVERY *)

    else
      begin
        AddToOverallLog( 'Unknown iDeliveryMethodType = "'+ IntToStr( iDeliveryMethodType ) + '"');
      end;
  end;(* case *)

  (* prepare customerdelivery *)
  with FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers do if active then close;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.parambyname( 'paramuniqueentityid' ).asinteger := aUniqueEntityID;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.parambyname( 'paramdeliveryaddress' ).asstring := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressStreet );
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.parambyname( 'paramdeliverydate' ).asdatetime := adeliverydate;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.parambyname( 'paramdeliverytime' ).asdatetime := iDeliveryTime;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.parambyname( 'paramdeliveryslotlengthinhours' ).asinteger := iDeliverySlotLengthInHours;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.parambyname( 'parampickuplocationid' ).asinteger := 0;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.parambyname( 'paramdeliveryareatype' ).asinteger := iDeliveryMethodType;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.open;

  (* add or edit customerdelivery record *)
  if FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.recordCount = 0 then
  begin
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.Append;
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYSTATE ).AsInteger := CUSTOMERDELIVERIES_DELIVERYSTATE_WAITING;
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELETED ).AsInteger := 0;
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYID ).AsInteger := 0;
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_PICKUPLOCATIONID ).AsInteger := 0;
  end else
  begin
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.edit;

    if aOrderStatus = Storeverse_Order_OrderStatus_OrderCancelled then
    begin
      AddToOverallLog( '-->> Marking delivery cancelled' );
      FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELETED ).AsInteger := 1;
      FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYSTATE ).AsInteger := CUSTOMERDELIVERIES_DELIVERYSTATE_DELIVERYFAILED;
      { TODO 1 -ctodo : how to take care of double orders for same delivery? }
    end;
    //SiMain.LogDateTime( 'Using existing customerdelivery with deliverydate', FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYDATE ).AsDateTime );
  end;

  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_UNIQUEENTITYID ).Asinteger := aUniqueEntityID;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_MODIFIEDDATETIME ).AsDateTime := Now;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYDATE ).AsDateTime := aDeliveryDate;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYTIME ).AsDateTime := iDeliveryTime;
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYSLOTLENGTHINHOURS ).AsInteger := iDeliverySlotLengthInHours;

  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYAREATYPE ).AsInteger := iDeliveryMethodType;


  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYCOMPANY ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryFirstName ) + ' ' +
                                                                                                                              SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryLastName );
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYADDRESS ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressStreet );
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYPOSTNUMBER ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressPostalCode );
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYPOSTOFFICE ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressCity );
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYPHONENUMBER ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryPhone );
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYPHONENUMBER2 ).AsString := ''; { TODO 2 -cthink : Storeverse has currently only one phonenumber }
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYDOORCODE ).AsString := ''; { TODO 1 -ctodo : Missing doorcode }
  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_DELIVERYADDRESSCOMMENTS ).AsString := ''; { TODO 1 -ctodo : Missing ajo-ohje }

  FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.Post;

  if FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.ChangeCount > 0 then
  begin
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.ApplyUpdates( 0 );
    FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.CommitUpdates;
  end;

  result := FDQuery_CustomerDeliveries_ByDeliveryDateTimeAddressAndOthers.FieldByName( CUSTOMERDELIVERIES_ID ).AsInteger;
end;(* CreateDeliveryIfNeeded *)

function Twindow_Server.CreatePaymentIfNeeded( const aUniqueEntityID, aCustomerDeliveryID : integer; aJSONOrderProperties, aJSONOrderDefaults : TJSONObject; const aOrderStatus : word ) : integer;
begin
  with FDQuery_Payments_ByExternalID do if active then close;
  FDQuery_Payments_ByExternalID.ParamByName( 'paramEXTERNALID' ).AsInteger := StrToIntDef( SafeJSONString( aJSONOrderDefaults, Storeverse_Order_OrderID ), -1 );
  { TODO 2 -cErrors : Handle -1 value }
  FDQuery_Payments_ByExternalID.open;

  case FDQuery_Payments_ByExternalID.RecordCount of
    0 : begin
          FDQuery_Payments_ByExternalID.Append;
          FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_EXTERNALID ).AsInteger := StrToIntDef( SafeJSONString( aJSONOrderDefaults, Storeverse_Order_OrderID ), -1 );
        end;
    1 : begin
          FDQuery_Payments_ByExternalID.Edit;

          if aOrderStatus = Storeverse_Order_OrderStatus_OrderCancelled then
          begin
            AddToOverallLog( '-->> Marking payment cancelled' );
            FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_DELETED ).AsInteger := PAYMENTS_DELETED_DELETED;

            AddToOverallLog( '-->> Marking orders cancelled' );
            if FDQuery_Orders_ByPaymentID.IndexFieldNames <> ORDERS_PROVIDERID then FDQuery_Orders_ByPaymentID.IndexFieldNames := ORDERS_PROVIDERID;
            with FDQuery_Orders_ByPaymentID do
            begin
              if ( not Active ) or ( FDQuery_Orders_ByPaymentID.ParamByName( 'paramPAYMENTID' ).AsInteger <> FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_ID ).AsInteger ) then
              begin
                with FDQuery_Orders_ByPaymentID do if active then close;
                FDQuery_Orders_ByPaymentID.ParamByName( 'paramPAYMENTID' ).AsInteger := FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_ID ).AsInteger;
                FDQuery_Orders_ByPaymentID.Open;
              end;

              First;
              while not eof do
              begin
                Edit;
                FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ORDERSTATE ).AsInteger := ORDERSTATE_DELETED;
                Post;

                next;
              end;

              if FDQuery_Orders_ByPaymentID.ChangeCount > 0 then
              begin
                FDQuery_Orders_ByPaymentID.ApplyUpdates( 0 );
                FDQuery_Orders_ByPaymentID.CommitUpdates;
              end;

            end;(* with orders... *)
          end;
        end;
    else
      { TODO 2 -cErrors : create better error handling }
      Memo_Log.Lines.Add( 'Critical FDQuery_Payments_ByExternalID.RecordCount response '+ inttostr( FDQuery_Payments_ByExternalID.RecordCount ));
  end; (* case *)

  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_UNIQUEENTITYID ).AsInteger := aUniqueEntityID;
  SafeAsDateTime( FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_CREATED ), aJSONOrderProperties, Storeverse_Order_created_at );
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_CUSTOMERDELIVERYID ).AsInteger := aCustomerDeliveryID;

  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_TOTALPRICE ).AsFloat := SafeAsFloatDef( aJSONOrderDefaults, Storeverse_Order_OrderTotal, 0 );
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_TOTALPRICEDISCOUNT ).AsFloat := SafeAsFloatDef( aJSONOrderDefaults, Storeverse_Order_DiscountAtCheckout, 0 );
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_PAIDWITHGIFTCARDS ).AsFloat := 0; { TODO 3 -cthink : implement giftcards? }
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_PAIDWITHKH24CREDIT ).AsFloat := 0; { TODO 3 -cthink : implement kh24 credit? }

  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_DELIVERYPRICE ).AsFloat := SafeAsFloatDef( aJSONOrderDefaults, Storeverse_Order_DeliveryCost, 0 );
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_DELIVERYPRICEDISCOUNT ).AsFloat := 0; { TODO 3 -cthink : implement deliveryprice discount? }

  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_COMMENTSFROMCUSTOMER ).AsString := ''; { TODO 2 -cthink : implement comments from customer? }
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_PAYMENTSTATE ).AsInteger := PAYMENTSTATE_FULLYPAID;
  FDQuery_Payments_ByExternalID.FieldByName( ORDERS_PAYMENTGATEWAYID ).AsInteger := 1;
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_PAYMENTTYPE ).AsInteger := PAYMENTTYPE_MAKSUKAISTA;

  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_INVOICECOMPANYNAME ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_CustomerFirstName ) + ' ' +
                                                                                       SafeJSONString( aJSONOrderDefaults, Storeverse_Order_CustomerLastName );
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_INVOICEADDRESS ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_CustomerAddressStreet );
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_INVOICEPOSTNUMBER ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_CustomerAddressPostalCode );
  FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_INVOICEPOSTOFFICE ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_CustomerAddressCity );

  FDQuery_Payments_ByExternalID.post;

  (* MASTER-DETAIL-NEGATIVE-ID BUGFIX *)
  if FDQuery_Payments_ByExternalID.ChangeCount > 0 then
  begin
    //SiMain.LogInteger( 'before apply FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_ID ).AsInteger', FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_ID ).AsInteger );
    FDQuery_Payments_ByExternalID.ApplyUpdates( 0 );
    FDQuery_Payments_ByExternalID.CommitUpdates;
    //SiMain.LogInteger( 'FDQuery_Payments_ByExternalID.RecordCount', FDQuery_Payments_ByExternalID.RecordCount );
    //SiMain.LogInteger( 'after apply FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_ID ).AsInteger', FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_ID ).AsInteger );
    Memo_Log.Lines.Add( 'PaymentID '+ FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_ID ).AsString );
    result := FDQuery_Payments_ByExternalID.FieldByName( PAYMENTS_ID ).AsInteger;
  end;
end;(* CreatePaymentIfNeeded *)

procedure Twindow_Server.FormCreate(Sender: TObject);

  function _SafeReadAndStoreDefaultStringValue( aIniFile : TIniFile; const aSection, aName, aDefaultValue : string ) : string;
  begin
    result := aIniFile.ReadString( aSection, aName, '' );
    if length( result ) = 0 then
    begin
      result := aDefaultValue;
      aIniFile.WriteString( aSection, aName, aDefaultValue );
    end;
  end;(* _SafeReadAndStoreDefaultStringValue *)

begin
  FTimer_Products := EncodeDate( 2000, 1, 1 );
  FTimer_OrdersPreFetch := EncodeDate( 2000, 1, 1 );
  FTimer_OrdersFetch := EncodeDate( 2000, 1, 1 );

  Label_InstanceInfo.Caption := IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName ));

  Memo_Log.Lines.Add( 'Opening inifile "'+ IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + gIniFileName + '"' );
  FIniFile := TIniFile.create( IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + gIniFileName );

  Memo_Log.Lines.Add( 'Storeverse API url "'+ _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gStoreverseAPIUrl, 'https://staging-core.lafka.tools/api/services/' ) + '"' );
  Memo_Log.Lines.Add( 'Storeverse API token "'+ _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gStoreverseAPIToken, 'SWw3a63A6phc9291SgUJTUcmaBaUcKjgkOAeKNGfztIMj7NNFsjQqBfKubwdkf' ) + '"' );
  Memo_Log.Lines.Add( 'Storeverse Username "'+ _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gStoreverseUsername, 'janne.timmerbacka@kauppahalli24.fi' ) + '"' );

  Memo_Log.Lines.Add( 'Setting up main database "'+ _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gHeartwoodDBFullFileName, 'C:\Servers\Luomuntarina\HEARTWOOD.FDB' ) + '"' );
  FDConnection.Params.Database := _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gHeartwoodDBFullFileName, 'C:\Servers\Luomuntarina\HEARTWOOD.FDB' );
  Memo_Log.Lines.Add( 'Setting up source database "'+ _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gHeartwoodSourceDBFullFileName, 'C:\Servers\Kauppahalli24\HEARTWOOD.FDB' ) + '"' );
  FDConnection_KH24.Params.Database := _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gHeartwoodSourceDBFullFileName, 'C:\Servers\Kauppahalli24\HEARTWOOD.FDB' );

  SaveLog( gLogStartupRoutines );
end;(* FormCreate *)

procedure Twindow_Server.FormDestroy(Sender: TObject);
begin
  FreeAndNil( FIniFile );
end;(* FormDestroy *)

procedure Twindow_Server.AddProviderOrderIfNeeded( const aUniqueEntityID, aPaymentID : Integer; aDeliveryDate : TDateTime; aJSONOrderProperties, aJSONOrderDefaults, iJSONProductDefaults : TJSONObject; aProduct_ByGTIN : TFDQuery );
begin
  if FDQuery_Orders_ByPaymentID.IndexFieldNames <> ORDERS_PROVIDERID then FDQuery_Orders_ByPaymentID.IndexFieldNames := ORDERS_PROVIDERID;

  (* Set orders parameters and open *)
  with FDQuery_Orders_ByPaymentID do
    if ( not Active ) or ( FDQuery_Orders_ByPaymentID.ParamByName( 'paramPAYMENTID' ).AsInteger <> aPaymentID ) then
    begin
      with FDQuery_Orders_ByPaymentID do if active then close;
      FDQuery_Orders_ByPaymentID.ParamByName( 'paramPAYMENTID' ).AsInteger := aPaymentID;
      FDQuery_Orders_ByPaymentID.Open;
    end;

  if not FDQuery_Orders_ByPaymentID.FindKey([ aProduct_ByGTIN.FieldByName( PRODUCTS_PROVIDERID ).AsInteger ]) then
  begin
    Memo_Log.Lines.Add( ' - New order for PROVIDERID '+ inttostr( aProduct_ByGTIN.FieldByName( PRODUCTS_PROVIDERID ).AsInteger ));
    FDQuery_Orders_ByPaymentID.Append;
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_PAYMENTID ).AsInteger := aPaymentID;
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_PROVIDERID ).AsInteger := aProduct_ByGTIN.FieldByName( PRODUCTS_PROVIDERID ).AsInteger;
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_UNIQUEENTITYID ).AsInteger := aUniqueEntityID;
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ORDERBYUSERID ).AsInteger := 0; { TODO 2 -ctodo : onko ORDERS_ORDERBYUSERID tarpeen? }
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ORDERSTATE ).AsInteger := ORDERSTATE_ORDERED;
    SafeAsDateTime( FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ORDERDATE ), aJSONOrderProperties, Storeverse_Order_created_at );
    SafeAsDateTime( FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ORDERTIME ), aJSONOrderProperties, Storeverse_Order_created_at );
//    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ORDERDATE ).AsDateTime := DateOf( Now );
//    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ORDERTIME ).AsDateTime := TimeOf( Now );
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_DELIVERYDATE ).AsDateTime := aDeliveryDate;
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_DELIVERYID ).AsInteger := 0;
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_PAYMENTSTATE ).AsInteger := PAYMENTSTATE_UNPAID;

    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_PAYMENTTYPE ).AsInteger := PAYMENTTYPE_INVOICE;

    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_PAYMENTGATEWAYID ).AsInteger := 1;
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_MODIFIED ).AsDateTime := Now;
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_COMMENTSTOPROVIDER ).AsString := '';

    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_DELIVERYCOMPANY ).AsString :=  SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryFirstName ) + ' ' +
                                                                                  SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryLastName );
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_DELIVERYADDRESS ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressStreet );
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_DELIVERYPOSTNUMBER ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressPostalCode );
    FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_DELIVERYPOSTOFFICE ).AsString := SafeJSONString( aJSONOrderDefaults, Storeverse_Order_DeliveryAddressCity );

    FDQuery_Orders_ByPaymentID.Post;

    if FDQuery_Orders_ByPaymentID.ChangeCount > 0 then
    begin
      FDQuery_Orders_ByPaymentID.ApplyUpdates( 0 );
//      Memo_Log.Lines.Add( '    - OrderID '+ FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ID ).AsString );
      FDQuery_Orders_ByPaymentID.CommitUpdates;
//      Memo_Log.Lines.Add( '      - OrderID '+ FDQuery_Orders_ByPaymentID.FieldByName( ORDERS_ID ).AsString );
    end;(* count *)
  end; (* findkey *)
end;(* AddProviderOrderIfNeeded *)

procedure Twindow_Server.btn_GetModifiedOrdersClick(Sender: TObject);
begin
  RequestModifiedOrders;
end;

procedure Twindow_Server.btn_GetModifiedProductsClick(Sender: TObject);
begin
  RequestModifiedProducts;
end;

procedure Twindow_Server.btn_TestAutoincClick(Sender: TObject);
begin
//  FDQuery_UniqueEntities_Only10Rows.CachedUpdates := true;
//
//  DataSource1.DataSet := FDQuery_UniqueEntities_Only10Rows;
//  FDQuery_UniqueEntities_Only10Rows.open;
//  FDQuery_UniqueEntities_Only10Rows.Append;
//
//  FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_NAME ).AsString := 'Testiii';
//  Memo_Log.Lines.Add( FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_ID ).AsString );
//
//  FDQuery_UniqueEntities_Only10Rows.Post;
//  FDQuery_UniqueEntities_Only10Rows.ApplyUpdates( 0 );
//  FDQuery_UniqueEntities_Only10Rows.CommitUpdates;
//
//  Memo_Log.Lines.Add( FDQuery_UniqueEntities_Only10Rows.FieldByName( UNIQUEENTITIES_ID ).AsString );
end;(* AddProviderOrderIfNeeded *)

procedure Twindow_Server.AddProduct( const aOrderID : integer; iJSONProductDefaults : TJSONObject; aProduct_ByGTIN : TFDQuery );
var
  iJSONValue: TJSonValue;
begin
  FDQuery_OrderRows_ByOrdersPaymentID.SetRange([ aProduct_ByGTIN.FieldByName( PRODUCTS_ID ).AsInteger ], [ aProduct_ByGTIN.FieldByName( PRODUCTS_ID ).AsInteger ]);
  try
    case FDQuery_OrderRows_ByOrdersPaymentId.RecordCount of
      0 : begin (* Add new OrderRow *)
            FDQuery_OrderRows_ByOrdersPaymentId.Append;
            Memo_Log.Lines.Add( '    Appending');
          end;

      1 : begin (* Update OrderRow *)
            FDQuery_OrderRows_ByOrdersPaymentId.edit;
            Memo_Log.Lines.Add( '    Editing');
          end

      else
      begin
        Memo_Log.Lines.Add( '! AddProduct: Invalid product count = '+ IntToStr( FDQuery_OrderRows_ByOrdersPaymentId.RecordCount ) + '! Exiting.' );
        { TODO 1 -ctodo : should there be a retry }
        exit;
      end;
    end;(* case *)

    (* add/update row data *)
    FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_ORDERID ).AsInteger := aOrderID;
    FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_PRODUCTID ).AsInteger := aProduct_ByGTIN.FieldByName( PRODUCTS_ID ).AsInteger;
    FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_PRODUCTNAME ).AsString := aProduct_ByGTIN.FieldByName( PRODUCTS_PRODUCTNAME ).AsString;
    FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_VAT ).AsFloat := aProduct_ByGTIN.FieldByName( PRODUCTS_VAT ).AsFloat;

    iJSONValue := iJSONProductDefaults.FindValue( Storeverse_OrderRow_RowID );
    if iJSONValue <> nil then FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_EXTERNALID ).AsInteger := StrToIntDef( iJSONValue.Value, 0 );
    iJSONValue := iJSONProductDefaults.FindValue( Storeverse_OrderRow_Amount ); //Amount	R020
    if iJSONValue <> nil then FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_ORDERAMOUNT ).AsInteger := StrToIntDef( iJSONValue.Value, 0 );

    { TODO 2 -cselvit� : Voiko tilauksella olla rivikohtaista alennusta? }
    FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_DISCOUNT ).AsFloat := 0;

    FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_PRICE ).AsFloat := SafeAsFloatDef( iJSONProductDefaults, Storeverse_OrderRow_ActualSalesPrice, 0 );
    FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_PURCHASEPRICE ).AsFloat := SafeAsFloatDef( iJSONProductDefaults, Storeverse_OrderRow_PurchasePrice, 0 );
    FDQuery_OrderRows_ByOrdersPaymentId.FieldByName( ORDERROWS_COLLECTNOTES ).AsString := '';
    FDQuery_OrderRows_ByOrdersPaymentId.Post;

    { TODO 3 -cthink : How to detect if row is removed? }

  finally
    FDQuery_OrderRows_ByOrdersPaymentId.CancelRange;
  end;(* try *)
end;(* AddProduct *)

procedure Twindow_Server.Button1Click(Sender: TObject);
begin
//  FDConnection.Params.Database := 'C:\Servers\Luomuntarina\HEARTWOOD.FDB';
//  FDQuery_Products.Active := true;
end;

end.
