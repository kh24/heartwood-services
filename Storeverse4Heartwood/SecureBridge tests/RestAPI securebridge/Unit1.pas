unit Unit1;

interface

uses
  schttp,
  system.json,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, ScBridge, ScSSLClient;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    btn_StoreverseHWCall: TButton;
    btn_GetModifiedOrders: TButton;
    btn_SV_GetModifiedProducts: TButton;
    ScSSLClient1: TScSSLClient;
    ScHttpWebRequest1: TScHttpWebRequest;
    procedure Button1Click(Sender: TObject);
    procedure btn_StoreverseHWCallClick(Sender: TObject);
    procedure btn_GetModifiedOrdersClick(Sender: TObject);
    procedure btn_SV_GetModifiedProductsClick(Sender: TObject);
  private
    procedure DebugValue( aJSONObject : TJSONObject; const aFieldName : string );
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.DebugValue( aJSONObject : TJSONObject; const aFieldName : string );
var
  iJSONValue: TJSonValue;
begin
  iJSONValue := aJSONObject.FindValue( aFieldName );
  if iJSONValue <> nil then
    Memo1.Lines.Add( aFieldName + ' = "'+ iJSONValue.Value + '"' )
  else
    Memo1.Lines.Add( aFieldName + ' = <not found>' );
end;(* DebugValue *)

procedure TForm1.btn_GetModifiedOrdersClick(Sender: TObject);
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iResponseStr : string;
  iJSONOrderArray, iJSONProductArray: TJsonArray;
  iJSONOrderProperty, iJSONOrderDefaults,
  iJSONProductDefaults, iJSONProductReference, iJSONProductReferenceDefaults : TJSONObject;
  iJSONValue: TJSonValue;
  iOrders, iProducts : Integer;
begin
  (**) Memo1.Lines.Clear;
  iRequest := TScHttpWebRequest.Create( 'http://staging-core.lafka.tools/api/services/heartwood/order?'+
                                        'X-Token=SWw3a63A6phc9291SgUJTUcmaBaUcKjgkOAeKNGfztIMj7NNFsjQqBfKubwdkf&filter=status=1&filter=currency=EUR&limit=2');
  try
//    iRequest.Credentials.UserName := 'janne@storeverse.io';
//    iRequest.Credentials.Password := 'H^?a7H+?mWD=';
    iRequest.Credentials.UserName := 'janne.timmerbacka@kauppahalli24.fi';
    iRequest.Credentials.Password := '_!S4f_Fr0NN';

    try
      iResponse := iRequest.GetResponse;
      iResponseStr := iResponse.ReadAsString;

//      (**) Memo1.Lines.Add( iResponseStr );
//      (**) Memo1.Lines.Add( '' );

      if Length( iResponseStr ) > 2 then
      //if Request.IsSecure then
      begin
        iJSONOrderArray := TJSONObject.ParseJSONValue( iResponseStr ) as TJSONArray;
        if iJSONOrderArray <> nil then
        begin
          try
            (* ORDERS *************************************************)
            (**) Memo1.Lines.Add( 'Order count '+ inttostr( iJSONOrderArray.Count ));
            if iJSONOrderArray.Count > 0 then
            for iOrders := 0 to pred( iJSONOrderArray.Count ) do
            begin
              iJSONValue := iJSONOrderArray.Items[ iOrders ].FindValue( 'id' );
              if iJSONValue <> nil then Memo1.Lines.Add( 'Order ['+ inttostr( iOrders ) +'] ID '+ iJSONValue.Value );

              iJSONOrderProperty := iJSONOrderArray.Items[ iOrders ].FindValue('properties') as TJSONObject;
              if iJSONOrderProperty <> nil then
              begin
                iJSONValue := iJSONOrderProperty.FindValue( 'created_at' );
                if iJSONValue <> nil then Memo1.Lines.Add( ' created_at '+ iJSONValue.Value );

                iJSONOrderDefaults := iJSONOrderProperty.FindValue('default') as TJSONObject;
                if iJSONOrderDefaults <> nil then
                begin
                  iJSONValue := iJSONOrderDefaults.FindValue( 'O022' );
                  if iJSONValue <> nil then Memo1.Lines.Add( ' O022 '+ iJSONValue.Value );

                  (* ORDER PRODUCTS **************************************************************)
                  iJSONProductArray := iJSONOrderProperty.FindValue( 'products' ) as TJSONArray;
                  if iJSONProductArray <> nil then
                  begin

                    for iProducts := 0 to pred( iJSONProductArray.Count ) do
                    begin
                      iJSONValue := iJSONProductArray.Items[ iProducts ].FindValue( 'product_id' );
                      if iJSONValue <> nil then Memo1.Lines.Add( '  - product_id ['+ inttostr( iProducts ) +'] '+ iJSONValue.Value );

                      iJSONProductDefaults := iJSONProductArray.Items[ iProducts ].FindValue('default') as TJSONObject;
                      if iJSONProductDefaults <> nil then
                      begin
                        iJSONValue := iJSONProductDefaults.FindValue( 'R012' );
                        if iJSONValue <> nil then Memo1.Lines.Add( '    * R012 '+ iJSONValue.Value );

                        iJSONProductReferenceDefaults := iJSONProductArray.Items[ iProducts ].FindValue('product_reference.default') as TJSONObject;
                        if iJSONProductReferenceDefaults <> nil then
                        begin
                          iJSONValue := iJSONProductReferenceDefaults.FindValue( 'F002' );
                          if iJSONValue <> nil then Memo1.Lines.Add( '    * F002 '+ iJSONValue.Value );

                        end else OutputDebugString( PCHAR( 'iJSONProductReferenceDefaults = nil' ));
                      end else OutputDebugString( PCHAR( 'iJSONProductDefaults = nil' ));
                    end;(* for iProducts *)
                  end else OutputDebugString( PCHAR( 'iJSONProductArray = nil' ));
                end else OutputDebugString( PCHAR( 'iJSONOrderDefaults = nil' ));
              end else OutputDebugString( PCHAR( 'iJSONOrderProperty = nil' ));
            end;(* for iOrders *)

            OutputDebugString( PCHAR( 'All good!' ));

          finally
            FreeAndNil( iJSONOrderArray );
          end;
        end else (* <> nil *)
          OutputDebugString( PCHAR( 'iJSONRootObject was empty "'+ iResponseStr + '"' ));
      end else
        OutputDebugString( PCHAR( 'Https request response was empty' ));

    finally
      //causes an exception FreeAndNil( iResponse );
    end;

  finally
    //iRequest.Disconnect;
    FreeAndNil( iRequest );
  end;
end;

procedure TForm1.btn_StoreverseHWCallClick(Sender: TObject);
var
  iOrders, iProducts : Integer;
  Request: TScHttpWebRequest;
  Response: TScHttpWebResponse;
  ResponseStr: String;
  JsonRootObject : TJSONObject;
  JSonRoot, JSonValue: TJSonValue;
  JsonArray, JsonOrderArray, JsonProductArray: TJsonArray;
  iName: String;
  iUserName: String;
  iEmail: String;
begin
  Memo1.Lines.Clear;
  Request := TScHttpWebRequest.Create('http://staging-core.lafka.tools/api/services/heartwood/order?'+
             'X-Token=SWw3a63A6phc9291SgUJTUcmaBaUcKjgkOAeKNGfztIMj7NNFsjQqBfKubwdkf&filter=status=1&filter=currency=EUR&limit=4');
//  Request.Credentials.UserName := 'janne@storeverse.io';
//  Request.Credentials.Password := 'H^?a7H+?mWD=';

  Request.Credentials.UserName := 'janne.timmerbacka@kauppahalli24.fi';
  Request.Credentials.Password := '_!S4f_Fr0NN';

  try
    Response := Request.GetResponse;
    if Request.IsSecure then
    begin
      ResponseStr := Response.ReadAsString;
      Memo1.Lines.Add( ResponseStr );
      Memo1.Lines.Add( '' );

//      JsonRootObject := TJSonObject.ParseJSONValue(ResponseStr) as TJSONObject;
//      try
//        if JsonRootObject <> nil then
//        begin
//          memo1.Lines.Add( 'status "'+ JsonRootObject.GetValue<string>('status', '-' ) +'"' );
//
//          JsonOrderArray := JsonRootObject.FindValue('result') as TJSONArray;
//          if JsonOrderArray <> nil then
//          begin
//            memo1.Lines.Add( 'Orders.count = '+ inttostr( JsonOrderArray.Count ));
//            for iOrders := 0 to pred( JsonOrderArray.Count ) do
//            begin
//              JSonValue := JsonOrderArray.Items[iOrders];
//              memo1.Lines.Add( 'Order['+ inttostr( iOrders ) + '] id = "'+ JSonValue.GetValue<string>('id', '-' ) +'"' );
//
//              JsonProductArray := JSonValue.FindValue('properties.products') as TJSONArray;
//              if JsonProductArray <> nil then
//              begin
//                memo1.Lines.Add( 'Products.count = '+ inttostr( JsonProductArray.Count ));
//                for iProducts := 0 to pred( JsonProductArray.Count ) do
//                begin
//                  JSonValue := JsonProductArray.Items[iProducts];
//                  memo1.Lines.Add( 'Product['+ inttostr( iProducts ) + '] id = "'+ JSonValue.GetValue<string>('product_id', '-' ) +'"' );
//                end;
//              end;
//            end;
//
////            JsonOrderArray := jsonvalue.
////            if JsonOrderArray <> nil then
////              for i := 0 to pred( JsonOrderArray.Count ) do
////              begin
////                JSonValue := JsonOrderArray.Items[0];
////                memo1.Lines.Add( 'Order['+ inttostr( i ) + '] id = "'+ JSonValue.GetValue<string>('id', '-' ) +'"' );
////              end;
//
//          end;
//        //memo1.Lines.Add( 'status ' + JSONValue.GetValue<string>('status'));
//        end;
//      finally
//        JsonArray.Free;
//      end;
    end;

  finally
    Response.Free;
  end;
end;

procedure TForm1.btn_SV_GetModifiedProductsClick(Sender: TObject);
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iResponseStr : string;
  iJSONResponse : TJSONObject;
  iJSONProductArray: TJSONArray;
  iJSONProductProperties : TJSONObject;
  iJSONValue: TJSonValue;
  iProducts : Integer;
begin
  (**) Memo1.Lines.Clear;
  iRequest := TScHttpWebRequest.Create( 'https://staging-core.lafka.tools/api/services/product?'+
                                        'X-Token=SWw3a63A6phc9291SgUJTUcmaBaUcKjgkOAeKNGfztIMj7NNFsjQqBfKubwdkf');//&limit=2' );
                                        //+ '&updated_at=2000-08-01,2022-08-05');
  try
    iRequest.Credentials.UserName := 'janne.timmerbacka@kauppahalli24.fi';
    iRequest.Credentials.Password := '_!S4f_Fr0NN';

    try
      iResponse := iRequest.GetResponse;
      iResponseStr := iResponse.ReadAsString;

//      (**) Memo1.Lines.Add( iResponseStr );
//      (**) Memo1.Lines.Add( '' );

      if Length( iResponseStr ) > 2 then
      //if Request.IsSecure then
      begin
        iJSONResponse := TJSONObject.ParseJSONValue( iResponseStr ) as TJSONObject;
        if iJSONResponse <> nil then
        begin
          iJSONValue := iJSONResponse.FindValue( 'status' );
          if iJSONValue <> nil then Memo1.Lines.Add( 'status = '+ iJSONValue.Value );

          { TODO 2 -cthink : process meta fields
              "meta": {
                "total": 2065,
                "limit": 20,
                "skip": 0,
                "order": "C001",
                "direction": "desc"
          }
          try
            (* PRODUCTS *************************************************)
            iJSONProductArray := iJSONResponse.FindValue( 'result' ) as TJSONArray;
            if iJSONProductArray <> nil then
            begin
              (**) Memo1.Lines.Add( 'Products count '+ inttostr( iJSONProductArray.Count ));
              if iJSONProductArray.Count > 0 then
              for iProducts := 0 to pred( iJSONProductArray.Count ) do
              begin
                iJSONValue := iJSONProductArray.Items[ iProducts ].FindValue( 'id' );
                if iJSONValue <> nil then Memo1.Lines.Add( 'Product ['+ inttostr( iProducts ) +'] ID '+ iJSONValue.Value );

                iJSONProductProperties := iJSONProductArray.Items[ iProducts ].FindValue( 'properties' ) as TJSONObject;
                if iJSONProductProperties <> nil then
                begin
//                  DebugValue( iJSONProductProperties, 'A006' ); //A006 / Storage "A006": "Warm",
//                  DebugValue( iJSONProductProperties, 'A008' ); //A008 / Date tracking "A008": true,
//                  DebugValue( iJSONProductProperties, 'A010' ); //A010 / Freshness promise "A010": 60,
//                  DebugValue( iJSONProductProperties, 'A011' ); //A011 / Frozen "A011": false,
                  DebugValue( iJSONProductProperties, 'C001' ); //C001 / Product code "C001": 2063,
                  DebugValue( iJSONProductProperties, 'S004' );
                  DebugValue( iJSONProductProperties, 'S005' );

//                  DebugValue( iJSONProductProperties, 'C002' ); //C002 / Product type "C002": "PACK",
//                  DebugValue( iJSONProductProperties, 'C003' ); //C003 / Is public "C003": true,
//                  DebugValue( iJSONProductProperties, 'C004' ); //C004 / Product group "C004": 1190200,
//
//                  DebugValue( iJSONProductProperties, 'C005' ); //C005 / Internal name "C005": "ELOMENTS ORGANIC VITAMIN TEA DOUBLE LEMON LUOMUTEE 14PSS/ 28G",
//                  DebugValue( iJSONProductProperties, 'C007' ); //C007 / Has variable weight "C007": false,
//                  DebugValue( iJSONProductProperties, 'F000' ); //F000 / Product picture "F000": "https://www.kauppahalli24.fi/pub/media/catalog/product/cache/a7fa59de44ae400e652bf7c13548bd46/9/3/9352177000026.jpg",
//                  DebugValue( iJSONProductProperties, 'L005' ); //L005 / Primary label "L005": "Eloments Organic Vitamin tea",
//                  DebugValue( iJSONProductProperties, 'L006' ); //L006 / Secondary label "L006": "Double Lemontee luomu",
//                  DebugValue( iJSONProductProperties, 'L900' ); //L900 / Internal comment "L900": "",
//                  DebugValue( iJSONProductProperties, 'M002' ); //M002 / Campaign price "M002": "",
//                  DebugValue( iJSONProductProperties, 'S007' ); //S007 / Warehouse stock "S007": 11,
//                  DebugValue( iJSONProductProperties, 'S008' ); //S008 / Next supply time "S008": "",
//                  DebugValue( iJSONProductProperties, 'S009' ); //S009 / Stock is limited "S009": true,
//                  DebugValue( iJSONProductProperties, 'S900' ); //S900 / Reference key "S900": "EL0005",
//                  DebugValue( iJSONProductProperties, 'S901' ); //S901 / Reorder alert level "S901": 2,
//                  DebugValue( iJSONProductProperties, 'S902' ); //S902 / Reorder quantity "S902": 1,
//                  DebugValue( iJSONProductProperties, 'S902' ); //S902 / Supplier reference "S903": 194
//                  DebugValue( iJSONProductProperties, 'U003' ); //U003 / Gross volume "U003": "",
//                  DebugValue( iJSONProductProperties, 'U004' ); //U004 / Net volume "U004": 2,
//                  DebugValue( iJSONProductProperties, 'U005' ); //U005 / Volume title, default value = g "U005": "g",
//                  DebugValue( iJSONProductProperties, 'U007' ); //U007 / Volume conversion "U007": 1000,
//                  DebugValue( iJSONProductProperties, 'U008' ); //U008 / Delivery width "U008": 68,
//                  DebugValue( iJSONProductProperties, 'U009' ); //U009 / Delivery height "U009": 78,
//                  DebugValue( iJSONProductProperties, 'U010' ); //U010 / Delivery depth "U010": 118,
//                  DebugValue( iJSONProductProperties, 'U011' ); //U011 / Delivery weight "U011": "0,028",
//                  DebugValue( iJSONProductProperties, 'U015' ); //U015 / Minimum storage temperature "U015": 10,
//                  DebugValue( iJSONProductProperties, 'U016' ); //U016 / Maximum storage temperature "U016": 25,
                end;(* iJSONProductProperties <> nil *)
              end;(* for iProducts *)

            end else OutputDebugString( PCHAR( 'iJSONProductArray = nil' ));

          finally
            FreeAndNil( iJSONProductArray );
          end;

        end else OutputDebugString( PCHAR( 'iJSONResponse = nil' ));
      end else OutputDebugString( PCHAR( 'iJSONRootObject was empty "'+ iResponseStr + '"' ));

    finally
      //causes an exception FreeAndNil( iResponse );
    end;

  finally
    //iRequest.Disconnect;
    FreeAndNil( iRequest );
  end;
end;(* btn_SV_GetModifiedProductsClick *)

procedure TForm1.Button1Click(Sender: TObject);
var
  iOrders, iProducts : Integer;
  Request: TScHttpWebRequest;
  Response: TScHttpWebResponse;
  ResponseStr: String;
  JsonRootObject : TJSONObject;
  JSonRoot, JSonValue: TJSonValue;
  JsonArray, JsonOrderArray, JsonProductArray: TJsonArray;
  iName: String;
  iUserName: String;
  iEmail: String;
begin
{  Response := ScHttpWebRequest1.GetResponse;
  if ScHttpWebRequest1.IsSecure then
  begin
    ResponseStr := Response.ReadAsString;
    Memo1.Lines.Add( ResponseStr );
    Memo1.Lines.Add( '' );
  end;

  exit;}
  Memo1.Lines.Clear;
  Request := TScHttpWebRequest.Create('https://staging-core.lafka.tools/api/services/order?'+
             'X-Token=SWw3a63A6phc9291SgUJTUcmaBaUcKjgkOAeKNGfztIMj7NNFsjQqBfKubwdkf&filter=status=1&filter=currency=EUR&limit=4');
  Request.SSLOptions.TrustServerCertificate := true;
  Request.Credentials.UserName := 'janne@storeverse.io';
  Request.Credentials.Password := 'H^?a7H+?mWD=';
  try
    Response := Request.GetResponse;
    if Request.IsSecure then
    begin
      ResponseStr := Response.ReadAsString;
      Memo1.Lines.Add( ResponseStr );
      Memo1.Lines.Add( '' );
      JsonRootObject := TJSonObject.ParseJSONValue(ResponseStr) as TJSONObject;
      try
        if JsonRootObject <> nil then
        begin
          memo1.Lines.Add( 'status "'+ JsonRootObject.GetValue<string>('status', '-' ) +'"' );

          JsonOrderArray := JsonRootObject.FindValue('result') as TJSONArray;
          if JsonOrderArray <> nil then
          begin
            memo1.Lines.Add( 'Orders.count = '+ inttostr( JsonOrderArray.Count ));
            for iOrders := 0 to pred( JsonOrderArray.Count ) do
            begin
              JSonValue := JsonOrderArray.Items[iOrders];
              memo1.Lines.Add( 'Order['+ inttostr( iOrders ) + '] id = "'+ JSonValue.GetValue<string>('id', '-' ) +'"' );

              JsonProductArray := JSonValue.FindValue('properties.products') as TJSONArray;
              if JsonProductArray <> nil then
              begin
                memo1.Lines.Add( 'Products.count = '+ inttostr( JsonProductArray.Count ));
                for iProducts := 0 to pred( JsonProductArray.Count ) do
                begin
                  JSonValue := JsonProductArray.Items[iProducts];
                  memo1.Lines.Add( 'Product['+ inttostr( iProducts ) + '] id = "'+ JSonValue.GetValue<string>('product_id', '-' ) +'"' );
                end;
              end;
            end;

//            JsonOrderArray := jsonvalue.
//            if JsonOrderArray <> nil then
//              for i := 0 to pred( JsonOrderArray.Count ) do
//              begin
//                JSonValue := JsonOrderArray.Items[0];
//                memo1.Lines.Add( 'Order['+ inttostr( i ) + '] id = "'+ JSonValue.GetValue<string>('id', '-' ) +'"' );
//              end;

          end;
        //memo1.Lines.Add( 'status ' + JSONValue.GetValue<string>('status'));
        end;
      finally
        JsonArray.Free;
      end;
    end;

  finally
    Response.Free;
  end;

  exit;

  Memo1.Lines.Clear;
  Request := TScHttpWebRequest.Create('https://jsonplaceholder.typicode.com/users');
  try
    Response := Request.GetResponse;
    try
      if Request.IsSecure then
      begin
        ResponseStr := Response.ReadAsString;
        Memo1.Lines.Add( ResponseStr );
        JsonArray := TJSonObject.ParseJSONValue(ResponseStr) as TJsonArray;
        try
          for iOrders:=0 to JsonArray.Count - 1 do begin
            JsonValue := JsonArray.Items[iOrders];
            iName := JsonValue.GetValue<string>('name');
            iUserName := JsonValue.GetValue<string>('username');
            iEmail := JsonValue.GetValue<string>('email');
            Memo1.Lines.Add('Name: ' + iName + ' - UserName: ' + iUserName + ' - Email: ' + iEmail);
          end;
        finally
          JsonArray.Free;
        end;
      end;
    finally
      Response.Free;
    end;
  finally
    Request.Free;
  end;

end;

end.
