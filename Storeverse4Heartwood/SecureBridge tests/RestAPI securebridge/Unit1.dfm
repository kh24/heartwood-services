object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 372
  ClientWidth = 722
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    722
    372)
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 39
    Width = 706
    Height = 325
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object btn_StoreverseHWCall: TButton
    Left = 89
    Top = 8
    Width = 184
    Height = 25
    Caption = 'HW Storeverse test call'
    TabOrder = 2
    OnClick = btn_StoreverseHWCallClick
  end
  object btn_GetModifiedOrders: TButton
    Left = 297
    Top = 8
    Width = 184
    Height = 25
    Caption = 'SV GetModifiedOrders'
    TabOrder = 3
    OnClick = btn_GetModifiedOrdersClick
  end
  object btn_SV_GetModifiedProducts: TButton
    Left = 487
    Top = 8
    Width = 184
    Height = 25
    Caption = 'SV GetModifiedProducts'
    TabOrder = 4
    OnClick = btn_SV_GetModifiedProductsClick
  end
  object ScSSLClient1: TScSSLClient
    Left = 352
    Top = 200
  end
  object ScHttpWebRequest1: TScHttpWebRequest
    Credentials.Password = 'H^?a7H+?mWD='
    Credentials.UserName = 'janne@storeverse.io'
    ProtocolVersion.Major = 1
    ProtocolVersion.Minor = 1
    RequestUri = 
      'http://core.lafka.tools/api/services/product?X-Token=SWw3a63A6ph' +
      'c9291SgUJTUcmaBaUcKj['#8230']NGfztIMj7NNFsjQqBfKubwdkf&order=C001&dire' +
      'ction=asc&limit=200'
    SSLOptions.Protocols = [spTls11, spTls12, spTls13, spSsl3]
    SSLOptions.TrustServerCertificate = True
    SSLOptions.TrustStorageCertificates = True
    Left = 344
    Top = 144
  end
end
