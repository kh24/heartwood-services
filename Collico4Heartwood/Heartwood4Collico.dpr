program Heartwood4Collico;

uses
  SmartInspect.VCL,
  SmartInspect.VCL.SiAuto,
  Vcl.Forms,
  wHeartwood4Collico in 'wHeartwood4Collico.pas' {window_Heartwood4Collico};

{$R *.res}

begin
  Si.Connections := 'pipe(), file(filename="Heartwood4Collico.sil", maxparts="30", rotate="daily")';
  Si.Enabled := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Twindow_Heartwood4Collico, window_Heartwood4Collico);
  Application.Run;
end.
