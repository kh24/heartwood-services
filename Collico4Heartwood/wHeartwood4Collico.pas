unit wHeartwood4Collico;

interface

uses
  (* API libraries *)
  ScHttp, system.json,
  (* SmartInspect *)
  SmartInspect.VCL.SiAuto,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  Twindow_Heartwood4Collico = class(TForm)
    Label_InstanceInfo: TLabel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  window_Heartwood4Collico: Twindow_Heartwood4Collico;

implementation

{$R *.dfm}

const
  CollicoAPIUrl = 'https://api.collico.fi';
  ApiKeyName = 'X-API-KEY';
  //ApiKeyValue = '8B4kNhWfY72xNXJknlRIxKQMnma+w5jTd0TX9IPg9ok='; // business
  //ApiKeyValue = 'D+PT1mbNV2r3sN1FYH0Mm408EwB18bQQhpilNX/IZqs='; // KH24 beta
  ApiKeyValue = ''; // estradi

procedure Twindow_Heartwood4Collico.Button1Click(Sender: TObject);
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iResponseString : string;
  iParseJSON : TJSONValue;

  iItemCounter, iBatchCounter : word;
  iItemArray, iBatchesArray : TJSONArray;
  iItemGTIN, iTotalBalance, iBatchItemAmount, iBatchItemExpiryDate : TJSONValue;
begin
  //result := false;
  iResponse := nil;

  iRequest := TScHttpWebRequest.Create( CollicoAPIUrl + '/api/v1/stockbalances' );
                // + '?productGtin=7310731102021');
  try
    iRequest.ContentType := 'application/json';
    iRequest.Method := ScHttp.rmGET;
    iRequest.Headers.Add( ApiKeyName, ApiKeyValue );
    //iRequest.Headers.Add( 'productGtin', '7310731102021' );
    SiMain.LogVerbose( 'EXECUTE: '+ CollicoAPIUrl + '/api/v1/stockbalances' +
              '?productGtin=7310731102021');

    try
      try
        iResponse := iRequest.GetResponse;
      except
        on E: HttpException do
        begin
          //if HttpException(E).StatusCode = scForbidden then
          SiMain.LogError( HttpException(E).ServerMessage +
                           ' Msg:' + HttpException(E).Message +
                           ' Trace:' + HttpException(E).StackTrace );
        end;
        on E: Exception do
          SiMain.LogError( E.Message );
      end;(* Except *)

      if iResponse <> nil then
      begin
        //SiMain.LogInteger( 'iResponse.StatusCode', iResponse.StatusCode. );
        SiMain.LogString( 'iResponse.StatusDescription', iResponse.StatusDescription );
        //SiMain.LogString( 'iResponse.Headers.Text', iResponse.Headers.Text );

        if iResponse.StatusCode = scOK then
        begin
          iResponseString := iResponse.ReadAsString;
          SiMain.LogVerbose( iResponseString );
          //result := length( iResponseString ) > 0;

          (* parse JSON *)
          iParseJSON := TJSONObject.ParseJSONValue( iResponseString );
          (* transform JSON into product items *)
          iItemArray := iParseJSON as TJSONArray;
          try
            SiMain.LogInteger( 'iItemArray.Count', iItemArray.Count );
            if iItemArray.Count > 0 then
            for iItemCounter := 0 to pred( iItemArray.Count ) do
            begin
              iItemGTIN := iItemArray.Items[ iItemCounter ].FindValue( 'product.gtin' );
              if assigned( iItemGTIN ) then
              begin
                SiMain.LogString( 'GTIN', iItemGTIN.Value );

                iTotalBalance := iItemArray.Items[ iItemCounter ].FindValue( 'totalBalance' );
                if assigned( iTotalBalance ) then
                begin
                  SiMain.LogString( 'totalBalance', iTotalBalance.Value );

                  {
                  iBatchesArray := iItemArray.Items[ iItemCounter ].FindValue( 'batches' ) as TJSONArray;
                  if assigned( iBatchesArray ) then
                  begin
                    for iBatchCounter := 0 to pred( iBatchesArray.Count ) do
                    begin
                      iBatchItemAmount := iBatchesArray.Items[ iBatchCounter ].FindValue( 'amount' );
                      if Assigned( iBatchItemAmount ) then SiMain.LogString( '  -amount', iBatchItemAmount.Value );

                      iBatchItemExpiryDate := iBatchesArray.Items[ iBatchCounter ].FindValue( 'expiryDate' );
                      if Assigned( iBatchItemExpiryDate ) then SiMain.LogString( '  -expirydate', iBatchItemExpiryDate.Value );

                    end;(* for iBatchCounter *)

                  end;(* Assigned iBatchesArray *)
                  }

                end;(* Assigned TotalBalance *)
              end;(* Assigned iItemGTIN *)
            end;(* for iItemCounter *)

          finally
            iParseJSON.Free;
          end;
        end;
      end;

    finally
      iResponse.Free;
    end;

  finally
    iRequest.Free;
  end;
end;

procedure Twindow_Heartwood4Collico.FormCreate(Sender: TObject);
begin
  Label_InstanceInfo.Caption := IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName ));
  SiMain.LogDebug( Label_InstanceInfo.Caption );
end;

end.
