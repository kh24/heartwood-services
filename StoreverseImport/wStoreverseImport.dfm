object Window_StoreverseImport: TWindow_StoreverseImport
  Left = 0
  Top = 0
  Caption = 'Storeverse Import'
  ClientHeight = 476
  ClientWidth = 694
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    694
    476)
  PixelsPerInch = 120
  TextHeight = 16
  object StringGrid: TStringGrid
    Left = 8
    Top = 39
    Width = 678
    Height = 249
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 4
    DefaultColWidth = 250
    DefaultRowHeight = 25
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSizing, goColSizing, goEditing]
    TabOrder = 0
  end
  object btn_OpenJT: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Open...'
    TabOrder = 1
    OnClick = btn_OpenJTClick
  end
  object btn_LoadProducts: TButton
    Left = 89
    Top = 8
    Width = 112
    Height = 25
    Caption = 'Load products'
    Enabled = False
    TabOrder = 2
    OnClick = btn_LoadProductsClick
  end
  object btn_UpdateToCore: TButton
    Left = 207
    Top = 8
    Width = 112
    Height = 25
    Caption = 'Update to CORE'
    TabOrder = 3
    OnClick = btn_UpdateToCoreClick
  end
  object Memo_Log: TMemo
    Left = 8
    Top = 294
    Width = 678
    Height = 174
    Anchors = [akLeft, akRight, akBottom]
    Lines.Strings = (
      'Memo_Log')
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object CheckBox_Simulate: TCheckBox
    Left = 325
    Top = 16
    Width = 164
    Height = 17
    Caption = 'Simulate'
    Checked = True
    State = cbChecked
    TabOrder = 5
  end
  object OpenDialog1: TOpenDialog
    Left = 168
    Top = 64
  end
end
