program StoreverseImport;

uses
  Vcl.Forms,
  SmartInspect.VCL, SmartInspect.VCL.SiAuto,
  wStoreverseImport in 'wStoreverseImport.pas' {Window_StoreverseImport};

{$R *.res}

begin
  Si.Connections := 'pipe(), file(filename="StoreverseImport.sil", maxparts="10", rotate="daily")';
  Si.Enabled := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TWindow_StoreverseImport, Window_StoreverseImport);
  Application.Run;
end.
