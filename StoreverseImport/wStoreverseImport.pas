unit wStoreverseImport;

interface

(* STEPS
   1. prepare import file inside excel
      - each field inside "xxxxxxxx". Add temporary pipe char prefix and appendix
        for other than "string" fields and replace the pipe with " chars before import,
        k�ytt�m�ll� ="|" & A2 "|" kaavaa
      - talleta CSV muodossa
      - muuta ; erottimeksi ,
      - muuta putken tilalle lainausmerkki
      - save in utf-8 (huom! Valitse No BOM UTF-8)
   2. confirm fields to be updated
   3. run in staging first
   4. run in production

   Average update time 0,35 seconds
*)

uses
  (* rest api *)
  schttp, system.json,
  system.inifiles,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.StdCtrls;

type
  TWindow_StoreverseImport = class(TForm)
    StringGrid: TStringGrid;
    OpenDialog1: TOpenDialog;
    btn_OpenJT: TButton;
    btn_LoadProducts: TButton;
    btn_UpdateToCore: TButton;
    Memo_Log: TMemo;
    CheckBox_Simulate: TCheckBox;
    procedure btn_OpenJTClick(Sender: TObject);
    procedure btn_LoadProductsClick(Sender: TObject);
    procedure btn_UpdateToCoreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FIniFile : TIniFile;
    FProductGTINandID : TStringList;
    FAuthorizeBarerToken : string;
    function RequestToken : boolean;
    function GetAndSaveProductsIntoList : boolean;
    function GetProducts( var aProductsJSON : TJSONObject; const aLimit, aSkip : word; const aParameters : string = '' ) : boolean;
    function Patch( const aObjectName : string; aBodyString : string ) : boolean;
    function FormatProductJSONValue( const aString : string ) : string;
    function CreateProductDescriptionUpdateJSON( aGTIN, aLeadText, aBodyText, aIngridientList : string ) : string;
    function CreateProductPrimaryAndSecondaryLabelkUpdateJSON( aGTIN, aPrimary, aSecondary : string ) : string;
    procedure SaveProductIDsIntoList( aProductsJSON : TJSONObject );
    function InternalRepeatRequest( aRequestURL : string; var aRequestSkipCounter : word; var aJSONResponse : TJSONObject; const aUsername, aPassword : string ) : boolean;
  public
    { Public declarations }
  end;

var
  Window_StoreverseImport: TWindow_StoreverseImport;

implementation

{$R *.dfm}

uses
  System.StrUtils,
  SmartInspect.VCL.SiAuto;

const
  gIniFileName = 'StoreverseImport.ini';
  gSectionSettings = 'Settings';
  gStoreverseAPIUrl = 'StoreverseAPIUrl'; // Include api/services/ or api/services/ or api/latest/
  gStoreverseAPIToken = 'StoreverseAPIToken';
  gStoreverseUsername = 'StoreverseUsername';
  gStoreversePassword = 'StoreversePassword';
  gStoreverseAPITrustServerCertificate = 'TrustServerCertificate';

procedure ClearStringGrid(const Grid: TStringGrid);
var
  c, r: Integer;
begin
  Grid.Visible := false;
  for c := 0 to Pred(Grid.ColCount) do
    for r := 0 to Pred(Grid.RowCount) do
      Grid.Cells[c, r] := '';
  Grid.Visible := true;
end;

function FormatContent( const aBuffer : string ) : string;
begin
  (* UTF conversion and remove " limiters *)
  result := Trim( AnsiReplaceStr( aBuffer, '"', '' ));
end;(* FormatContent *)

function TWindow_StoreverseImport.RequestToken : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iBodyBytes : tbytes;
  iBodyString, iResponseStr : string;

  iJSONResponse : TJSONObject;
  iJSONValue : TJSONValue;
begin
  result := false;

  Memo_Log.Lines.Add( 'RequestToken "'+ FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'auth/token"' );
  SiMain.LogVerbose( 'RequestToken "'+ FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'auth/token"' );
  iRequest := TScHttpWebRequest.Create( FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'auth/token' );
  try
    iRequest.SSLOptions.TrustServerCertificate := FIniFile.ReadBool( gSectionSettings, gStoreverseAPITrustServerCertificate, false );
    iRequest.Method := rmPost;
    iRequest.ContentType := 'application/x-www-form-urlencoded';

    iBodyString := 'username=' + FIniFile.ReadString( gSectionSettings, gStoreverseUsername, '' ) +
               '&password=' + FIniFile.ReadString( gSectionSettings, gStoreversePassword, '' );
    iBodyBytes := TEncoding.UTF8.GetBytes( iBodyString );
    iRequest.WriteBuffer( iBodyBytes );
    iRequest.ContentLength := length( iBodyBytes );

    try
      iResponse := iRequest.GetResponse;
      iResponseStr := iResponse.ReadAsString;
      SiMain.LogString( 'iResponseStr', iResponseStr );
      if Length( iResponseStr ) > 2 then
      begin
        iJSONResponse := TJSONObject.ParseJSONValue( iResponseStr ) as TJSONObject;
        if iJSONResponse <> nil then
        begin
          iJSONValue := iJSONResponse.FindValue( 'access_token' );
          if iJSONValue <> nil then
          begin
            FAuthorizeBarerToken := iJSONValue.Value;
            SiMain.LogString( 'FAuthorizeBarerToken', FAuthorizeBarerToken );
            result := true;
          end;
        end;
      end;

    finally
      iResponse.Free;
    end;

  finally
    iRequest.disconnect;
    iRequest.Free;
  end;
end;(* RequestToken *)

function TWindow_StoreverseImport.GetAndSaveProductsIntoList : boolean;
const
  LIMITCOUNT = 1000;
var
  iProductsJSON : TJSONObject;
  iJSONProductArray: TJSONArray;
  iJSONProductProperties : TJSONObject;
  iJSONDeliveryGTIN, iJSONProductID : TJSonValue;
  iProducts : Integer;
  iSkipCount : word;
begin
  result := false;
  FProductGTINandID.Clear;
  iSkipCount := 0;

  while GetProducts( iProductsJSON, LIMITCOUNT, iSkipCount ) do
  begin
    if iProductsJSON <> nil then
    begin
      try
        iJSONProductArray := iProductsJSON.FindValue( 'result' ) as TJSONArray;
        if iJSONProductArray <> nil then
        begin
          if iJSONProductArray.Count > 0 then
          begin
            for iProducts := 0 to pred( iJSONProductArray.Count ) do
            begin
              iJSONProductID := iJSONProductArray.Items[ iProducts ].FindValue( 'id' );

              iJSONProductProperties := iJSONProductArray.Items[ iProducts ].FindValue( 'properties' ) as TJSONObject;
              if iJSONProductProperties <> nil then
              begin
                (* Store product id and delivery GTIN *)
                iJSONDeliveryGTIN := iJSONProductProperties.FindValue( 'S004' );
                if ( iJSONDeliveryGTIN <> nil ) and ( iJSONProductID <> nil ) then
                  FProductGTINandID.AddPair( iJSONDeliveryGTIN.Value, iJSONProductID.Value )
                else
                  SiMain.LogWarning( 'nils detected' );

                (* one product is enough for positive result *)
                result := true;
              end;(* iJSONProductProperties <> nil *)
            end;(* for iProducts *)

          end else (* iJSONProductArray.Count > 0 *)
            (* result is empty, so there are no more products to download. Break the while loop *)
            break;

        end else Memo_Log.Lines.Add( 'iJSONProductArray = nil' );

      finally
        FreeAndNil( iJSONProductArray );
      end;(* try *)

      inc( iSkipCount, LIMITCOUNT );
      SiMain.LogInteger( 'iSkipCount', iSkipCount );
      FreeAndNil( iProductsJSON );
    end;(* if <> nil *)
  end;(* while *)
end;(* GetAndSaveProductsIntoList *)

function TWindow_StoreverseImport.GetProducts( var aProductsJSON : TJSONObject; const aLimit, aSkip : word; const aParameters : string = '' ) : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iParams : string;
begin
  result := false;

  iParams := '?limit='+ inttostr( aLimit ) +'&skip='+ IntToStr( aSkip );

  Memo_Log.Lines.Add( 'GetProducts "'+ FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'api/latest/product' + iParams );
  SiMain.LogVerbose( 'GetProducts "'+ FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'api/latest/product' + iParams );
  iRequest := TScHttpWebRequest.Create( FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'api/latest/product' + iParams );
  try
    iRequest.SSLOptions.TrustServerCertificate := FIniFile.ReadBool( gSectionSettings, gStoreverseAPITrustServerCertificate, false );
    iRequest.Method := rmGet;
    iRequest.Headers.Add( 'Authorization', 'Bearer '+ FAuthorizeBarerToken );
    iRequest.ContentType := 'application/json';
    try
      iResponse := iRequest.GetResponse;
      aProductsJSON := TJSONObject.ParseJSONValue( iResponse.ReadAsString ) as TJSONObject;
      result := aProductsJSON <> nil;

    finally
      iResponse.Free;
    end;

  finally
    iRequest.disconnect;
    iRequest.Free;
  end;
end;(* GetProducts *)

function TWindow_StoreverseImport.Patch( const aObjectName : string; aBodyString : string ) : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iBodyBytes : tbytes;
  iResponseJSON : TJSONObject;
begin
  result := false;

  Memo_Log.Lines.Add( 'Patch "'+ FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'api/latest/' + aObjectName );
  SiMain.LogVerbose( 'Patch "'+ FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'api/latest/' + aObjectName );
  iRequest := TScHttpWebRequest.Create( FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) + 'api/latest/' + aObjectName );
  try
    iRequest.SSLOptions.TrustServerCertificate := FIniFile.ReadBool( gSectionSettings, gStoreverseAPITrustServerCertificate, false );
    iRequest.Method := rmPATCH;
    iRequest.Headers.Add( 'Authorization', 'Bearer '+ FAuthorizeBarerToken );
    iRequest.ContentType := 'application/json';

    iBodyBytes := TEncoding.UTF8.GetBytes( aBodyString );
    iRequest.WriteBuffer( iBodyBytes );
    iRequest.ContentLength := length( iBodyBytes );

    try
      try
        iResponse := iRequest.GetResponse;
        iResponseJSON := TJSONObject.ParseJSONValue( iResponse.ReadAsString ) as TJSONObject;
        result := iResponseJSON <> nil;
        if result then SiMain.LogString( 'iResponseJSON.ToString', iResponseJSON.ToString );
      except
        On E:Exception do
        begin
          SiMain.LogException( E, 'PATH error' );
        end;
      end;

    finally
      iResponse.Free;
    end;

  finally
    iRequest.disconnect;
    iRequest.Free;
  end;
end;(* Patch *)

function TWindow_StoreverseImport.FormatProductJSONValue( const aString : string ) : string;
begin
  (* null or string *)
  if length( aString ) = 0 then result := 'null' else result := '"'+ aString +'"';

  (* replace enter with \n *)
  result := StringReplace( result, sLineBreak, '<br>', [rfReplaceAll]);
end;(* _FormatValue *)

function TWindow_StoreverseImport.CreateProductDescriptionUpdateJSON( aGTIN, aLeadText, aBodyText, aIngridientList : string ) : string;
var
  iGTINIndex : integer;
begin
  result := '';
  iGTINIndex := FProductGTINandID.IndexOfName( aGTIN );
  if iGTINIndex <> -1 then
  begin
    result := '[{"id": '+ FProductGTINandID.ValueFromIndex[ iGTINIndex ] + ','+
              '"properties": {'+
                '"L008": ' + FormatProductJSONValue( aLeadText ) + ',' +
                '"L009": ' + FormatProductJSONValue( aBodyText ) + ',' +
                '"L007": ' + FormatProductJSONValue( aIngridientList ) + '}'+
              '}]';
  end else
    SiMain.LogError( 'Id for product "'+ aGTIN + '" was not found!' );
end;(* CreateProductDescriptionUpdateJSON *)

function TWindow_StoreverseImport.CreateProductPrimaryAndSecondaryLabelkUpdateJSON( aGTIN, aPrimary, aSecondary : string ) : string;
var
  iGTINIndex : integer;
begin
  result := '';
  iGTINIndex := FProductGTINandID.IndexOfName( aGTIN );
  if iGTINIndex <> -1 then
  begin
    result := '[{"id": '+ FProductGTINandID.ValueFromIndex[ iGTINIndex ] + ','+
              '"properties": {'+
                '"L005": ' + FormatProductJSONValue( aPrimary ) + ',' +
                '"L006": ' + FormatProductJSONValue( aSecondary ) + '}'+
              '}]';
  end else
    SiMain.LogError( 'Id for product "'+ aGTIN + '" was not found!' );
end;(* CreateProductPrimaryAndSecondaryLabelkUpdateJSON *)

procedure TWindow_StoreverseImport.SaveProductIDsIntoList( aProductsJSON : TJSONObject );
var
  iJSONProductArray: TJSONArray;
  iJSONProductProperties : TJSONObject;
  iJSONDeliveryGTIN, iJSONProductID : TJSonValue;
  iProducts : Integer;
begin
  FProductGTINandID.Clear;

  try
    iJSONProductArray := aProductsJSON.FindValue( 'result' ) as TJSONArray;
    if iJSONProductArray <> nil then
    begin
      if iJSONProductArray.Count > 0 then
      for iProducts := 0 to pred( iJSONProductArray.Count ) do
      begin
        iJSONProductID := iJSONProductArray.Items[ iProducts ].FindValue( 'id' );


        iJSONProductProperties := iJSONProductArray.Items[ iProducts ].FindValue( 'properties' ) as TJSONObject;
        if iJSONProductProperties <> nil then
        begin
          (* Store product id and delivery GTIN *)
          iJSONDeliveryGTIN := iJSONProductProperties.FindValue( 'S004' );
          if ( iJSONDeliveryGTIN <> nil ) and ( iJSONProductID <> nil ) then
            FProductGTINandID.AddPair( iJSONDeliveryGTIN.Value, iJSONProductID.Value )
          else
            SiMain.LogWarning( 'nils detected' );
        end;(* iJSONProductProperties <> nil *)
      end;(* for iProducts *)

    end else Memo_Log.Lines.Add( 'iJSONProductArray = nil' );

  finally
    FreeAndNil( iJSONProductArray );
  end;
end;(* SaveProductIDsIntoList *)

function TWindow_StoreverseImport.InternalRepeatRequest( aRequestURL : string; var aRequestSkipCounter : word; var aJSONResponse : TJSONObject; const aUsername, aPassword : string ) : boolean;
var
  iRequest: TScHttpWebRequest;
  iResponse: TScHttpWebResponse;
  iResponseStr : string;
  iJSONValue : TJSonValue;
  iMetaTotal, iItemsCount, iMetalSkip : integer;
begin
  result := false;

  if aRequestSkipCounter > 0 then
    aRequestURL := aRequestURL + '&skip='+ inttostr( aRequestSkipCounter );

  Memo_Log.Lines.Add( 'request "'+ aRequestURL + '"' );
  SiMain.LogVerbose( 'request "'+ aRequestURL + '"' );
  iRequest := TScHttpWebRequest.Create( aRequestURL );
  try
    iRequest.SSLOptions.TrustServerCertificate := FIniFile.ReadBool( gSectionSettings, gStoreverseAPITrustServerCertificate, false );
    iRequest.Credentials.UserName := aUsername;
    iRequest.Credentials.Password := aPassword;

    try
      iResponse := iRequest.GetResponse;
      iResponseStr := iResponse.ReadAsString;
      if Length( iResponseStr ) > 2 then
      //if Request.IsSecure then
      begin
        aJSONResponse := TJSONObject.ParseJSONValue( iResponseStr ) as TJSONObject;
        if aJSONResponse <> nil then
        begin
          iJSONValue := aJSONResponse.FindValue( 'status' );
          if iJSONValue <> nil then
            if iJSONValue.Value <> '200' then
              Memo_Log.Lines.Add( 'Invalid request status = '+ iJSONValue.Value );
              { TODO 2 -cthink : pit�isik� t�h�n kohtaa pys�ytt�� k�sittely jos arvo on <> 200 }

          iJSONValue := aJSONResponse.FindValue( 'meta.total' );
          if iJSONValue <> nil then iMetaTotal := StrToIntDef( iJSONValue.Value, -1 ) else iMetaTotal := -2;
          iJSONValue := aJSONResponse.FindValue( 'items' );
          if iJSONValue <> nil then iItemsCount := StrToIntDef( iJSONValue.Value, -1 ) else iItemsCount := -2;
          iJSONValue := aJSONResponse.FindValue( 'meta.skip' );
          if iJSONValue <> nil then iMetalSkip := StrToIntDef( iJSONValue.Value, -1 ) else iMetalSkip := -2;

          Memo_Log.Lines.Add( 'meta.Total = '+ inttostr( iMetaTotal ));
          Memo_Log.Lines.Add( 'meta.Skip = '+ inttostr( iMetalSkip ));
          Memo_Log.Lines.Add( 'Items = '+ inttostr( iItemsCount ));

          result := iItemsCount > 0;
          Memo_Log.Lines.Add( 'result = '+ inttostr( ord( result )));

          aRequestSkipCounter := aRequestSkipCounter + iItemsCount;
          Memo_Log.Lines.Add( 'new skip value = '+ inttostr( aRequestSkipCounter ));

        end else Memo_Log.Lines.Add( 'InternalRepeatRequest: aJSONResponse = nil' );
      end else Memo_Log.Lines.Add( 'InternalRepeatRequest: aJSONResponse was empty "'+ iResponseStr + '"' );

    finally
      iResponse.Free;
    end;

  finally
    iRequest.disconnect;
    iRequest.Free;
  end;
end;(* InternalRepeatRequest *)

procedure TWindow_StoreverseImport.btn_LoadProductsClick(Sender: TObject);
const
  LIMITPRODUCTCOUNT = 200;
var
  iJSONResponse : TJSONObject;
  iJSONProductArray: TJSONArray;
  iJSONProductProperties : TJSONObject;
  iJSONDeliveryGTIN, iJSONProductID : TJSonValue;
  iProducts : Integer;
  iRequestSkipCounter : word;
begin
  Memo_Log.Lines.Clear;
  FProductGTINandID.Clear;

  //AddToOverallLog( 'RequestModifiedProducts begin' );
  try
    try
      iRequestSkipCounter := 0;
      while InternalRepeatRequest( FIniFile.ReadString( gSectionSettings, gStoreverseAPIUrl, '' ) +
                                   'api/services/product?' +
                                  // 'X-Token=' + FIniFile.ReadString( gSectionSettings, gStoreverseAPIToken, '' ) +
                                   '&order=C001&direction=asc&limit='+ inttostr( LIMITPRODUCTCOUNT ),
                                   iRequestSkipCounter, iJSONResponse,
                                   FIniFile.ReadString( gSectionSettings, gStoreverseUsername, 'janne.timmerbacka@kauppahalli24.fi' ),
                                   FIniFile.ReadString( gSectionSettings, gStoreversePassword, '_!S4f_Fr0NN' )) do
      begin
        if iJSONResponse <> nil then
        begin
          (* PRODUCTS *************************************************)
//          if FDQuery_Products.active then FDQuery_Products.close;
//          if FDQuery_Products.IndexFieldNames <> PRODUCTS_BARCODE then FDQuery_Products.IndexFieldNames := PRODUCTS_BARCODE;
//          FDQuery_Products.Open;

          try
            iJSONProductArray := iJSONResponse.FindValue( 'result' ) as TJSONArray;
            if iJSONProductArray <> nil then
            begin
              if iJSONProductArray.Count > 0 then
              for iProducts := 0 to pred( iJSONProductArray.Count ) do
              begin
                iJSONProductID := iJSONProductArray.Items[ iProducts ].FindValue( 'id' );


                iJSONProductProperties := iJSONProductArray.Items[ iProducts ].FindValue( 'properties' ) as TJSONObject;
                if iJSONProductProperties <> nil then
                begin
                  (* Store product id and delivery GTIN *)

                  iJSONDeliveryGTIN := iJSONProductProperties.FindValue( 'S004' );
                  if ( iJSONDeliveryGTIN <> nil ) and ( iJSONProductID <> nil ) then
                    FProductGTINandID.AddPair( iJSONDeliveryGTIN.Value, iJSONProductID.Value )
                  else
                    SiMain.LogWarning( 'nils detected' );
                end;(* iJSONProductProperties <> nil *)
              end;(* for iProducts *)

            end else Memo_Log.Lines.Add( 'iJSONProductArray = nil' );

//            SaveLog( gLogGetModifiedProducts );

          finally
            FreeAndNil( iJSONProductArray );
//            if FDQuery_Products.ChangeCount > 0 then
//            begin
//              FDQuery_Products.ApplyUpdates;
//              FDQuery_Products.CommitUpdates;
//            end;
//            if FDQuery_Products.active then FDQuery_Products.close;
          end;(* try PRODUCTS *)

        end;(* iJSONResponse <> nil *)
          { TODO 2 -cthink : pit�isik� nil case k�sitell� }
      end;(* while *)

      SiMain.LogStringList( 'FProductGTINandID', FProductGTINandID );

    except
      On E:Exception do
      begin
        Memo_Log.Lines.Add( 'RequestModifiedProducts.EXCEPTION '+ E.Message );
        SiMain.LogException( E, 'RequestModifiedProducts.EXCEPTION');
//        AddToOverallLog( 'RequestModifiedProducts.EXCEPTION '+ E.Message );
      end;
    end;

  finally
//    SaveLog( gLogGetModifiedProducts );
//    AddToOverallLog( 'RequestModifiedProducts end' );
  end;
end;

procedure TWindow_StoreverseImport.btn_OpenJTClick(Sender: TObject);
var
  FileName: string;
  FileContents, CellContents : TStringList;
  Row, Pos, StartPos, GridRow, GridCol : word;
  //, Col,  : Integer;
  InsideQuotes: Boolean;
  Line, Cell: string;
begin
  if OpenDialog1.Execute then
  begin
    FileName := OpenDialog1.FileName;

    FileContents := TStringList.Create;
    CellContents := TStringList.Create;
    try
      FileContents.LoadFromFile(FileName);
      StringGrid.RowCount := 2;
      StringGrid.ColCount := 5;
      StringGrid.FixedRows := 1;  // Assume first row is header
      GridRow := 0;
      GridCol := 0;

      InsideQuotes := False;
      Cell := '';
      CellContents.Clear;

      for Row := 0 to FileContents.Count - 1 do
      begin
        Line := FileContents[Row];
        StartPos := 1;

        for Pos := 1 to Length(Line) do
        begin
          if Line[Pos] = '"' then
            InsideQuotes := not InsideQuotes
          else
            if (Line[Pos] = ',') and not InsideQuotes then
          begin
            (* store cell content *)
            Cell := Copy(Line, StartPos, Pos - StartPos);
            CellContents.Add(UTF8ToString( Cell ));
            StartPos := Pos + 1;

            StringGrid.Cells[GridCol, GridRow] := FormatContent( CellContents.Text );
//            SiMain.LogStringList( 'cell('+ inttostr( GridCol )+
//                                  ','+ inttostr( GridRow )+
//                                  ')', CellContents );
            inc( GridCol );
            CellContents.clear;
          end;
        end;

        (* Add into last cell *)
        Cell := UTF8ToString( Copy(Line, StartPos, MaxInt));
        CellContents.Add(Cell);

        (* New line outside quotes means new record *)
        if not InsideQuotes then
        begin
          StringGrid.Cells[GridCol, GridRow] := FormatContent( CellContents.Text );
//            SiMain.LogStringList( 'cell('+ inttostr( GridCol )+
//                                  ','+ inttostr( GridRow )+
//                                  ')', CellContents );
          inc( GridRow );
          StringGrid.RowCount := GridRow;
          GridCol := 0;

          CellContents.Clear;
        end;
      end;

    finally
      FileContents.Free;
      CellContents.Free;
    end;
  end;
end;

procedure TWindow_StoreverseImport.btn_UpdateToCoreClick(Sender: TObject);
var
  iJSONBuffer : string;
  iProductCounter, iUpdatedCount, iFailedCount : word;
begin
  if RequestToken then
  begin
    if GetAndSaveProductsIntoList then
    begin
      SiMain.LogStringList( 'FProductGTINandID', FProductGTINandID );
      Memo_Log.Lines.Add( 'FProductGTINandID.Count=' + inttostr( FProductGTINandID.Count ));

      iUpdatedCount := 0;
      iFailedCount := 0;

      for iProductCounter := 1 to StringGrid.RowCount do
        if length( StringGrid.Cells[ 0, iProductCounter] ) > 0 then
        begin
          iJSONBuffer := CreateProductPrimaryAndSecondaryLabelkUpdateJSON( StringGrid.Cells[ 0, iProductCounter], //gtin
                                                  StringGrid.Cells[ 1, iProductCounter], //primary
                                                  StringGrid.Cells[ 2, iProductCounter]); //secondary
          {
          iJSONBuffer := CreateProductDescriptionUpdateJSON( StringGrid.Cells[ 0, iProductCounter], //gtin
                                                  StringGrid.Cells[ 1, iProductCounter], //lead
                                                  StringGrid.Cells[ 2, iProductCounter], //body
                                                  StringGrid.Cells[ 3, iProductCounter]); //incredidents
          }
          if not CheckBox_Simulate.Checked then
          begin
            if length( iJSONBuffer ) > 0 then
            begin
              SiMain.LogString( StringGrid.Cells[ 0, iProductCounter], iJSONBuffer );
              if Patch( 'product', iJSONBuffer ) then
                inc( iUpdatedCount )
              else
              begin
                inc( iFailedCount );
                SiMain.LogError( 'Patch updating product "'+ StringGrid.Cells[ 0, iProductCounter] + '" failed!' );
              end;
            end else (* length *)
              inc( iFailedCount );
          end else
            (* CheckBox_Simulate.Checked *)
            SiMain.LogDebug( 'JSON: '+ iJSONBuffer );
        end;(* for *)

    end;(* GetProducts *)
  end;(* RequestToken *)

  Memo_Log.Lines.Add( '' );
  Memo_Log.Lines.Add( 'COMPLETE!' );
  Memo_Log.Lines.Add( '-updated '+ inttostr( iUpdatedCount ));
  Memo_Log.Lines.Add( '-failed '+ inttostr( iFailedCount ));

  SiMain.LogInteger( 'iUpdatedCount', iUpdatedCount );
  SiMain.LogInteger( 'iFailedCount', iFailedCount );
end;

procedure TWindow_StoreverseImport.FormCreate(Sender: TObject);
  function _SafeReadAndStoreDefaultStringValue( aIniFile : TIniFile; const aSection, aName, aDefaultValue : string ) : string;
  begin
    result := aIniFile.ReadString( aSection, aName, '' );
    if length( result ) = 0 then
    begin
      result := aDefaultValue;
      aIniFile.WriteString( aSection, aName, aDefaultValue );
    end;
  end;(* _SafeReadAndStoreDefaultStringValue *)

begin
  FAuthorizeBarerToken := '';
  FProductGTINandID := TStringList.Create;

  Memo_Log.Lines.Add( 'Opening inifile "'+ IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + gIniFileName + '"' );
  FIniFile := TIniFile.create( IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + gIniFileName );

  Memo_Log.Lines.Add( 'Storeverse API url "'+ _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gStoreverseAPIUrl, 'https://staging-core.lafka.tools/api/services/' ) + '"' );
  Memo_Log.Lines.Add( 'Storeverse API token "'+ _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gStoreverseAPIToken, 'SWw3a63A6phc9291SgUJTUcmaBaUcKjgkOAeKNGfztIMj7NNFsjQqBfKubwdkf' ) + '"' );
  Memo_Log.Lines.Add( 'Storeverse Username "'+ _SafeReadAndStoreDefaultStringValue( FIniFile, gSectionSettings, gStoreverseUsername, 'janne.timmerbacka@kauppahalli24.fi' ) + '"' );
end;

procedure TWindow_StoreverseImport.FormDestroy(Sender: TObject);
begin
  FreeAndNil( FIniFile );
  FreeAndNil( FProductGTINandID );
end;

end.
