unit windowSynkka4Storeverse;

interface

uses
  IdHTTPServer, IdCustomHTTPServer, IdContext, IdURI,
  JSON,
  System.Inifiles,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IdBaseComponent, IdComponent,
  IdCustomTCPServer, Vcl.StdCtrls;

type
  Twindow_Synkka4Storeverse = class(TForm)
    IdHTTPServer: TIdHTTPServer;
    Label_Status: TLabel;
    procedure IdHTTPServerCommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FIniFile : TIniFile;
    function ExampleProductJSON( const aIndex : word = 0 ) : string;
    function HandleNewProduct(GTIN: string) : string;
    function HandleUpdateProducts(StartFromProduct, EndToProduct: Word) : string;
    function HtmlEncode(const AText: string): string;
  public
  end;

var
  window_Synkka4Storeverse: Twindow_Synkka4Storeverse;

implementation

{$R *.dfm}

uses
  System.StrUtils,
  SmartInspect.VCL.SiAuto;

procedure Twindow_Synkka4Storeverse.FormActivate(Sender: TObject);
begin
  if tag <> 1 then
  begin
    tag := 1;

    IdHTTPServer.DefaultPort := FIniFile.ReadInteger( 'Settings', 'port', 8098 );
    Label_Status.Caption := 'Port '+ inttostr( IdHTTPServer.DefaultPort ) +
                            ' ExeName '+ Application.ExeName;
    SiMain.LogDebug( 'Starting http server in port '+ inttostr( IdHTTPServer.DefaultPort ));
    IdHTTPServer.Active := true;
  end;
end;

procedure Twindow_Synkka4Storeverse.FormCreate(Sender: TObject);
begin
  FIniFile := TIniFile.Create( IncludeTrailingPathDelimiter( ExtractFilePath( Application.ExeName )) + 'Synkka4Storeverse.ini' );
end;

procedure Twindow_Synkka4Storeverse.FormDestroy(Sender: TObject);
begin
  FreeAndNil( FIniFile );
end;

function Twindow_Synkka4Storeverse.HandleNewProduct(GTIN: string) : string;
begin
  result := ExampleProductJSON;
  //SiMain.LogString( 'HandleNewProduct', result);
end;

function Twindow_Synkka4Storeverse.ExampleProductJSON( const aIndex : word = 0 ) : string;
var
  iProductJSON : TJSONObject;
  iDeliveryJSON, iSupplyJSON, iStorageAndHandlingJSON, iAttributesJSON, iNutritionJSON, iLabelsJSON : TJSONObject;
begin
  iProductJSON := TJSONObject.Create;
  iLabelsJSON := TJSONObject.Create;
  iNutritionJSON := TJSONObject.Create;
  iSupplyJSON := TJSONObject.Create;
  iStorageAndHandlingJSON := TJSONObject.Create;
  iAttributesJSON := TJSONObject.Create;
  iDeliveryJSON := TJSONObject.Create;
  try
    iProductJSON.AddPair('GTIN', '6413300024388');
    iProductJSON.AddPair('InternalName', 'Arla ihana rahka piparkakku 200G laktoositon');
    iProductJSON.AddPair('NetVolume', TJSONNumber.Create( 200 ));
    iProductJSON.AddPair('VolumeTitle', 'g');

//    if FIniFile.ReadInteger( 'Settings', 'Skipport', 8098 ); then

    iLabelsJSON.AddPair( 'LeadText', HtmlEncode('<p>Arla Ihana rahka piparkakku 200 g laktoositon, kausimaku / Arla Ihana kvark pepparkaka 200 g laktosfri, s�songsmak</p>'+
                         '<p>Arla Ihana piparkakkurahka on herkullisen t�ytel�inen piparkakun makuinen rahka j�lkiruokiin, leivontaan ja t�ytteisiin. Tuote'+
                         ' maistuu mainiolta sellaisenaan tai siit� on helppo tehd� nopea j�lkiruoka lis��m�ll� joukkoon esimerkiksi keksimuruja, sinihomejuustomurua'+
                         ' ja hillotettua p��ryn��. Kokeile rahkaa lis�ksi piirakoiden ja kakkujen t�ytteen�, ihania reseptej� l�yd�t osoitteesta arla.fi. Tuote on '+
                         'laktoositon.</p><p>Arla Ihana pepparkaka kvark �r en l�cker fyllig pepparkaka smakande kvark till desserter, bakning och fyllningar. Produkten'+
                         ' smakar utm�rkt som s�dan eller det �r l�tt att snabbt g�ra en dessert genom att till�gga tex kexsmulor, bl�m�gelost och syltade p�ron. Prova'+
                         ' ocks� kvarken som fyllning i piroger och kakor, ljuvliga recept hittar du p� adressen arla.fi  Produkten �r laktosfri.</p><p><strong>'+
                         'S�ILYTYSOHJEET/ F�RVARING INSTRUKTIONER</strong></p><p>S�ilytet��n alle +8�C. Parasta ennen: p�iv�ys kannessa / F�rvaras under +8�C. B�st'+
                         ' f�re: se locket.</p><p><strong>K�YTT�OHJEET/ ANV�NDARINSTRUKTIONER</strong></p><p>J�lkiruokiin, t�ytteisiin ja leivontaan.</p><p>Till '+
                         'efterr�tter, fyllningar och bakning.</p><p><strong>KIERR�TYSOHJEET/ �TERVINNINGSINSTRUKTIONER</strong></p><p>K��re kartonkiker�ykseen, '+
                         'purkki muovinker�ykseen tai energiaj�tteeseen, kansi metallinker�ykseen.</p><p>Omslaget till kartonginsamlingen, burken till plastinsamlingen'+
                         ' eller energiaavfall, locket till metallinsamlingen.</p><p><strong>ALKUPER�MAA/ VALMISTUSMAA/ URSPRUNGSLAND/TILLVERKNINGSLAND</strong></p>'+
                         '<p>Suomi 246</p><p><strong>MAAHANTUOJA / IMPORT�R</strong></p><p>Valmistaja /Tillverkare</p><p>Arla Oy</p>') );
    iLabelsJSON.AddPair( 'BodyText', HtmlEncode('<strong><p>VALMISTAJA/ TILLVERKARE</p></strong><p>Arla Oy</p>') );
    iLabelsJSON.AddPair( 'IngredientList', HtmlEncode('<p>MAITORAHKA (past�roitu MAITO, hapate), past�roitu KERMA, sokeri, vesi, maissit�rkkelys, luontainen vanilja-aromi, '+
                         'sakeuttamisaine (pektiini), kaneli, luontaiset aromit (mm. mausteneilikka), inkiv��ri, s�il�nt�aine (kaliumsorbaatti), happamuudens��t�aine(sitruunahappo)'+
                         '</p><p>KVARG (past�riserad MJ�LK, syrningskultur), past�riserad GR�DDE, socker, vatten, majsst�rkelse, naturlig vaniljarom, f�rtjockningsmedel (pektin), '+
                         'kanel, naturliga aromer (bl.a. kryddnejlika), ingef�ra, konserveringsmedel (kaliumsorbat), surhetsreglerande medel(citronsyra)</p>' ));
    iProductJSON.AddPair( 'Labels', iLabelsJSON );

    //iNutritionJSON.AddPair( 'PortionSize', '100' );
    //iNutritionJSON.AddPair( 'PortionUnit', 'g' );
    iNutritionJSON.AddPair( 'EnergeticValue', TJSONNumber.Create( 160 ));
    iNutritionJSON.AddPair( 'Fat', TJSONNumber.Create( 7 ));
    iNutritionJSON.AddPair( 'SaturatedFats', TJSONNumber.Create( 4.6 ));
    iNutritionJSON.AddPair( 'Protein', TJSONNumber.Create( 7.1 ));
    iNutritionJSON.AddPair( 'Carbohydrates', TJSONNumber.Create( 15 ));
    iNutritionJSON.AddPair( 'Sugar', TJSONNumber.Create( 14 ));
    iNutritionJSON.AddPair( 'Lactose', TJSONNumber.Create( 5 ));
    iNutritionJSON.AddPair( 'Fibers', TJSONNumber.Create( 3 ));
    iNutritionJSON.AddPair( 'Salt', TJSONNumber.Create( 0.1 ));
    iNutritionJSON.AddPair( 'Calium', TJSONNull.Create );
    iProductJSON.AddPair( 'Nutrition', iNutritionJSON );

    iAttributesJSON.AddPair( 'Frozen', TJSONBool.Create( true ));
    //iAttributesJSON.AddPair( 'Organic', 'False' );
    //iAttributesJSON.AddPair( 'Local', 'True' );
    iAttributesJSON.AddPair( 'GlutenFree', TJSONBool.Create( True ));
    iAttributesJSON.AddPair( 'LactoseFree', TJSONBool.Create( True ));
    iAttributesJSON.AddPair( 'Vegan', TJSONBool.Create( False ));
    //iAttributesJSON.AddPair( 'Oversize', 'True' );
    //iAttributesJSON.AddPair( 'Fragile', 'True' );
    iProductJSON.AddPair( 'Attributes', iAttributesJSON );

    iStorageAndHandlingJSON.AddPair( 'MinimumTemperature', TJSONNumber.Create( 3 ));
    iStorageAndHandlingJSON.AddPair( 'MaximumTemperature', TJSONNumber.Create( 6 ));
    iProductJSON.AddPair( 'StorageAndHandling', iStorageAndHandlingJSON );

    iDeliveryJSON.AddPair( 'DeliveryWidth', TJSONNumber.Create( 95 ));
    iDeliveryJSON.AddPair( 'DeliveryHeight', TJSONNumber.Create( 95 ));
    iDeliveryJSON.AddPair( 'DeliveryDepth', TJSONNumber.Create( 58 ));
    iDeliveryJSON.AddPair( 'DeliveryWeight', TJSONNumber.Create( 0.2 ));
    iProductJSON.AddPair( 'Delivery', iDeliveryJSON );

//    iSupplyJSON.AddPair( 'Calium', '2' );
//    iProductJSON.AddPair( 'Supply', iSupplyJSON );

    result := iProductJSON.ToString;

  finally
    FreeAndNil( iProductJSON );
    //FreeAndNil( iDeliveryJSON );
  end;
end;(* ExampleProductJSON *)

function Twindow_Synkka4Storeverse.HandleUpdateProducts(StartFromProduct, EndToProduct: Word) : string;
var
  iResponseArrayJSON: TJSONArray;
  iProductJSON: TJSONObject;
  iParseJSON : TJSONValue;
  i: Word;
begin
  iResponseArrayJSON := TJSONArray.Create;
  try
    for i := StartFromProduct to EndToProduct do
    begin
      iParseJSON := TJSONObject.ParseJSONValue( ExampleProductJSON( i ));

      iProductJSON := iParseJSON as TJSONObject;
      iResponseArrayJSON.Add(iProductJSON);
    end;

    result := iResponseArrayJSON.ToString;
    //SiMain.LogString( 'HandleUpdateProducts', result );

  finally
    iResponseArrayJSON.Free;
  end;
end;

function Twindow_Synkka4Storeverse.HtmlEncode(const AText: string): string;
(* https://mateam.net/html-escape-characters/ *)
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(AText) do
  begin
    case AText[I] of
      '�': Result := Result + '&auml;';
      '�': Result := Result + '&ouml;';
      '�': Result := Result + '&aring;';
      '�': Result := Result + '&Auml;';
      '�': Result := Result + '&Ouml;';
      '�': Result := Result + '&Aring;';
    //  '<': Result := Result + '&lt;';    these we need
    //  '>': Result := Result + '&gt;';
      '&': Result := Result + '&amp;';
      '"': Result := Result + '&quot;';
      '''': Result := Result + '&#39;';
      '�' : result := result + '&#176;';
      '+' : result := result + '&#43;';
      '-' : result := result + '&#8722;';
      '/' : result := result + '&#47;';
      else Result := Result + AText[I];
    end;
  end;
end;(* HtmlEncode *)

procedure Twindow_Synkka4Storeverse.IdHTTPServerCommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
  Params: TStrings;
  GTIN: string;
  StartFromProduct, EndToProduct: Word;
begin
  SiMain.LogDebug( 'CommandGet: '+ ARequestInfo.Document );
  if ARequestInfo.Params.Count > 0 then SiMain.LogStringList( 'Params', ARequestInfo.Params );

  if StartsText('/new', ARequestInfo.Document) then      //http://localhost/new?gtin=123448238423423
  begin
    Params := TStringList.Create;
    try
      Params.Delimiter := '&';
      Params.DelimitedText := Copy(ARequestInfo.Document, 13, MaxInt);
      GTIN := Params.Values['GTIN'];
      AResponseInfo.ContentText := HandleNewProduct(GTIN);
      AResponseInfo.ContentType := 'application/json';
      AResponseInfo.ResponseNo := 200;

    finally
      Params.Free;
    end;
  end
  else if StartsText('/update', ARequestInfo.Document) then   //http://localhost/update?StartFromProduct=1&EndToProduct=5
  begin
    StartFromProduct := StrToInt(ARequestInfo.Params.Values['StartFromProduct']);
    EndToProduct := StrToInt(ARequestInfo.Params.Values['EndToProduct']);

    AResponseInfo.ContentText := HandleUpdateProducts(StartFromProduct, EndToProduct);
    AResponseInfo.ContentType := 'application/json';
    AResponseInfo.ResponseNo := 200;
  end else
  begin
    AResponseInfo.ContentType := 'text';
    AResponseInfo.ResponseNo := 200;
    AResponseInfo.ContentText := '<html><head><title>Synkka Storeverse API</title></head>' +
      '<body>USAGE:<br>' +
      'Update single new product from synkka .../new?gtin=123448238423423<br>' +
      'Update all existing products in Storeverse store from synkka .../update?StartFromProduct=1&EndToProduct=5<br>' +
      '</body></html>';
  end;
end;

end.
