program Synkka4Storeverse;

uses
  Vcl.Forms,
  SmartInspect.VCL, SmartInspect.VCL.SiAuto,
  windowSynkka4Storeverse in 'windowSynkka4Storeverse.pas' {window_Synkka4Storeverse};

{$R *.res}

begin
  Si.Connections := 'pipe(), file(filename="Heartwood4SynkkaStoreverse.sil", maxparts="10", rotate="daily")';
  Si.Enabled := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Twindow_Synkka4Storeverse, window_Synkka4Storeverse);
  Application.Run;
end.
